//
//  Untitled.swift
//  HareSwift
//
//  Created by Entao on 2025/1/3.
//

import Foundation
import HareSwift

////Log.add(printer: DirLogPrinter(dir: "/Users/entao/Downloads/log"))
//Log.add(printer: CacheLogPrinter)
//
//let d = Date.now
//logd(d.year, d.month, d.day, "  ", d.hour, d.minute, d.second, d.millsecond)
//logd("Hello")
//logd("Entao")
let dir = "/Users/entao/Downloads"
let url = dir.fileURL
let ls1 = FileManager.default.subpaths(atPath: dir)  // list names
println("subpath: ", ls1)
let ls2 = try FileManager.default.subpathsOfDirectory(atPath: dir)
println("subpath of dir : ", ls2)
let ls3 = try FileManager.default.contentsOfDirectory(atPath: dir)
println("contents: ", ls3)
