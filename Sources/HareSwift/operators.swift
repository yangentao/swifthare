//
//  operators.swift
//  HareSwift
//
//  Created by Entao on 2025/1/22.
//
import Foundation

/// "age" >>> 99
infix operator >>> : AdditionPrecedence
@inlinable
public func >>> <A: Any, B: Any>(a: A, b: B) -> Pair<A, B> {
    return Pair(a, b)
}

/// "age" <<<  999
infix operator <<< : AdditionPrecedence
@inlinable
public func <<< <V: Any>(label: String, value: V) -> LabelValue<V> {
    return LabelValue<V>(label, value)
}

// a ?= 1
infix operator ?= : AssignmentPrecedence
@inlinable
public func ?= <A: Any>(a: inout A?, b: A) {
    if a == nil {
        a = b
    }
}

//IN   1 *= [1,2,3]
@inlinable
public func *= <T: Equatable, C: Sequence>(e: T, c: C) -> Bool where C.Element == T {
    return c.contains(e)
}

public func >> <T: Any>(key: String, value: T) -> StringValue<T> {
    return StringValue(key: key, value: value)
}
