//
// Created by entaoyang@163.com on 2022/9/13.
//

import Foundation

extension NSObject {

    public func receive(notification: Notification.Name, selector: Selector, for object: NSObject? = nil) {
        NotificationCenter.default.addObserver(self, selector: selector, name: notification, object: object)
    }

}

extension AttrKey {
    fileprivate static let tagText: AttrKey = .init("attr.tag_text")
}

extension NSObject {
    public var tagText: String {
        get {
            self[.tagText] ?? ""
        }
        set {
            self[.tagText] = newValue
        }
    }
}

extension NSObject {
    //MyTarget.MyClass => "Myclass"
    public var classNameOnly: String {
        typeName.substringAfterLast(char: ".")
    }
    //MyTarget.MyClass => "MyTarget.MyClass"
    public var typeName: String {
        NSStringFromClass(type(of: self))
    }
}

public typealias ObjectBlock = (NSObject) -> Void

extension AttrKey {
    fileprivate static let observeCallback = AttrKey("observeCallback")
}

extension NSObject {
    public func onChanged(prop: String, _ callback: @escaping ObjectBlock) {
        self[.observeCallback] = callback
        self.addObserver(ObjectObserverStub.shared, forKeyPath: prop, options: [.old, .new], context: nil)
    }
}

private class ObjectObserverStub: NSObject {
    private override init() {
        super.init()
    }

    @objc
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
        if let obj: NSObject = object as? NSObject, let callback: ObjectBlock = obj[.observeCallback] {
            callback(obj)
            return
        }
    }

    static let shared = ObjectObserverStub()
}

//------------------new style--------------

//extension AttrKey {
//    static let userChanged = MsgID("msg.userChanged")
//}

public struct AttrKey: Hashable, Equatable, RawRepresentable {
    public typealias RawValue = String
    public var rawValue: String

    public init(_ rawValue: String) {
        self.rawValue = rawValue
    }

    public init(rawValue: String) {
        self.rawValue = rawValue
    }

    public var hashValue: Int {
        rawValue.hashValue
    }

    public func hash(into hasher: inout Hasher) {
        rawValue.hash(into: &hasher)
    }

    public static func == (lhs: AttrKey, rhs: AttrKey) -> Bool {
        lhs.rawValue == rhs.rawValue
    }

    public static func == (lhs: AttrKey, rhs: String) -> Bool {
        lhs.rawValue == rhs
    }
}

private class ObjectAttrMapX: NSObject {
    public var map: [String: Any] = [:]

}

private var attr_key_x: UnsafeMutableRawPointer = malloc(1)

extension NSObject {

    public func require<T: Any>(attr: AttrKey, _ block: () -> T) -> T {
        if let a: T = self[attr] {
            return a
        }
        let b = block()
        self[attr] = b
        return b
    }

    public subscript<T: Any>(_ key: AttrKey) -> T? {
        get {
            _attrs.map[key.rawValue] as? T
        }
        set {
            if let v = newValue {
                _attrs.map[key.rawValue] = v
            } else {
                _attrs.map.removeValue(forKey: key.rawValue)
            }
        }
    }

    public func getAttr<T: Any>(key: String) -> T? {
        return _attrs.map[key] as? T
    }
    public func setAttr(key: String, value: Any?) {
        if let v = value {
            _attrs.map[key] = v
        } else {
            _attrs.map.removeValue(forKey: key)
        }
    }
    public func removeAttr<T: Any>(key: String) -> T? {
        _attrs.map.removeValue(forKey: key) as? T
    }

    fileprivate var _attrs: ObjectAttrMapX {
        if let m = objc_getAssociatedObject(self, attr_key_x) as? ObjectAttrMapX {
            return m
        } else {
            let map = ObjectAttrMapX()
            objc_setAssociatedObject(self, attr_key_x, map, .OBJC_ASSOCIATION_RETAIN)
            return map
        }

    }
}
