import Foundation

extension FileHandle {
    public static func openForWrite(url: URL, append: Bool) throws -> FileHandle? {
        let e = url.exists
        if e.isDir {
            throw HareError("Exist Directory")
        }
        try url.parentURL.dirCreate()
        if !e.exists {
            try url.fileCreate()
        }

        let fh: FileHandle = try FileHandle(forWritingTo: url)
        if append {
            let _ = try fh.seekEnd()
        }
        return fh
    }

    public static func openForWrite(path: String, append: Bool) throws -> FileHandle? {
        let e = path.exists
        if e.isDir {
            throw HareError("Exist Directory")
        }
        try path.parentPath.dirCreate()
        if !e.exists {
            try path.fileCreate()
        }

        guard let fh: FileHandle = FileHandle(forWritingAtPath: path) else {
            throw HareError("Open File Failed: \(path)")
        }
        if append {
            let _ = try fh.seekEnd()
        }
        return fh
    }

    public func seekEnd() throws -> UInt64 {
        if #available(macOS 10.15.4, iOS 13.4, *) {
            return try self.seekToEnd()
        } else {
            return self.seekToEndOfFile()
        }
    }
}

public struct ExistsResult {
    public let exists: Bool
    private let isDirectory: Bool

    public init(exists: Bool, isDirectory: Bool) {
        self.exists = exists
        self.isDirectory = isDirectory
    }

    public var isDir: Bool {
        exists && isDirectory
    }
    public var isFile: Bool {
        exists && !isDirectory
    }
}
