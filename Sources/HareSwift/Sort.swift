//
// Created by entaoyang@163.com on 2022/10/2.
//

import Foundation

public extension Sequence {
    @available(iOS 15.0, macOS 12.0, *)
    func sorted<V: Comparable>(order: SortOrder, _ keyPath: KeyPath<Element, V>) -> [Element] {
        let s = Sorter(order: order, keyPath: keyPath)
        return self.sorted(using: s)
    }

    @available(iOS 15.0, macOS 12.0, *)
    func sorted<V: Comparable, V2: Comparable>(order: SortOrder, _ keyPath: KeyPath<Element, V>,
                                               order2: SortOrder, _ keyPath2: KeyPath<Element, V2>) -> [Element] {
        let s = Sorter(order: order, keyPath: keyPath)
        let s2 = Sorter(order: order2, keyPath: keyPath2)
        s.equalCallback = { a, b in
            s2.compare(a, b)
        }
        return self.sorted(using: s)
    }

    @available(iOS 15.0, macOS 12.0, *)
    func sorted<V: Comparable, V2: Comparable, V3: Comparable>(order: SortOrder, _ keyPath: KeyPath<Element, V>,
                                                               order2: SortOrder, _ keyPath2: KeyPath<Element, V2>,
                                                               order3: SortOrder, _ keyPath3: KeyPath<Element, V3>) -> [Element] {
        let s = Sorter(order: order, keyPath: keyPath)
        let s2 = Sorter(order: order2, keyPath: keyPath2)
        let s3 = Sorter(order: order3, keyPath: keyPath3)
        s.equalCallback = { a, b in
            s2.compare(a, b)
        }
        s2.equalCallback = { a, b in
            s3.compare(a, b)
        }
        return self.sorted(using: s)
    }
}

public extension Array {

    @discardableResult
    @available(iOS 15.0, macOS 12.0, *)
    mutating func sort<V: Comparable>(order: SortOrder, _ keyPath: KeyPath<Element, V>) -> Self {
        let s = Sorter(order: order, keyPath: keyPath)
        self.sort(using: s)
        return self
    }

    @discardableResult
    @available(iOS 15.0, macOS 12.0, *)
    mutating func sort<V: Comparable, V2: Comparable>(order: SortOrder, _ keyPath: KeyPath<Element, V>,
                                                      order2: SortOrder, _ keyPath2: KeyPath<Element, V2>) -> Self {
        let s = Sorter(order: order, keyPath: keyPath)
        let s2 = Sorter(order: order2, keyPath: keyPath2)
        s.equalCallback = { a, b in
            s2.compare(a, b)
        }
        self.sort(using: s)
        return self
    }

    @discardableResult
    @available(iOS 15.0, macOS 12.0, *)
    mutating func sort<V: Comparable, V2: Comparable, V3: Comparable>(order: SortOrder, _ keyPath: KeyPath<Element, V>,
                                                                      order2: SortOrder, _ keyPath2: KeyPath<Element, V2>,
                                                                      order3: SortOrder, _ keyPath3: KeyPath<Element, V3>) -> Self {
        let s = Sorter(order: order, keyPath: keyPath)
        let s2 = Sorter(order: order2, keyPath: keyPath2)
        let s3 = Sorter(order: order3, keyPath: keyPath3)
        s.equalCallback = { a, b in
            s2.compare(a, b)
        }
        s2.equalCallback = { a, b in
            s3.compare(a, b)
        }
        self.sort(using: s)
        return self
    }

}

@available(iOS 15.0, macOS 12.0, *)
public class Sorter<T: Any, V: Any & Comparable>: NSObject, SortComparator {
    public typealias Compared = T
    public var order: SortOrder
    let keyPath: KeyPath<T, V>

    var equalCallback: (T, T) -> ComparisonResult = { _, _ in
        .orderedDescending
    }

    public init(order: SortOrder, keyPath: KeyPath<T, V>) {
        self.keyPath = keyPath
        self.order = order
    }

    public func compare(_ lhs: T, _ rhs: T) -> ComparisonResult {
        if order == .forward {
            if lhs[keyPath: keyPath] < rhs[keyPath: keyPath] {
                return .orderedAscending
            }
            if lhs[keyPath: keyPath] > rhs[keyPath: keyPath] {
                return .orderedDescending
            }
        } else {
            if rhs[keyPath: keyPath] < lhs[keyPath: keyPath] {
                return .orderedAscending
            }
            if rhs[keyPath: keyPath] > lhs[keyPath: keyPath] {
                return .orderedDescending
            }
        }
        return equalCallback(lhs, rhs)
    }

}

public extension Array {

    @discardableResult
    mutating func sort<T: Comparable, T2: Comparable>(asc: Bool, keyPath: KeyPath<Element, T>, keyPath2: KeyPath<Element, T2>) -> Self {
        if asc {
            self.sort { a, b in
                a[keyPath: keyPath] == b[keyPath: keyPath] ? a[keyPath: keyPath2] < b[keyPath: keyPath2] : a[keyPath: keyPath] < b[keyPath: keyPath]
            }
        } else {
            self.sort { a, b in
                b[keyPath: keyPath] == a[keyPath: keyPath] ? b[keyPath: keyPath2] < a[keyPath: keyPath2] : b[keyPath: keyPath] < a[keyPath: keyPath]
            }
        }
        return self
    }

    @discardableResult
    mutating func sort<T: Comparable>(asc: Bool, keyPath: KeyPath<Element, T>) -> Self {
        if asc {
            self.sort { a, b in
                a[keyPath: keyPath] < b[keyPath: keyPath]
            }
        } else {
            self.sort { a, b in
                b[keyPath: keyPath] < a[keyPath: keyPath]
            }
        }
        return self
    }
}

public extension Sequence {

    func sorted<T: Comparable, T2: Comparable>(asc: Bool, keyPath: KeyPath<Element, T>, keyPath2: KeyPath<Element, T2>) -> [Element] {
        if asc {
            return self.sorted { a, b in
                a[keyPath: keyPath] == b[keyPath: keyPath] ? a[keyPath: keyPath2] < b[keyPath: keyPath2] : a[keyPath: keyPath] < b[keyPath: keyPath]
            }
        } else {
            return self.sorted { a, b in
                b[keyPath: keyPath] == a[keyPath: keyPath] ? b[keyPath: keyPath2] < a[keyPath: keyPath2] : b[keyPath: keyPath] < a[keyPath: keyPath]
            }
        }
    }

    func sorted<T: Comparable>(asc: Bool, keyPath: KeyPath<Element, T>) -> [Element] {

        if asc {
            return self.sorted { a, b in
                a[keyPath: keyPath] < b[keyPath: keyPath]
            }
        } else {
            return self.sorted { a, b in
                b[keyPath: keyPath] < a[keyPath: keyPath]
            }
        }
    }

}