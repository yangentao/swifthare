import Foundation

public class FileX {
    public let handle: FileHandle

    public init(handle: FileHandle) {
        self.handle = handle
    }
    public static func open(reading: URL) throws -> FileX {
        return FileX(handle: try FileHandle(forReadingFrom: reading))
    }

    public static func open(writing url: URL) throws -> FileX {
        if let h = try FileHandle.openForWrite(url: url, append: false) {
            return FileX(handle: h)
        }
        throw HareError("Open File Failed")
    }

    public static func open(appending url: URL) throws -> FileX {
        if let h = try FileHandle.openForWrite(url: url, append: true) {
            return FileX(handle: h)
        }
        throw HareError("Open File Failed")
    }
    deinit {
        try? handle.close()
    }

    public func flush() {
        try? handle.synchronize()
    }

    public func close() {
        try? handle.close()
    }

    public var availableData: Data {
        self.handle.availableData
    }

    public func write(data: Data) {
        self.handle.write(data)
    }

    public func readData() -> Data {
        self.handle.readDataToEndOfFile()
    }

    public func readData(length: Int) -> Data {
        self.handle.readData(ofLength: length)
    }

    public var offsetInFile: UInt64 {
        self.handle.offsetInFile
    }

    public func seekEnd() -> ULong {
        self.handle.seekToEndOfFile()
    }

    public func seek(to n: ULong) {
        self.handle.seek(toFileOffset: n)
    }

    public func seek(offset: UInt64) throws {
        try self.handle.seek(toOffset: offset)
    }

    public func readText(_ enc: String.Encoding = .utf8) -> String? {
        self.readData().string(enc)
    }

    public func write(text: String, encoding: String.Encoding = .utf8) throws {
        if let d = text.data(using: encoding) {
            self.write(data: d)
        } else {
            throw HareError("text to data failed")
        }
    }
}
