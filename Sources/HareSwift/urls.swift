//
// Created by entaoyang@163.com on 2022/9/22.
//

import Foundation

extension URLComponents {
    public func param(name: String) -> String? {
        self.queryItems?.first {
            $0.name == name
        }?
        .value
    }

    public mutating func param(name: String, value: String?) {
        let p = URLQueryItem(name: name, value: value)
        if self.queryItems == nil {
            self.queryItems = [p]
        } else {
            self.queryItems?.append(p)
        }
    }
}

extension URL {

    public var queryMap: [String: String] {
        var map = [String: String]()
        if let c = URLComponents(url: self, resolvingAgainstBaseURL: false), let ls = c.queryItems {
            for item in ls {
                map[item.name] = item.value ?? ""
            }
        }
        return map
    }
}
