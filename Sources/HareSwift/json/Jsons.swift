//
// Created by entaoyang@163.com on 2022/10/3.
//

import Foundation

public class Json {
    static var castCallback: ((Any) throws -> JsonValue)? = nil
}

public enum JsonType: String {
    case jnull = "null"
    case jbool = "bool"
    case jnum = "num"
    case jstring = "string"
    case jarray = "array"
    case jobject = "object"
}

public class JsonValue: CustomStringConvertible {
    public let type: JsonType

    public init(_ type: JsonType) {
        self.type = type
    }

    open var jsonText: String {
        fatalError("NOT IMPLEMENT")
    }

    public var description: String {
        return jsonText
    }
    public subscript(_ index: Int) -> JsonValue {
        return JsonNull.shared
    }
    public subscript(_ key: String) -> JsonValue {
        return JsonNull.shared
    }
}

public class JsonNull: JsonValue {
    private init() {
        super.init(.jnull)
    }

    open override var jsonText: String {
        return "null"
    }
    public static let shared = JsonNull()
}

public class JsonBool: JsonValue, ExpressibleByBooleanLiteral {
    public let value: Bool

    public init(_ value: Bool) {
        self.value = value
        super.init(.jbool)
    }

    required public convenience init(booleanLiteral value: Bool) {
        self.init(value)
    }

    open override var jsonText: String {
        return "\(value)"
    }
}

public class JsonNum: JsonValue, ExpressibleByIntegerLiteral, ExpressibleByFloatLiteral {

    public let value: NSNumber
    var hasDot: Bool = false

    public init(_ value: NSNumber) {
        self.value = value
        super.init(.jnum)
    }

    public convenience init<T: Numeric>(_ v: T) {
        self.init(v.toNSNumber)
    }

    public required convenience init(integerLiteral value: Int) {
        self.init(value.toNSNumber)
    }

    public required convenience init(floatLiteral value: Double) {
        self.init(value.toNSNumber)
    }

    open override var jsonText: String {
        return "\(value)"
    }
}

public class JsonString: JsonValue, ExpressibleByStringLiteral, ExpressibleByExtendedGraphemeClusterLiteral, ExpressibleByUnicodeScalarLiteral {

    public let value: String

    public init(_ value: String) {
        self.value = value
        super.init(.jstring)
    }

    public required convenience init(stringLiteral value: String) {
        self.init(value)
    }

    public required convenience init(extendedGraphemeClusterLiteral value: String) {
        self.init(value)
    }

    public required convenience init(unicodeScalarLiteral value: String) {
        self.init(value)
    }

    public convenience init(data: Data) {
        self.init(data.string() ?? "")
    }

    open override var jsonText: String {
        value.quoted
    }
}

public class JsonArray: JsonValue, Sequence, ExpressibleByArrayLiteral {
    public var array: [JsonValue] = []

    public init() {
        super.init(.jarray)
    }

    public required convenience init(arrayLiteral elements: Any?...) {
        self.init()
        array.reserveCapacity(elements.count)
        for e in elements {
            array.append(anyToJson(e))
        }
    }

    public func makeIterator() -> IndexingIterator<[JsonValue]> {
        array.makeIterator()
    }

    public var count: Int {
        array.count
    }

    public func get(_ index: Int) -> JsonValue? {
        return array.get(index)

    }

    public func set(_ index: Int, value: JsonValue) {
        if array.indices.contains(index) {
            array[index] = value
        } else if array.count == index {
            array.append(value)
        }
    }

    public override subscript(_ index: Int) -> JsonValue {
        get {
            array.get(index) ?? JsonNull.shared
        }
        set {
            array.set(index, value: newValue)
        }

    }
    open override var jsonText: String {
        "[" + array.map {
            $0.jsonText
        }.joined(separator: ", ") + "]"
    }
}

public class JsonObject: JsonValue, Sequence, ExpressibleByDictionaryLiteral {
    public var map: [String: JsonValue] = [:]

    public init() {
        super.init(.jobject)
    }

    public required convenience init(dictionaryLiteral elements: (String, Any?)...) {
        self.init()
        map.reserveCapacity(elements.count)
        for p in elements {
            map[p.0] = anyToJson(p.1)
        }
    }

    public func makeIterator() -> DictionaryIterator<String, JsonValue> {
        map.makeIterator()
    }

    public var count: Int {
        map.count
    }

    public func get(_ key: String) -> JsonValue? {
        map[key]
    }

    public func set(_ key: String, value: Any?) {
        map[key] = anyToJson(value ?? nil)
    }

    public override subscript(_ key: String) -> JsonValue {
        get {
            map[key] ?? JsonNull.shared
        }
        set {
            map[key] = newValue
        }
    }
    open override var jsonText: String {
        "{" + map.map {
            $0.key.quoted + ":" + $0.value.jsonText
        }.joined(separator: ", ") + "}"
    }

}

public extension JsonArray {

    func append(_ v: JsonValue) {
        array.append(v)
    }

    func append(all: [JsonValue]) {
        array.append(contentsOf: all)
    }

    func append<T: Any>(all: [T], _ block: (T) -> JsonValue) {
        array.reserveCapacity(array.count + all.count)
        for item in all {
            array.append(block(item))
        }
    }
}

public extension JsonObject {

}

public extension JsonNum {
    var intValue: Int {
        self.value.intValue
    }
    var longValue: Int64 {
        self.value.int64Value
    }
    var uintValue: UInt {
        self.value.uintValue
    }
    var ulongValue: UInt64 {
        self.value.uint64Value
    }

    var floatValue: Float {
        self.value.floatValue
    }
    var doubleValue: Double {
        self.value.doubleValue
    }

    convenience init(_ v: UInt32) {
        self.init(NSNumber(value: v))
    }

    convenience init(_ v: UInt64) {
        self.init(NSNumber(value: v))
    }

    convenience init(_ v: Int32) {
        self.init(NSNumber(value: v))
    }

    convenience init(_ v: Int64) {
        self.init(NSNumber(value: v))
    }

    convenience init(_ v: Float) {
        self.init(NSNumber(value: v))
    }

    convenience init(_ v: Double) {
        self.init(NSNumber(value: v))
    }
}

public extension JsonValue {

}



public extension JsonValue {

    var asString: String? {
        (self as? JsonString)?.value
    }
    var asBool: Bool? {
        (self as? JsonBool)?.value
    }
    var asNumber: NSNumber? {
        (self as? JsonNum)?.value
    }
}

fileprivate func anyToJson(_ value: Any?) -> JsonValue {
    guard let v = value ?? nil else {
        return JsonNull.shared
    }
    switch v {
    case is NSNull:
        return JsonNull.shared
    case let yv as JsonValue:
        return yv
    case let s as String:
        return JsonString(s)
    case let b as Bool:
        return JsonBool(b)
    case let f as CGFloat:
        return JsonNum(Double(f))
    case let num as NSNumber:
        return JsonNum(num)
    case let dec as Decimal:
        return JsonNum(NSDecimalNumber(decimal: dec))
    case let data as Data:
        if let s = data.string() {
            return JsonString(s)
        }
        return JsonNull.shared
//    case let pairs as Array<KeyAny>:
//        let yo = JsonObject()
//        yo.array.reserveCapacity(pairs.count)
//        for p in pairs {
//            if let pv = p.value ?? nil {
//                yo.put(p.key, pv)
//            } else {
//                yo.putNull(p.key)
//            }
//        }
//        return yo
    case let map as [String: Any?]:
        let yo = JsonObject()
        yo.map = dicToJsonDic(map)
        return yo
    case let ar as Array<Any?>:
        let ya = JsonArray()
        ya.array = arrayToJsonArray(es: ar)
        return ya

    default:
        if let c = Json.castCallback {
            do {
                return try c(v)
            } catch {

            }
        }
        let m = Mirror(reflecting: v)
        print(m.description)
        print(m.displayStyle ?? "null")
        print(m.subjectType)
        dump(v)
        return JsonNull.shared
    }
}

fileprivate func dicToJsonDic(_ es: [String: Any?]) -> [String: JsonValue] {
    var dic = [String: JsonValue]()
    dic.reserveCapacity(es.count)
    for (k, v) in es {
        if let vv = v ?? nil {
            dic[k] = anyToJson(vv)
        } else {
            dic[k] = JsonNull.shared
        }
    }
    return dic
}

fileprivate func arrayToJsonArray(es: [Any?]) -> [JsonValue] {
    var data = [JsonValue]()
    data.reserveCapacity(es.count)
    for e in es {
        if let v = e ?? nil {
            data.append(anyToJson(v))
        } else {
            data.append(JsonNull.shared)
        }
    }
    return data
}

