//
// Created by entaoyang@163.com on 2022/10/3.
//

import Foundation

public extension Json {
    static func encode<T>(text value: T) -> String where T: Encodable {
        encode(value: value).jsonText
    }

    static func encode<T>(value: T) -> JsonValue where T: Encodable {
        let e = JsonEncoder()
        do {
            try value.encode(to: e)
        } catch let err {
            print(err)
        }
        return e.jsonValue
    }
}

fileprivate class JsonEncoder: Encoder {
    var codingPath: [CodingKey] = []

    var userInfo: [CodingUserInfoKey: Any] = [:]

    var jsonValue: JsonValue = JsonNull.shared

    init() {

    }

    func container<Key>(keyedBy type: Key.Type) -> KeyedEncodingContainer<Key> where Key: CodingKey {
        self.jsonValue = JsonObject()
        return KeyedEncodingContainer(JsonKeyedEncodingContainer<Key>(self))
    }

    func unkeyedContainer() -> UnkeyedEncodingContainer {
        self.jsonValue = JsonArray()
        return JsonArrayEncodingContainer(self)
    }

    func singleValueContainer() -> SingleValueEncodingContainer {
        JsonSingleEncodingContainer(self)
    }

}

fileprivate class JsonKeyedEncodingContainer<K: CodingKey>: KeyedEncodingContainerProtocol {
    typealias Key = K
    var codingPath: [CodingKey] = []
    let encoder: JsonEncoder
    var jsonObject: JsonObject

    init(_ en: JsonEncoder) {
        self.encoder = en
        jsonObject = en.jsonValue as! JsonObject
    }

    func encodeNil(forKey key: K) throws {
        jsonObject.map[key.stringValue] = JsonNull.shared
    }

    func encode(_ value: Bool, forKey key: K) throws {
        jsonObject.map[key.stringValue] = JsonBool(value)
    }

    func encode(_ value: String, forKey key: K) throws {
        jsonObject.map[key.stringValue] = JsonString(value)
    }

    func encode(_ value: Double, forKey key: K) throws {
        jsonObject.map[key.stringValue] = JsonNum(NSNumber(value: value))
    }

    func encode(_ value: Float, forKey key: K) throws {
        jsonObject.map[key.stringValue] = JsonNum(NSNumber(value: value))
    }

    func encode(_ value: Int, forKey key: K) throws {
        jsonObject.map[key.stringValue] = JsonNum(NSNumber(value: value))
    }

    func encode(_ value: Int8, forKey key: K) throws {
        jsonObject.map[key.stringValue] = JsonNum(NSNumber(value: value))
    }

    func encode(_ value: Int16, forKey key: K) throws {
        jsonObject.map[key.stringValue] = JsonNum(NSNumber(value: value))
    }

    func encode(_ value: Int32, forKey key: K) throws {
        jsonObject.map[key.stringValue] = JsonNum(NSNumber(value: value))
    }

    func encode(_ value: Int64, forKey key: K) throws {
        jsonObject.map[key.stringValue] = JsonNum(NSNumber(value: value))
    }

    func encode(_ value: UInt, forKey key: K) throws {
        jsonObject.map[key.stringValue] = JsonNum(NSNumber(value: value))
    }

    func encode(_ value: UInt8, forKey key: K) throws {
        jsonObject.map[key.stringValue] = JsonNum(NSNumber(value: value))
    }

    func encode(_ value: UInt16, forKey key: K) throws {
        jsonObject.map[key.stringValue] = JsonNum(NSNumber(value: value))
    }

    func encode(_ value: UInt32, forKey key: K) throws {
        jsonObject.map[key.stringValue] = JsonNum(NSNumber(value: value))
    }

    func encode(_ value: UInt64, forKey key: K) throws {
        jsonObject.map[key.stringValue] = JsonNum(NSNumber(value: value))
    }

    func encode<T>(_ value: T, forKey key: K) throws where T: Encodable {
        let e = JsonEncoder()
        try value.encode(to: e)
        jsonObject.map[key.stringValue] = e.jsonValue
    }

    func nestedContainer<NestedKey>(keyedBy keyType: NestedKey.Type, forKey key: K) -> KeyedEncodingContainer<NestedKey> where NestedKey: CodingKey {
        let e = JsonEncoder()
        e.jsonValue = JsonObject()
        jsonObject.map[key.stringValue] = e.jsonValue
        return KeyedEncodingContainer(JsonKeyedEncodingContainer<NestedKey>(e))
    }

    func nestedUnkeyedContainer(forKey key: K) -> UnkeyedEncodingContainer {
        let e = JsonEncoder()
        e.jsonValue = JsonArray()
        jsonObject.map[key.stringValue] = e.jsonValue
        return JsonArrayEncodingContainer(e)
    }

    func superEncoder() -> Encoder {
        let e = JsonEncoder()
        e.jsonValue = self.jsonObject
        return e
    }

    func superEncoder(forKey key: K) -> Encoder {
        let e = JsonEncoder()
        e.jsonValue = JsonObject()
        jsonObject.map[key.stringValue] = e.jsonValue
        return e
    }

}

fileprivate class JsonArrayEncodingContainer: UnkeyedEncodingContainer {

    var codingPath: [CodingKey] = []

    var count: Int {
        jsonArray.array.count
    }
    let jsonArray: JsonArray
    let encoder: JsonEncoder

    init(_ encoder: JsonEncoder) {
        self.encoder = encoder
        self.jsonArray = self.encoder.jsonValue as! JsonArray
    }

    func nestedContainer<NestedKey>(keyedBy keyType: NestedKey.Type) -> KeyedEncodingContainer<NestedKey> where NestedKey: CodingKey {
        let e = JsonEncoder()
        e.jsonValue = JsonObject()
        self.jsonArray.array += e.jsonValue
        return KeyedEncodingContainer(JsonKeyedEncodingContainer<NestedKey>(e))
    }

    func nestedUnkeyedContainer() -> UnkeyedEncodingContainer {
        let e = JsonEncoder()
        e.jsonValue = JsonArray()
        self.jsonArray.array += e.jsonValue
        return JsonArrayEncodingContainer(e)
    }

    func superEncoder() -> Encoder {
        let e = JsonEncoder()
        e.jsonValue = self.jsonArray
        return e
    }

    func encodeNil() throws {
        jsonArray.array += JsonNull.shared
    }

    func encode(_ value: Bool) throws {
        jsonArray.array += JsonBool(value)
    }

    func encode(_ value: String) throws {
        jsonArray.array += JsonString(value)
    }

    func encode(_ value: Double) throws {
        jsonArray.array += JsonNum(NSNumber(value: value))
    }

    func encode(_ value: Float) throws {
        jsonArray.array += JsonNum(NSNumber(value: value))
    }

    func encode(_ value: Int) throws {
        jsonArray.array += JsonNum(NSNumber(value: value))
    }

    func encode(_ value: Int8) throws {
        jsonArray.array += JsonNum(NSNumber(value: value))
    }

    func encode(_ value: Int16) throws {
        jsonArray.array += JsonNum(NSNumber(value: value))
    }

    func encode(_ value: Int32) throws {
        jsonArray.array += JsonNum(NSNumber(value: value))
    }

    func encode(_ value: Int64) throws {
        jsonArray.array += JsonNum(NSNumber(value: value))
    }

    func encode(_ value: UInt) throws {
        jsonArray.array += JsonNum(NSNumber(value: value))
    }

    func encode(_ value: UInt8) throws {
        jsonArray.array += JsonNum(NSNumber(value: value))
    }

    func encode(_ value: UInt16) throws {
        jsonArray.array += JsonNum(NSNumber(value: value))
    }

    func encode(_ value: UInt32) throws {
        jsonArray.array += JsonNum(NSNumber(value: value))
    }

    func encode(_ value: UInt64) throws {
        jsonArray.array += JsonNum(NSNumber(value: value))
    }

    func encode<T>(_ value: T) throws where T: Encodable {
        let e = JsonEncoder()
        try value.encode(to: e)
        jsonArray.array += e.jsonValue
    }

}

fileprivate class JsonSingleEncodingContainer: SingleValueEncodingContainer {
    var codingPath: [CodingKey] = []

    let encoder: JsonEncoder

    init(_ encoder: JsonEncoder) {
        self.encoder = encoder
    }

    func encodeNil() throws {
        encoder.jsonValue = JsonNull.shared
    }

    func encode(_ value: Bool) throws {
        encoder.jsonValue = JsonBool(value)
    }

    func encode(_ value: String) throws {
        encoder.jsonValue = JsonString(value)
    }

    func encode(_ value: Double) throws {
        encoder.jsonValue = JsonNum(NSNumber(value: value))
    }

    func encode(_ value: Float) throws {
        encoder.jsonValue = JsonNum(NSNumber(value: value))
    }

    func encode(_ value: Int) throws {
        encoder.jsonValue = JsonNum(NSNumber(value: value))
    }

    func encode(_ value: Int8) throws {
        encoder.jsonValue = JsonNum(NSNumber(value: value))
    }

    func encode(_ value: Int16) throws {
        encoder.jsonValue = JsonNum(NSNumber(value: value))
    }

    func encode(_ value: Int32) throws {
        encoder.jsonValue = JsonNum(NSNumber(value: value))
    }

    func encode(_ value: Int64) throws {
        encoder.jsonValue = JsonNum(NSNumber(value: value))
    }

    func encode(_ value: UInt) throws {
        encoder.jsonValue = JsonNum(NSNumber(value: value))
    }

    func encode(_ value: UInt8) throws {
        encoder.jsonValue = JsonNum(NSNumber(value: value))
    }

    func encode(_ value: UInt16) throws {
        encoder.jsonValue = JsonNum(NSNumber(value: value))
    }

    func encode(_ value: UInt32) throws {
        encoder.jsonValue = JsonNum(NSNumber(value: value))
    }

    func encode(_ value: UInt64) throws {
        encoder.jsonValue = JsonNum(NSNumber(value: value))
    }

    func encode<T>(_ value: T) throws where T: Encodable {
        let e = JsonEncoder()
        try value.encode(to: e)
        encoder.jsonValue = e.jsonValue
    }

}