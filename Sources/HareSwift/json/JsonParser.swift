//
// Created by entaoyang@163.com on 2022/10/3.
//

import Foundation

public extension Json {
    static func parse(value json: String) throws -> JsonValue {
        let p = JsonParser(json)
        return try p.parseValue()
    }

    static func parse(array json: String) throws -> JsonArray {
        let v = try JsonParse(value: json)
        if let a = v as? JsonArray {
            return a
        }
        throw JsonError.typeMistake
    }

    static func parse(object json: String) throws -> JsonObject {
        let v = try JsonParse(value: json)
        if let a = v as? JsonObject {
            return a
        }
        throw JsonError.typeMistake
    }
}

func JsonParse(value json: String) throws -> JsonValue {
    let p = JsonParser(json)
    return try p.parseValue()
}

func JsonParse(array json: String) throws -> JsonArray {
    let v = try JsonParse(value: json)
    if let a = v as? JsonArray {
        return a
    }
    throw JsonError.typeMistake
}

func JsonParse(object json: String) throws -> JsonObject {
    let v = try JsonParse(value: json)
    if let a = v as? JsonObject {
        return a
    }
    throw JsonError.typeMistake
}

public enum JsonError: String, Swift.Error {
    case parseError, typeMistake, unknownChar
}

fileprivate class JsonParser {
    let text: String
    let data: [Character]
    var current: Int = 0

    init(_ text: String) {
        self.text = text
        var ca = Array<Character>()
        ca.reserveCapacity(text.count)
        for c in text {
            ca.append(c)
        }
        data = ca
    }

    func parseValue() throws -> JsonValue {
        let v = try parse()
        if !shouldEnd() {
            throw JsonError.parseError
        }
        return v
    }

    private func parse() throws -> JsonValue {
        skipWhite()
        if end {
            throw JsonError.parseError
        }
        let ch = currentChar
        if ch == "{" {
            return try parseObject()
        }
        if ch == "[" {
            return try parseArray()
        }
        if ch == QUOTE {
            return try parseString()
        }
        if ch == "t" {
            return try parseTrue()
        }
        if ch == "f" {
            return try parseFalse()
        }
        if ch == "n" {
            return try parseNull()
        }
        if NUM_START.contains(ch) {
            return try parseNumber()
        }

        throw JsonError.parseError
    }

    private func parseArray() throws -> JsonArray {
        try tokenc("[")

        let ya = JsonArray()
        while !end {
            skipWhite()
            if currentChar == "]" {
                break
            }
            if currentChar == "," {
                next()
                continue
            }
            let yv = try parse()
            ya.array.append(yv)
        }
        try tokenc("]")
        return ya
    }

    private func parseObject() throws -> JsonObject {
        try tokenc("{")

        let yo = JsonObject()
        while !end {
            skipWhite()
            if currentChar == "}" {
                break
            }
            if currentChar == "," {
                next()
                continue
            }
            let key = try parseString()
            try tokenc(":")
            let yv = try parse()
            yo.map[key.value] = yv
        }
        try tokenc("}")
        return yo
    }

    private func parseString() throws -> JsonString {
        try tokenc(QUOTE)
        var text = ""
        var escing = false
        while !end {
            let ch = currentChar
            if !escing {
                if ch == QUOTE {
                    break
                }
                next()
                if ch == SLASH {
                    escing = true
                    continue
                }
                text.append(ch)
            } else {
                escing = false
                next()
                //["\"", "\\", "/", "b", "f", "n", "r", "t", "u"]
                switch ch {
                case QUOTE, SLASH, "/":
                    text.append(ch)
                case "n":
                    text.append("\n")
                case "r":
                    text.append("\r")
                case "t":
                    text.append("\t")
                case "u":
                    if current + 4 < data.count {
                        var us = ""
                        us.append(data[current + 0])
                        us.append(data[current + 1])
                        us.append(data[current + 2])
                        us.append(data[current + 3])
                        current += 4
                        if let hex = Int(us, radix: 16) {
                            if let us = UnicodeScalar(hex) {
                                text.append(Character(us))
                            } else {
                                throw JsonError.parseError
                            }
                        } else {
                            throw JsonError.parseError
                        }
                    } else {
                        throw JsonError.parseError
                    }
                    break
                default:
                    text.append(ch)
                }
            }

        }
        if escing {
            throw JsonError.parseError
        }
        try tokenc(QUOTE)
        return JsonString(text)
    }

    private func parseNumber() throws -> JsonValue {
        skipWhite()
        var text = ""
        while !end {
            let c = currentChar
            if !NUMS.contains(c) {
                break
            }
            text.append(c)
            next()
        }
        if text.isEmpty {
            throw JsonError.parseError
        }
        guard let d = Double(text) else {
            throw JsonError.parseError
        }
        let v = JsonNum(NSNumber(value: d))
        v.hasDot = text.contains(".")
        return v

    }

    private func parseTrue() throws -> JsonBool {
        try tokens("true")
        return JsonBool(true)
    }

    private func parseFalse() throws -> JsonBool {
        try tokens("false")
        return JsonBool(false)
    }

    private func parseNull() throws -> JsonNull {
        try tokens("null")
        return JsonNull.shared
    }

    private var end: Bool {
        current >= data.count
    }

    private func shouldEnd() -> Bool {
        self.skipWhite()
        return self.end
    }

    private func skipWhite() {
        while !end {
            if currentChar.isWhite {
                next()
            } else {
                return
            }
        }
    }

    private var currentChar: Character {
        data[current]
    }

    private func next() {
        current += 1
    }

    private func isChar(_ c: Character) -> Bool {
        currentChar == c
    }

    private func tokenc(_ c: Character) throws {
        skipWhite()
        if currentChar != c {
            throw JsonError.parseError
        }
        next()
        skipWhite()
    }

    private func tokens(_ cs: String) throws {
        skipWhite()
        for c in cs {
            if currentChar != c {
                throw JsonError.parseError
            }
            next()
        }
        skipWhite()
    }

}

fileprivate extension Character {
    var isWhite: Bool {
        WHITE.contains(self)
    }
}

func escape(json text: String) -> String {
    var n: Int = 0
    for c in text {
        if c == SLASH || c == QUOTE {
            n += 1
        }
    }
    if n == 0 {
        return text
    }
    var buf = String()
    buf.reserveCapacity(text.count + n)
    for c in text {
        if c == SLASH || c == QUOTE {
            buf.append(SLASH)
        }
        buf.append(c)
    }
    return buf
}

fileprivate let SLASH: Character = "\\"
fileprivate let QUOTE: Character = "\""
fileprivate let CR: Character = "\r"
fileprivate let LF: Character = "\n"
fileprivate let SP: Character = " "
fileprivate let TAB: Character = "\t"
fileprivate let WHITE: [Character] = [CR, LF, SP, TAB]
fileprivate let NUM_START: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "-"]
fileprivate let NUMS: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ".", "e", "E", "+", "-"]
fileprivate let ESCAP: Set<Character> = [QUOTE, SLASH, "/", "b", "f", "n", "r", "t", "u"]
