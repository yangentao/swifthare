//
// Created by entaoyang@163.com on 2022/10/3.
//

import Foundation

public extension Json {
    static func decode<T>(text: String) throws -> T where T: Decodable {
        let v = try JsonParse(value: text)
        return try decode(value: v)
    }

    static func decode<T>(value: JsonValue) throws -> T where T: Decodable {
        let d = JsonDecoder(value: value)
        return try T.init(from: d)
    }
}

fileprivate class JsonDecoder: Decoder {
    var codingPath: [CodingKey] = []

    var userInfo: [CodingUserInfoKey: Any] = [:]
    let jsonValue: JsonValue

    init(value: JsonValue) {
        self.jsonValue = value
    }

    func container<Key>(keyedBy type: Key.Type) throws -> KeyedDecodingContainer<Key> where Key: CodingKey {
        try KeyedDecodingContainer(JsonKeyedDecodingContainer<Key>(self))
    }

    func unkeyedContainer() throws -> UnkeyedDecodingContainer {
        try JsonArrayDecodingContainer(self)
    }

    func singleValueContainer() throws -> SingleValueDecodingContainer {
        JsonSingleDecodingContainer(self)
    }

}

fileprivate class JsonKeyedDecodingContainer<K: CodingKey>: KeyedDecodingContainerProtocol {
    typealias Key = K
    var codingPath: [CodingKey] = []
    var allKeys: [K]
    let decoder: JsonDecoder
    let jo: JsonObject

    init(_ d: JsonDecoder) throws {
        self.decoder = d
        if let j = d.jsonValue as? JsonObject {
            self.jo = j
            allKeys = jo.map.keys.compactMap {
                K(stringValue: $0)
            }
        } else {
            throw typeMismatch(type: JsonObject.self, path: codingPath, desc: d.jsonValue.jsonText)
        }

    }

    private func value(of key: K) -> JsonValue {
        jo.map[key.stringValue] ?? JsonNull.shared
    }

    func contains(_ key: K) -> Bool {
        return allKeys.contains {
            $0.stringValue == key.stringValue
        }
    }

    func decodeNil(forKey key: K) throws -> Bool {
        if let v = jo.map[key.stringValue] {
            return v is JsonNull
        }
        return true
    }

    func decode(_ type: Bool.Type, forKey key: K) throws -> Bool {
        let v = value(of: key)
        switch v {
        case let m as JsonBool:
            return m.value
        default:
            throw  typeMismatch(type: type, path: codingPath, desc: v.jsonText)
        }
    }

    func decode(_ type: String.Type, forKey key: K) throws -> String {
        let v = value(of: key)
        switch v {
        case let m as JsonString:
            return m.value
        default:
            return ""
        }
    }

    func decode(_ type: Double.Type, forKey key: K) throws -> Double {
        let v = value(of: key)
        switch v {
        case let m as JsonNum:
            return Double(truncating: m.value)
        default:
            throw  typeMismatch(type: type, path: codingPath, desc: v.jsonText)
        }
    }

    func decode(_ type: Float.Type, forKey key: K) throws -> Float {
        let v = value(of: key)
        switch v {
        case let m as JsonNum:
            return Float(truncating: m.value)
        default:
            throw  typeMismatch(type: type, path: codingPath, desc: v.jsonText)
        }
    }

    func decode(_ type: Int.Type, forKey key: K) throws -> Int {
        let v = value(of: key)
        switch v {
        case let m as JsonNum:
            return Int(truncating: m.value)
        default:
            throw  typeMismatch(type: type, path: codingPath, desc: v.jsonText)
        }
    }

    func decode(_ type: Int8.Type, forKey key: K) throws -> Int8 {
        let v = value(of: key)
        switch v {
        case let m as JsonNum:
            return Int8(truncating: m.value)
        default:
            throw  typeMismatch(type: type, path: codingPath, desc: v.jsonText)
        }
    }

    func decode(_ type: Int16.Type, forKey key: K) throws -> Int16 {
        let v = value(of: key)
        switch v {
        case let m as JsonNum:
            return Int16(truncating: m.value)
        default:
            throw  typeMismatch(type: type, path: codingPath, desc: v.jsonText)
        }
    }

    func decode(_ type: Int32.Type, forKey key: K) throws -> Int32 {
        let v = value(of: key)
        switch v {
        case let m as JsonNum:
            return Int32(truncating: m.value)
        default:
            throw  typeMismatch(type: type, path: codingPath, desc: v.jsonText)
        }
    }

    func decode(_ type: Int64.Type, forKey key: K) throws -> Int64 {
        let v = value(of: key)
        switch v {
        case let m as JsonNum:
            return Int64(truncating: m.value)
        default:
            throw  typeMismatch(type: type, path: codingPath, desc: v.jsonText)
        }
    }

    func decode(_ type: UInt.Type, forKey key: K) throws -> UInt {
        let v = value(of: key)
        switch v {
        case let m as JsonNum:
            return UInt(truncating: m.value)
        default:
            throw  typeMismatch(type: type, path: codingPath, desc: v.jsonText)
        }
    }

    func decode(_ type: UInt8.Type, forKey key: K) throws -> UInt8 {
        let v = value(of: key)
        switch v {
        case let m as JsonNum:
            return UInt8(truncating: m.value)
        default:
            throw  typeMismatch(type: type, path: codingPath, desc: v.jsonText)
        }
    }

    func decode(_ type: UInt16.Type, forKey key: K) throws -> UInt16 {
        let v = value(of: key)
        switch v {
        case let m as JsonNum:
            return UInt16(truncating: m.value)
        default:
            throw  typeMismatch(type: type, path: codingPath, desc: v.jsonText)
        }
    }

    func decode(_ type: UInt32.Type, forKey key: K) throws -> UInt32 {
        let v = value(of: key)
        switch v {
        case let m as JsonNum:
            return UInt32(truncating: m.value)
        default:
            throw  typeMismatch(type: type, path: codingPath, desc: v.jsonText)
        }
    }

    func decode(_ type: UInt64.Type, forKey key: K) throws -> UInt64 {
        let v = value(of: key)
        switch v {
        case let m as JsonNum:
            return UInt64(truncating: m.value)
        default:
            throw  typeMismatch(type: type, path: codingPath, desc: v.jsonText)
        }
    }

    func decode<T>(_ type: T.Type, forKey key: K) throws -> T where T: Decodable {
        let v = value(of: key)
        return try type.init(from: JsonDecoder(value: v))
    }

    func nestedContainer<NestedKey>(keyedBy type: NestedKey.Type, forKey key: K) throws -> KeyedDecodingContainer<NestedKey> where NestedKey: CodingKey {
        let v = value(of: key)
        return try KeyedDecodingContainer(JsonKeyedDecodingContainer<NestedKey>(JsonDecoder(value: v)))
    }

    func nestedUnkeyedContainer(forKey key: K) throws -> UnkeyedDecodingContainer {
        try JsonArrayDecodingContainer(JsonDecoder(value: value(of: key)))
    }

    func superDecoder() throws -> Decoder {
        self.decoder
    }

    func superDecoder(forKey key: K) throws -> Decoder {
        JsonDecoder(value: value(of: key))
    }

}

fileprivate class JsonArrayDecodingContainer: UnkeyedDecodingContainer {

    var codingPath: [CodingKey] = []

    var currentIndex: Int = 0

    let decoder: JsonDecoder
    let jarray: JsonArray

    var isAtEnd: Bool {
        currentIndex >= jarray.count
    }
    var count: Int? {
        jarray.count
    }

    init(_ d: JsonDecoder) throws {
        self.decoder = d
        if let ar = d.jsonValue as? JsonArray {
            self.jarray = ar
        } else {
            throw typeMismatch(type: JsonArray.self, path: self.codingPath, desc: "decode array failed, source is NOT an array: \(d.jsonValue.jsonText)")
        }
    }

    func nestedContainer<NestedKey>(keyedBy type: NestedKey.Type) throws -> KeyedDecodingContainer<NestedKey> where NestedKey: CodingKey {
        return try KeyedDecodingContainer(JsonKeyedDecodingContainer<NestedKey>(JsonDecoder(value: nextValue())))
    }

    func nestedUnkeyedContainer() throws -> UnkeyedDecodingContainer {
        let jv = nextValue()
        let d = JsonDecoder(value: jv)
        return try JsonArrayDecodingContainer(d)
    }

    func superDecoder() throws -> Decoder {
        fatalError("NOT IMPLEMENT superDecoder")
    }

    func decodeNil() throws -> Bool {
        let jv = nextValue()
        return jv is JsonNull
    }

    func decode(_ type: String.Type) throws -> String {
        let jv = nextValue()
        switch jv {
        case let m as JsonString:
            return m.value
        default:
            return jv.jsonText
        }
    }

    func decode(_ type: Double.Type) throws -> Double {
        let jv = nextValue()
        switch jv {
        case let m as JsonNum:
            return Double(truncating: m.value)
        default:
            throw typeMismatch(type: type, path: codingPath, desc: jv.jsonText)
        }
    }

    func decode(_ type: Float.Type) throws -> Float {
        let jv = nextValue()
        switch jv {
        case let m as JsonNum:
            return Float(truncating: m.value)
        default:
            throw typeMismatch(type: type, path: codingPath, desc: jv.jsonText)
        }
    }

    func decode(_ type: Int.Type) throws -> Int {
        let jv = nextValue()
        switch jv {
        case let m as JsonNum:
            return Int(truncating: m.value)
        default:
            throw typeMismatch(type: type, path: codingPath, desc: jv.jsonText)
        }
    }

    func decode(_ type: Int8.Type) throws -> Int8 {
        let jv = nextValue()
        switch jv {
        case let m as JsonNum:
            return Int8(truncating: m.value)
        default:
            throw typeMismatch(type: type, path: codingPath, desc: jv.jsonText)
        }
    }

    func decode(_ type: Int16.Type) throws -> Int16 {
        let jv = nextValue()
        switch jv {
        case let m as JsonNum:
            return Int16(truncating: m.value)
        default:
            throw typeMismatch(type: type, path: codingPath, desc: jv.jsonText)
        }
    }

    func decode(_ type: Int32.Type) throws -> Int32 {
        let jv = nextValue()
        switch jv {
        case let m as JsonNum:
            return Int32(truncating: m.value)
        default:
            throw typeMismatch(type: type, path: codingPath, desc: jv.jsonText)
        }
    }

    func decode(_ type: Int64.Type) throws -> Int64 {
        let jv = nextValue()
        switch jv {
        case let m as JsonNum:
            return Int64(truncating: m.value)
        default:
            throw typeMismatch(type: type, path: codingPath, desc: jv.jsonText)
        }
    }

    func decode(_ type: UInt.Type) throws -> UInt {
        let jv = nextValue()
        switch jv {
        case let m as JsonNum:
            return UInt(truncating: m.value)
        default:
            throw typeMismatch(type: type, path: codingPath, desc: jv.jsonText)
        }
    }

    func decode(_ type: UInt8.Type) throws -> UInt8 {
        let jv = nextValue()
        switch jv {
        case let m as JsonNum:
            return UInt8(truncating: m.value)
        default:
            throw typeMismatch(type: type, path: codingPath, desc: jv.jsonText)
        }
    }

    func decode(_ type: UInt16.Type) throws -> UInt16 {
        let jv = nextValue()
        switch jv {
        case let m as JsonNum:
            return UInt16(truncating: m.value)
        default:
            throw typeMismatch(type: type, path: codingPath, desc: jv.jsonText)
        }
    }

    func decode(_ type: UInt32.Type) throws -> UInt32 {
        let jv = nextValue()
        switch jv {
        case let m as JsonNum:
            return UInt32(truncating: m.value)
        default:
            throw typeMismatch(type: type, path: codingPath, desc: jv.jsonText)
        }
    }

    func decode(_ type: UInt64.Type) throws -> UInt64 {
        let jv = nextValue()
        switch jv {
        case let m as JsonNum:
            return UInt64(truncating: m.value)
        default:
            throw typeMismatch(type: type, path: codingPath, desc: jv.jsonText)
        }
    }

    func decode(_ type: Bool.Type) throws -> Bool {
        let jv = nextValue()
        switch jv {
        case let m as JsonBool:
            return m.value
        default:
            throw typeMismatch(type: type, path: codingPath, desc: jv.jsonText)
        }
    }

    func decode<T>(_ type: T.Type) throws -> T where T: Decodable {
        let jv = nextValue()
        return try type.init(from: JsonDecoder(value: jv))
    }

    func nextValue() -> JsonValue {
        let jv = jarray[currentIndex]
        currentIndex += 1
        return jv
    }

}

fileprivate class JsonSingleDecodingContainer: SingleValueDecodingContainer {
    var codingPath: [CodingKey] = []
    let decoder: JsonDecoder
    let jv: JsonValue

    init(_ d: JsonDecoder) {
        self.decoder = d
        self.jv = d.jsonValue
    }

    func decodeNil() -> Bool {
        switch jv {
        case is JsonNull:
            return true
        default:
            return false
        }
    }

    func decode(_ type: Bool.Type) throws -> Bool {
        switch jv {
        case let m as JsonBool:
            return m.value
        default:
            //TODO num
            return false
        }
    }

    func decode(_ type: String.Type) throws -> String {
        switch jv {
        case let m as JsonString:
            return m.value
        default:
            return jv.jsonText
        }
    }

    func decode(_ type: Double.Type) throws -> Double {
        switch jv {
        case let m as JsonNum:
            return Double(truncating: m.value)
        default:
            throw  typeMismatch(type: type, path: codingPath, desc: jv.jsonText)
        }
    }

    func decode(_ type: Float.Type) throws -> Float {
        switch jv {
        case let m as JsonNum:
            return Float(truncating: m.value)
        default:
            throw  typeMismatch(type: type, path: codingPath, desc: jv.jsonText)
        }
    }

    func decode(_ type: Int.Type) throws -> Int {
        switch jv {
        case let m as JsonNum:
            return Int(truncating: m.value)
        default:
            throw  typeMismatch(type: type, path: codingPath, desc: jv.jsonText)
        }
    }

    func decode(_ type: Int8.Type) throws -> Int8 {
        switch jv {
        case let m as JsonNum:
            return Int8(truncating: m.value)
        default:
            throw  typeMismatch(type: type, path: codingPath, desc: jv.jsonText)
        }
    }

    func decode(_ type: Int16.Type) throws -> Int16 {
        switch jv {
        case let m as JsonNum:
            return Int16(truncating: m.value)
        default:
            throw  typeMismatch(type: type, path: codingPath, desc: jv.jsonText)
        }
    }

    func decode(_ type: Int32.Type) throws -> Int32 {
        switch jv {
        case let m as JsonNum:
            return Int32(truncating: m.value)
        default:
            throw  typeMismatch(type: type, path: codingPath, desc: jv.jsonText)
        }
    }

    func decode(_ type: Int64.Type) throws -> Int64 {
        switch jv {
        case let m as JsonNum:
            return Int64(truncating: m.value)
        default:
            throw  typeMismatch(type: type, path: codingPath, desc: jv.jsonText)
        }
    }

    func decode(_ type: UInt.Type) throws -> UInt {
        switch jv {
        case let m as JsonNum:
            return UInt(truncating: m.value)
        default:
            throw  typeMismatch(type: type, path: codingPath, desc: jv.jsonText)
        }
    }

    func decode(_ type: UInt8.Type) throws -> UInt8 {
        switch jv {
        case let m as JsonNum:
            return UInt8(truncating: m.value)
        default:
            throw  typeMismatch(type: type, path: codingPath, desc: jv.jsonText)
        }
    }

    func decode(_ type: UInt16.Type) throws -> UInt16 {
        switch jv {
        case let m as JsonNum:
            return UInt16(truncating: m.value)
        default:
            throw  typeMismatch(type: type, path: codingPath, desc: jv.jsonText)
        }
    }

    func decode(_ type: UInt32.Type) throws -> UInt32 {
        switch jv {
        case let m as JsonNum:
            return UInt32(truncating: m.value)
        default:
            throw  typeMismatch(type: type, path: codingPath, desc: jv.jsonText)
        }
    }

    func decode(_ type: UInt64.Type) throws -> UInt64 {
        switch jv {
        case let m as JsonNum:
            return UInt64(truncating: m.value)
        default:
            throw  typeMismatch(type: type, path: codingPath, desc: jv.jsonText)
        }
    }

    func decode<T>(_ type: T.Type) throws -> T where T: Decodable {
        try type.init(from: self.decoder)
    }

}

fileprivate func typeMismatch(type: Any.Type, path: [CodingKey], desc: String) -> DecodingError {
    DecodingError.typeMismatch(type, DecodingError.Context(codingPath: path, debugDescription: desc))
}