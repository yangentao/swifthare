import Foundation

let fileManager = FileManager.default
extension URL {

    public func mkdirs() throws {
        if !self.existDirectory {
            try FileManager.default.createDirectory(at: self, withIntermediateDirectories: true)
        }
    }

    public func list(deep: Bool = false) throws -> [String] {
        if deep {
            return try FileManager.default.subpathsOfDirectory(atPath: self.pathX)
        } else {
            return try FileManager.default.contentsOfDirectory(atPath: self.pathX)
        }
    }
    public func children(_ options: FileManager.DirectoryEnumerationOptions = []) throws -> [URL] {
        try fileManager.contentsOfDirectory(at: self, includingPropertiesForKeys: nil, options: options)
    }

    public var pathX: String {
        self.path
    }
    public var parentURL: URL {
        self.deletingLastPathComponent()
    }

    public var fileSize: Int {
        if let map = try? fileAttrs(), let n: NSNumber = map[FileAttributeKey.size] as? NSNumber {
            return Int(truncating: n)
        }
        return 0
    }

    public func fileAttrs() throws -> [FileAttributeKey: Any] {
        return try fileManager.attributesOfItem(atPath: self.path)
    }

    @discardableResult
    public func fileCreate(data: Data? = nil) throws -> Bool {
        try self.parentURL.mkdirs()
        return fileManager.createFile(atPath: self.path, contents: data)
    }

    public func dirCreate() throws {
        try fileManager.createDirectory(at: self, withIntermediateDirectories: true)
    }
    public var existAny: Bool {
        self.exists.exists
    }
    public var existFile: Bool {
        self.exists.isFile
    }
    public var existDirectory: Bool {
        self.exists.isDir
    }

    @available(*, deprecated, renamed: "exists", message: "rename to exists")
    public var fileExists: ExistsResult {
        return existResult
    }
    @available(*, deprecated, renamed: "exists", message: "rename to exists")
    public var existResult: ExistsResult {
        return exists
    }
    public var exists: ExistsResult {
        var b: ObjCBool = false
        let e = fileManager.fileExists(atPath: self.path, isDirectory: &b)
        return ExistsResult(exists: e, isDirectory: b.boolValue)
    }

    public var fileReadable: Bool {
        fileManager.isReadableFile(atPath: self.path)
    }
    public var fileWritable: Bool {
        fileManager.isWritableFile(atPath: self.path)
    }

    public var fileContent: Data? {
        fileManager.contents(atPath: self.path)
    }
    public var fileContentText: String? {
        fileContent?.string()
    }
    public var fileData: Data? {
        try? Data(contentsOf: self)
    }
    public var fileText: String? {
        fileData?.string()
    }

    public func remove() throws {
        try fileManager.removeItem(at: self)
    }

    public func copyItem(to target: URL) throws {
        try fileManager.copyItem(at: self, to: target)
    }
}
