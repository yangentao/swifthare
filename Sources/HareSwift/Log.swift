// Created by entaoyang@163.com on 2019-07-28.
// Copyright (c) 2019 entao.dev. All rights reserved.
// ios

import Foundation

public let Log = LogImpl()

public struct LogLevel: Hashable, Equatable, RawRepresentable, Sendable {
    public let rawValue: String

    public init(rawValue: String) {
        self.rawValue = rawValue
    }

    public init(_ value: String) {
        self.rawValue = value
    }
}

extension LogLevel {
    public static let success = LogLevel("SUCCESS")
    public static let error = LogLevel("ERROR")
    public static let warning = LogLevel("WARNING")
    public static let info = LogLevel("INFO")
    public static let debug = LogLevel("DEBUG")
    public static let verbose = LogLevel("VERBOSE")
}

public final class LogItem: ToString, Sendable {
    public let level: LogLevel
    public let tag: String?
    public let message: String
    public let date: Date
    private let line: String

    public init(level: LogLevel, tag: String?, message: String) {
        self.level = level
        self.tag = tag
        self.message = message
        self.date = .now
        self.line = date.formatedDateTimeMill + " " + level.rawValue + ": " + (tag ?? "") + " " + message + "\n"
    }

    public func toString() -> String {
        return line
    }
}

public typealias LogAcceptor = (LogItem) -> Bool

//强引用

public struct LogPrinterItem: Equatable {
    public var printer: LogPrinter
    public var acceptor: LogAcceptor = { _ in
        true
    }

    public static func == (lhs: LogPrinterItem, rhs: LogPrinterItem) -> Bool {
        lhs.printer === rhs.printer
    }
}

public struct LogListenerItem: Equatable {
    public weak var listener: LogListener? = nil
    public var acceptor: LogAcceptor = { _ in
        true
    }

    public static func == (lhs: LogListenerItem, rhs: LogListenerItem) -> Bool {
        lhs.listener === rhs.listener
    }
}

public class LogImpl {
    private let lock: NSRecursiveLock = NSRecursiveLock()
    private var printerItems: [LogPrinterItem] = []
    private var listenerItems: [LogListenerItem] = []

    public private(set) var historyItems: [LogItem] = []

    public var keepSize: Int = 1000 {
        didSet {
            if self.keepSize < 0 {
                self.keepSize = 0
            }
            if self.historyItems.count > keepSize {
                self.historyItems.removeFirst(self.historyItems.count - keepSize)
            }
        }
    }

    fileprivate init() {
        set(printer: ConsoleLogPrinter())
    }

    deinit {
        let ls = self.printerItems
        ls.forEach { p in
            p.printer.logFlush()
            p.printer.logUninstall()
        }
    }

}

extension LogImpl {
    public func filterItems(_ block: (LogItem) -> Bool) -> [LogItem] {
        self.lock.lock()
        defer {
            self.lock.unlock()
        }
        return self.historyItems.filter(block)
    }

}

extension LogImpl {
    public func add(listener: LogListener) {
        self.listenerItems.addOnAbsence(LogListenerItem(listener: listener))
    }

    public func add(listener: LogListener, acceptor: @escaping LogAcceptor) {
        self.listenerItems.addOnAbsence(LogListenerItem(listener: listener, acceptor: acceptor))
    }

    public func remove(listener: LogListener) {
        self.listenerItems.removeAll { item in
            item.listener === listener
        }
    }
}

extension LogImpl {

    public func add(printer: LogPrinter) {
        self.lock.lock()
        defer {
            self.lock.unlock()
        }
        self.printerItems.addOnAbsence(LogPrinterItem(printer: printer))
    }

    public func add(printer: LogPrinter, acceptor: @escaping LogAcceptor) {
        self.lock.lock()
        defer {
            self.lock.unlock()
        }
        self.printerItems.addOnAbsence(LogPrinterItem(printer: printer, acceptor: acceptor))
    }

    public func set(printer: LogPrinter) {
        self.clearPrinters()
        self.add(printer: printer)
    }

    public func set(printer: LogPrinter, acceptor: @escaping LogAcceptor) {
        self.clearPrinters()
        self.add(printer: printer, acceptor: acceptor)
    }

    public func clearPrinters() {
        self.lock.lock()
        defer {
            self.lock.unlock()
        }
        let ls = self.printerItems
        self.printerItems = []
        ls.forEach {
            $0.printer.logUninstall()
        }
    }

    public func remove(printer: LogPrinter) {
        self.lock.lock()
        defer {
            self.lock.unlock()
        }
        self.printerItems.removeAll { item in
            item.printer === printer
        }
        printer.logUninstall()
    }

    public func flush() {
        self.lock.lock()
        defer {
            self.lock.unlock()
        }
        let ls = self.printerItems
        ls.forEach {
            $0.printer.logFlush()
        }
    }

    public func log(item: LogItem) {
        self.lock.lock()
        defer {
            self.lock.unlock()
        }
        if self.keepSize > 0 {
            if self.historyItems.count > self.keepSize + 20 {
                let n = self.historyItems.count - self.keepSize
                self.historyItems.removeFirst(n)
            }
            self.historyItems.append(item)
        }
        let ls = self.printerItems
        for pi in ls {
            if pi.acceptor(item) {
                pi.printer.logWrite(item: item)
            }
        }
        Task { @MainActor in
            var needClean = false
            let ls = self.listenerItems
            for l in ls {
                if let L = l.listener {
                    if l.acceptor(item) {
                        L.onLog(item: item)
                    }
                } else {
                    needClean = true
                }
            }
            if needClean {
                self.listenerItems.removeAll {
                    $0.listener == nil
                }
            }
        }

    }

}

extension LogImpl {

    public func logItems(level: LogLevel, tag: String = "", items: [Any?]) {
        self.log(item: LogItem(level: level, tag: tag, message: itemsToString(items)))
    }

    public func info(_ items: Any?...) {
        self.logItems(level: .info, items: items)
    }

    public func debug(_ items: Any?...) {
        self.logItems(level: .debug, items: items)
    }

    public func warn(_ items: Any?...) {
        self.logItems(level: .warning, items: items)
    }

    public func error(_ items: Any?...) {
        self.logItems(level: .error, items: items)
    }

    public func success(_ items: Any?...) {
        self.logItems(level: .success, items: items)
    }

    public func verbose(_ items: Any?...) {
        self.logItems(level: .verbose, items: items)
    }
}

extension LogImpl {

    public func info(tag: String, _ items: Any?...) {
        self.logItems(level: .info, tag: tag, items: items)
    }

    public func debug(tag: String, _ items: Any?...) {
        self.logItems(level: .debug, tag: tag, items: items)
    }

    public func warn(tag: String, _ items: Any?...) {
        self.logItems(level: .warning, tag: tag, items: items)
    }

    public func error(tag: String, _ items: Any?...) {
        self.logItems(level: .error, tag: tag, items: items)
    }

    public func success(tag: String, _ items: Any?...) {
        self.logItems(level: .success, tag: tag, items: items)
    }

    public func verbose(tag: String, _ items: Any?...) {
        self.logItems(level: .verbose, tag: tag, items: items)
    }
}

public func logd(_ ss: Any?...) {
    Log.logItems(level: .debug, items: ss)
}

public func loge(_ ss: Any?...) {
    Log.logItems(level: .error, items: ss)
}

public func log(tag: String, _ ss: Any?...) {
    Log.logItems(level: .debug, tag: tag, items: ss)
}

public func logd(tag: String, _ ss: Any?...) {
    Log.logItems(level: .debug, tag: tag, items: ss)
}

public func loge(tag: String, _ ss: Any?...) {
    Log.logItems(level: .error, tag: tag, items: ss)
}

public protocol LogListener: AnyObject {
    func onLog(item: LogItem)
}

public protocol LogPrinter: AnyObject {
    func logWrite(item: LogItem)
    func logFlush()
    func logUninstall()
}

public class ConsoleLogPrinter: LogPrinter {
    public func logWrite(item: LogItem) {
        if item.level == .error {
            print(item.toString(), terminator: "", to: &StdError)
        } else {
            print(item.toString(), terminator: "")
        }
    }

    public func logFlush() {

    }

    public func logUninstall() {

    }
}

public class FileLogPrinter: LogPrinter, TextOutputStream {
    private var handle: FileHandlePrinter?
    private let keyClean: String = Date().timeIntervalSince1970.toString()

    public init(path: String) {
        if let h = try? FileHandle.openForWrite(path: path, append: true) {
            handle = FileHandlePrinter(handle: h)
        }
    }

    public func write(_ string: String) {
        handle?.write(string)
    }

    public func logWrite(item: LogItem) {
        handle?.logWrite(item: item)
    }

    public func logFlush() {
        handle?.logFlush()
    }

    public func logUninstall() {
        handle?.logUninstall()
    }

    deinit {
        handle = nil
    }

}

public var CacheLogPrinter = DirLogPrinter(dir: Dirs.cacheDir.appendPath("log"))

public class DirLogPrinter: LogPrinter, TextOutputStream {
    private let dir: String
    private var preDate: Date = Date.now
    private var handle: FileHandlePrinter? = nil
    private let fileCount: Int

    public init(dir: String, fileCount: Int = 100) {
        self.dir = dir
        self.fileCount = fileCount
        if !dir.existDirectory {
            try? FileManager.default.createDirectory(atPath: dir, withIntermediateDirectories: true)
        }
        println("DirLogPrinter: dir = \(dir)")
        checkFile(date: Date.now)
    }
    private func checkOlds() throws {
        // TODO delete old files
        let ls = try FileManager.default.contentsOfDirectory(atPath: dir)
        if ls.count < fileCount {
            return
        }

    }
    private func checkFile(date: Date) {
        if handle == nil || !preDate.sameDay(date) {
            preDate = date
            let filepath = joinPath(dir, date.formated)
            handle?.close()
            if let h = try? FileHandle.openForWrite(path: filepath, append: true) {
                handle = FileHandlePrinter(handle: h)
            }
            try? checkOlds()
        }
    }

    public func write(_ string: String) {
        handle?.write(string)
    }

    public func logWrite(item: LogItem) {
        checkFile(date: item.date)
        handle?.logWrite(item: item)
    }

    public func logFlush() {
        handle?.logFlush()
    }

    public func logUninstall() {
        handle?.logUninstall()
    }

    deinit {
        handle = nil
    }

}

public class FileHandlePrinter: LogPrinter, TextOutputStream {
    private var fh: FileHandle?
    private let keyClean: String = Date().timeIntervalSince1970.toString()

    public init(handle: FileHandle) {
        self.fh = handle

        AppExitClean.shared.add(keyClean) { [weak self] in
            self?.close()
        }
    }
    public func close() {
        try? fh?.close()
        fh = nil
    }

    public func write(_ string: String) {
        let line = string + "\n"
        fh?.write(line.data(using: .utf8)!)
    }

    public func logWrite(item: LogItem) {
        let text = item.toString()
        fh?.write(text.data(using: .utf8)!)
    }

    public func logFlush() {
        try? fh?.synchronize()
    }

    public func logUninstall() {
        close()
        AppExitClean.shared.remove(keyClean)
    }

    deinit {
        self.close()
    }

}
