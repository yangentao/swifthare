//
// Created by entaoyang@163.com on 2022/9/18.
//

import Foundation

extension Array {
    public init(capacity: Int) {
        self.init()
        self.reserveCapacity(capacity)
    }

    public func get(_ index: Int) -> Element? {
        if self.indices.contains(index) {
            return self[index]
        }
        return nil
    }

    public mutating func set(_ index: Int, value: Element) {
        if self.indices.contains(index) {
            self[index] = value
        } else if self.count == index {
            self.append(value)
        } else {
            fatalError("array.set(index,value), out of bound")
        }
    }
    public mutating func addAll<S>(_ es: S) where Element == S.Element, S: Sequence {
        self.append(contentsOf: es)
    }

    public mutating func add(item: Element, index: Int) {
        if self.indices.contains(index) {
            self.insert(item, at: index)
        } else {
            self.append(item)
        }
    }

    public mutating func add(_ e: Element) {
        self.append(e)
    }

    public static func += (lhs: inout Array, rhs: Self.Element) {
        lhs.append(rhs)
    }

    public func toMap<K: Hashable, V: Any>(_ block: (Element) -> (K, V)?) -> [K: V] {
        var map: [K: V] = [:]
        for item in self {
            if let p = block(item) {
                map[p.0] = p.1
            }
        }
        return map
    }

    public func head(size: Int) -> [Element] {
        if size <= 0 {
            return []
        }
        if self.count < size {
            return self
        }
        return self[0..<size].toArray
    }

    public func tail(size: Int) -> [Element] {
        if size <= 0 {
            return []
        }
        if self.count < size {
            return self
        }
        let from: Int = self.count - size
        return self[from..<self.count].toArray
    }

    public func limit(size: Int) -> [Element] {
        if self.count <= size {
            return self
        }
        return self[..<size].toArray
    }

    @inlinable
    public var lastItem: Self.Element {
        self[self.count - 1]
    }

    public func distinct() -> [Self.Element] where Self.Element: Comparable {
        var ls: [Element] = []
        ls.reserveCapacity(self.count)
        for e in self {
            if !ls.contains(e) {
                ls.append(e)
            }
        }
        return ls
    }

    public func distincted<T: Hashable>(by keyBlock: (Self.Element) -> T) -> [Self.Element] {
        var set: Set<T> = Set<T>(minimumCapacity: self.count)
        var ls: [Self.Element] = [Self.Element](capacity: self.count)
        for item in self {
            let key = keyBlock(item)
            if !set.contains(key) {
                set.add(key)
                ls += item
            }
        }
        return ls

    }

    public mutating func popFirst() -> Element? {
        if self.isEmpty {
            return nil
        }
        return self.remove(at: 0)
    }

    public func first(_ block: (Element) -> Bool) -> Element? {
        for e in self {
            if block(e) {
                return e
            }
        }
        return nil
    }

    public func first(size: Int) -> [Element] {
        var ls = [Element]()

        var n = size
        if n > self.count {
            n = self.count
        }

        for i in 0..<n {
            ls.append(self[i])
        }
        return ls
    }

    //只删除第一个符合条件的, 返回被删除的元素

    @discardableResult
    public mutating func removeFirst(_ block: (Element) -> Bool) -> Element? {
        for i in self.indices {
            let item = self[i]
            if block(item) {
                self.remove(at: i)
                return item
            }
        }
        return nil
    }

    public mutating func removeElement(_ e: Element) where Element: Equatable {
        for i in 0..<self.count {
            if self[i] == e {
                self.remove(at: i)
                return
            }
        }
    }

    public mutating func removeAll(_ other: [Element]) where Element: Equatable {
        for e in other {
            self.removeElement(e)
        }
    }

    //返回被删除的元素

    @discardableResult
    public mutating func removeAllIf(block: (Element) -> Bool) -> [Element] {
        var arr = [Element]()
        var i = 0
        while i < self.count {
            let item = self[i]
            if block(item) {
                self.remove(at: i)
                arr.append(item)
            } else {
                i += 1
            }
        }
        return arr
    }

    //IndexSet

    public mutating func removeIndexs(indexSet: IndexSet) {
        let ls = indexSet.sortedDesc({ $0 })
        for n in ls {
            self.remove(at: n)
        }
    }

    public mutating func removeIndexs(indexSet: Set<Int>) {
        let ls = indexSet.sortedDesc({ $0 })
        for n in ls {
            self.remove(at: n)
        }
    }
    public mutating func sort(asc: Bool) where Element: Comparable {
        if asc {
            self.sort(by: { $0 < $1 })
        } else {
            self.sort(by: { $1 < $0 })
        }
    }
    public mutating func sort<T>(asc: Bool, _ block: (Element) -> T) where T: Comparable {
        if asc {
            self.sort(by: { block($0) < block($1) })
        } else {
            self.sort(by: { block($1) < block($0) })
        }
    }

    public mutating func sortAsc<T>(_ block: (Element) -> T) where T: Comparable {
        self.sort(by: { block($0) < block($1) })
    }

    public mutating func sortDesc<T>(_ block: (Element) -> T) where T: Comparable {
        self.sort(by: { block($1) < block($0) })
    }

}

extension Array where Element: Equatable {
    public static func -= (lhs: inout Array, rhs: Element?) {
        if let r = rhs {
            lhs.removeAll { e in
                e == r
            }
        }
    }

    @discardableResult
    public mutating func addOnAbsence(_ ele: Element) -> Bool {
        if self.contains(ele) {
            return false
        }
        self.append(ele)
        return true
    }
}

extension ArraySlice {

    public var toArray: [Element] {
        return [Element](self)
    }
}

extension Collection {
    public var notEmpty: Bool {
        !self.isEmpty
    }

    public func joinToString(_ sep: String, _ block: (Element) -> String) -> String {
        return self.map(block).joined(separator: sep)
    }
}

extension Sequence {
    public var toArray: [Element] {
        [Element](self)
    }
    /// if empty, return false
    public func all(_ block: (Element) -> Bool) -> Bool {
        var n: Int = 0
        for e in self {
            n += 1
            if !block(e) {
                return false
            }
        }
        return n == 0 ? false : true
    }
    /// if empty, return true
    public func every(_ block: (Element) -> Bool) -> Bool {
        for e in self {
            if !block(e) {
                return false
            }
        }
        return true
    }
    /// if empty, return false
    @available(*, deprecated, renamed: "existOne", message: "use existOne instead")
    public func any(_ block: (Element) -> Bool) -> Bool {
        for e in self {
            if block(e) {
                return true
            }
        }
        return false
    }

    public func existOne(_ block: (Element) -> Bool) -> Bool {
        for e in self {
            if block(e) {
                return true
            }
        }
        return false
    }
    public func emptyOrExist(_ block: (Element) -> Bool) -> Bool {
        var emp = true
        for e in self {
            if block(e) {
                return true
            } else {
                emp = false
            }
        }
        return emp
    }
    public func map<T: Any>(_ keyPath: KeyPath<Element, T>) -> [T] {
        map {
            $0[keyPath: keyPath]
        }
    }

    public func filter<T: Any>(type: T.Type) -> [T] {
        self.compactMap {
            $0 as? T
        }
    }

    public func first<T: Any>(type: T.Type) -> T? {
        for e in self {
            if e is T {
                return e as? T
            }
        }
        return nil
    }
    public func sorted(asc: Bool) -> [Element] where Element: Comparable {
        if asc {
            return self.sorted(by: { $0 < $1 })
        } else {
            return self.sorted(by: { $1 < $0 })
        }
    }

    public func sorted<T>(asc: Bool, _ block: (Element) -> T) -> [Element] where T: Comparable {
        if asc {
            return self.sorted(by: { block($0) < block($1) })
        } else {
            return self.sorted(by: { block($1) < block($0) })
        }
    }

    public func sortedAsc<T>(_ block: (Element) -> T) -> [Element] where T: Comparable {
        self.sorted(by: { block($0) < block($1) })
    }

    public func sortedDesc<T>(_ block: (Element) -> T) -> [Element] where T: Comparable {
        self.sorted(by: { block($1) < block($0) })
    }

    public func sumBy<T>(_ block: (Element) -> T) -> T where T: Numeric {
        var m: T = 0
        for e in self {
            m += block(e)
        }
        return m
    }

    @discardableResult
    public func each(_ block: (Element) -> Void) -> Self {
        for item in self {
            block(item)
        }
        return self
    }
}

extension Sequence where Element: Hashable {
    public var toSet: Set<Element> {
        Set<Element>(self)
    }
}

extension Sequence where Element: Numeric {
    public func sum() -> Element {
        var m: Element = 0
        for a in self {
            m += a
        }
        return m
    }

}

extension Set {
    public mutating func clear() {
        self.removeAll(keepingCapacity: true)
    }

}

extension Set where Element: Hashable {

    public mutating func add(_ e: Element) {
        self.insert(e)
    }
}

extension Dictionary {

    public func has(key: Key) -> Bool {
        self.keys.contains(key)

    }

    public var keySet: Set<Key> {
        return Set<Key>(self.keys)
    }
    public var valueArray: [Value] {
        return [Value](self.values)
    }

    public mutating func clear() {
        self.removeAll(keepingCapacity: true)
    }

    public mutating func getOrPut(_ key: Self.Key, _ block: (Self.Key) -> Self.Value) -> Self.Value {
        if let v = self[key] {
            return v
        }
        let v = block(key)
        self[key] = v
        return v
    }

    //保留set中的元素

    public mutating func retain(_ keySet: Set<Key>) {
        var allKeys = Set<Key>(self.keys)
        allKeys.subtract(keySet)
        for k in allKeys {
            self.removeValue(forKey: k)
        }

    }

    //保留set中的元素

    public mutating func retain(_ keySet: [Key]) {
        var allKeys = Set<Key>(self.keys)
        allKeys.subtract(keySet)
        for k in allKeys {
            self.removeValue(forKey: k)
        }

    }

    public mutating func putAll(dic: [Key: Value]) {
        for e in dic {
            self[e.key] = e.value
        }
    }

    public func transformKey<K>(_ block: (Key) -> K) -> [K: Value] where K: Hashable {
        var m: [K: Value] = [:]
        m.reserveCapacity(self.capacity)
        for (k, v) in self {
            m[block(k)] = v
        }
        return m
    }

    public func transformValue<V>(_ block: (Value) -> V) -> [Key: V] {
        var m: [Key: V] = [:]
        m.reserveCapacity(self.capacity)
        for (k, v) in self {
            m[k] = block(v)
        }
        return m
    }

    public func transform<K, V>(_ block: (Key, Value) -> (K, V)) -> [K: V] where K: Hashable {
        var m: [K: V] = [:]
        m.reserveCapacity(self.capacity)
        for (k, v) in self {
            let (a, b) = block(k, v)
            m[a] = b
        }
        return m
    }
}

public class MySet<Element: Hashable> {
    public var set: Set<Element>

    public init(_ capcity: Int = 16) {
        set = Set<Element>(minimumCapacity: capcity)
    }

    public init<Source>(_ sequence: Source) where Element == Source.Element, Source: Sequence {
        set = Set<Element>(sequence)
    }

    public init(arrayLiteral elements: Element...) {
        set = Set<Element>(minimumCapacity: elements.count + 8)
        for e in elements {
            set.insert(e)
        }
    }
}

public class MyMap<Key: Hashable, Value> {
    public var map: [Key: Value]

    public init(_ capcity: Int = 16) {
        map = [Key: Value](minimumCapacity: capcity)
    }

    public init(dictionaryLiteral elements: (Key, Value)...) {
        map = [Key: Value](minimumCapacity: elements.count + 8)
        for (k, v) in elements {
            map[k] = v
        }
    }

    public init(_ map: [Key: Value]) {
        self.map = map
    }
}

public class MyArray<Element> {
    public var array: [Element]

    public init() {
        array = [Element]()
    }

    public init<S>(_ s: S) where Element == S.Element, S: Sequence {
        array = [Element](s)
    }

    public init(repeating repeatedValue: Element, count: Int) {
        array = [Element](repeating: repeatedValue, count: count)
    }
}
