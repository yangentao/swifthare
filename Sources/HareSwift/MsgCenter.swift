//
// Created by entaoyang@163.com on 2017/10/11.
// Copyright (c) 2017 yet.net. All rights reserved.
//

import Foundation

//example

extension MsgID {
    static let userChanged = MsgID("msg.userChanged")
}

public struct MsgID: Hashable, Equatable, RawRepresentable {
    public typealias RawValue = String
    public var rawValue: String

    public init(_ rawValue: String) {
        self.rawValue = rawValue
    }

    public init(rawValue: String) {
        self.rawValue = rawValue
    }

    public var hashValue: Int {
        rawValue.hashValue
    }

    public func hash(into hasher: inout Hasher) {
        rawValue.hash(into: &hasher)
    }

    public static func ==(lhs: MsgID, rhs: MsgID) -> Bool {
        lhs.rawValue == rhs.rawValue
    }

    public static func ==(lhs: MsgID, rhs: String) -> Bool {
        lhs.rawValue == rhs
    }
}

public class Msg: Equatable {
    public var msg: MsgID
    public var sender: Any? = nil
    public var argMap: [String: Any] = [:]
    public var result = Array<Any>()
    public var any: Any? = nil
    public var anyObject: AnyObject? = nil
    public var n1 = 0
    public var n2 = 0
    public var s1 = ""
    public var s2 = ""
    public var b1 = false
    public var b2 = false

    public init(_ msg: MsgID) {
        self.msg = msg
    }

    public subscript(_ key: String) -> Any? {
        get {
            argMap[key]
        }
        set {
            argMap[key] = newValue
        }
    }
}

public extension Msg {
    func fire() {
        Tasks.fore {
            MsgCenter.fireCurrent(self)
        }
    }

    func match(_ ids: MsgID...) -> Bool {
        for id in ids {
            if self.msg == id {
                return true
            }
        }
        return false
    }

    static func ==(lhs: Msg, rhs: String) -> Bool {
        lhs.msg == rhs
    }

    static func ==(lhs: Msg, rhs: Msg) -> Bool {
        lhs.msg == rhs.msg
    }
}

public protocol MsgListener: AnyObject {
    func onMsg(msg: Msg)
}

fileprivate class ListenerItem: Equatable {
    weak var listener: MsgListener?

    init(_ l: MsgListener) {
        self.listener = l
    }

    static func ==(lhs: ListenerItem, rhs: ListenerItem) -> Bool {
        if lhs === rhs {
            return true
        }
        if let a = lhs.listener, let b = rhs.listener {
            return a === b
        }
        return false
    }
}

public class MsgCenterObject {
    private var allItems = [ListenerItem]()

    public func remove(listener: MsgListener) {
        sync(self) {
            allItems.removeAll(where: { $0.listener === listener })
        }
    }

    public func add(listener: MsgListener) {
        sync(self) {
            let item = ListenerItem(listener)
            if !allItems.contains(item) {
                allItems.append(item)
            }
        }
    }

    public func fireCurrent(_ msg: Msg) {
        allItems.removeAll {
            $0.listener == nil
        }
        let aItems = allItems
        aItems.forEach { item in
            item.listener?.onMsg(msg: msg)
        }
    }

}

public let MsgCenter = MsgCenterObject()

public extension MsgID {
    func fire() {
        let m = Msg(self)
        m.fire()
    }

    func fire(_ block: (Msg) -> Void) {
        let m = Msg(self)
        block(m)
        m.fire()
    }
}

public extension NSObject {
    func fire(_ msg: MsgID) {
        let m = Msg(msg)
        m.sender = self
        m.fire()
    }

    func fire(_ msg: MsgID, _ block: (Msg) -> Void) {
        let m = Msg(msg)
        m.sender = self
        block(m)
        m.fire()
    }
}
