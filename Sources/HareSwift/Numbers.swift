//
// Created by entaoyang@163.com on 2022/9/13.
//

import Foundation

public let KB = 1024
public let MB = 1024 * 1024
public let GB = 1024 * 1024 * 1024

public typealias Byte = UInt8
public typealias Long = Int64
public typealias ULong = UInt64

extension Double {
    public func numberFormat(_ block: (NumberFormatter) -> Void) -> String {
        let f = NumberFormatter()
        f.numberStyle = .none
        block(f)
        let num = NSNumber(value: self)
        return f.string(from: num)!

    }
}
extension Int {
    public func numberFormat(_ block: (NumberFormatter) -> Void) -> String {
        let f = NumberFormatter()
        f.numberStyle = .none
        block(f)
        let num = NSNumber(value: self)
        return f.string(from: num)!

    }
    // 1.format("%02X")
    public func format(_ s: String) -> String {
        String(format: s, self)
    }
}

extension Int {
    public func ge(_ v: Int) -> Int {
        if self < v {
            return v
        }
        return self
    }
}

@propertyWrapper
public struct GreatEQ<T: SignedNumeric & Comparable> {
    public let minValue: T
    public var value: T

    public init(wrappedValue: T, minValue: T) {
        self.minValue = minValue
        self.value = wrappedValue
    }

    public var wrappedValue: T {
        get {
            if value < minValue {
                return minValue
            }
            return value
        }
        set {
            value = newValue
        }
    }
}

public func / (lhs: CGFloat, rhs: Int) -> CGFloat {
    lhs / CGFloat(rhs)
}

public func * (lhs: CGFloat, rhs: Int) -> CGFloat {
    lhs * CGFloat(rhs)
}

extension Numeric {
    public func onZero(_ value: Self) -> Self {
        if self == 0 {
            return value
        }
        return self
    }

    public func ge(_ v: Self) -> Self where Self: Comparable {
        if self < v {
            return v
        }
        return self
    }

    public func le(_ v: Self) -> Self where Self: Comparable {
        if self > v {
            return v
        }
        return self
    }

    public func limit(from: Self, to: Self) -> Self where Self: Comparable {
        if self > to {
            return to
        }
        if self < from {
            return from
        }
        return self
    }

    public var isIntegers: Bool {
        switch self {
        case is Int, is Int8, is Int16, is Int32, is Int64, is UInt, is UInt8, is UInt16, is UInt32, is UInt64:
            return true
        default:
            return false
        }
    }
    public var isReals: Bool {
        switch self {
        case is Float, is Double, is Float32, is Float64, is CGFloat, is Decimal:
            return true
        default:
            return false
        }
    }

    public var number: NSNumber {
        self.toNSNumber
    }

    public var toNSNumber: NSNumber {
        switch self {
        case let a as Int:
            return NSNumber(value: a)
        case let a as Int8:
            return NSNumber(value: a)
        case let a as Int16:
            return NSNumber(value: a)
        case let a as Int32:
            return NSNumber(value: a)
        case let a as Int64:
            return NSNumber(value: a)

        case let a as UInt:
            return NSNumber(value: a)
        case let a as UInt8:
            return NSNumber(value: a)
        case let a as UInt16:
            return NSNumber(value: a)
        case let a as UInt32:
            return NSNumber(value: a)
        case let a as UInt64:
            return NSNumber(value: a)

        case let a as Float:
            return NSNumber(value: a)
        case let a as Double:
            return NSNumber(value: a)
        case let a as Float32:
            return NSNumber(value: a)
        case let a as Float64:
            return NSNumber(value: a)
        case let a as CGFloat:
            return NSNumber(value: Double(a))
        case let a as Decimal:
            return NSDecimalNumber(decimal: a)
        default:
            fatalError("unknown number : \(self)")
        }
    }
}

extension BinaryInteger {
    public func toString() -> String {
        "\(self)"
    }

    public var cint: Int32 {
        Int32(self)
    }
    public var int32Value: Int32 {
        Int32(self)
    }
    public var uint32Value: UInt32 {
        UInt32(self)
    }
    public var intValue: Int {
        Int(self)
    }
    public var uintValue: UInt {
        UInt(self)
    }
    public var longValue: Long {
        Long(self)
    }
    public var ulongValue: ULong {
        ULong(self)
    }
    public var floatValue: Float {
        Float(self)
    }
    public var doubleValue: Double {
        Double(self)
    }
    public var cgfloatValue: CGFloat {
        CGFloat(self)
    }
}

extension BinaryFloatingPoint {
    public func toString() -> String {
        "\(self)"
    }

    public var cint: Int32 {
        Int32(self)
    }
    public var int32Value: Int32 {
        Int32(self)
    }
    public var uint32Value: UInt32 {
        UInt32(self)
    }
    public var intValue: Int {
        Int(self)
    }
    public var uintValue: UInt {
        UInt(self)
    }
    public var longValue: Long {
        Long(self)
    }
    public var ulongValue: ULong {
        ULong(self)
    }
    public var floatValue: Float {
        Float(self)
    }
    public var doubleValue: Double {
        Double(self)
    }
    public var cgfloatValue: CGFloat {
        CGFloat(self)
    }
}

extension FixedWidthInteger {
    public var f: CGFloat {
        CGFloat(self)
    }
    public var s: String {
        "\(self)"
    }
}

extension NSNumber {
    public var isInteger: Bool {
        !stringValue.contains(".")
    }
}

extension Int64 {

    public var date: Date {
        Date(timeIntervalSince1970: Double(self / 1000))
    }
}

extension Double {
    public func keepDot(_ n: Int) -> String {
        String(format: "%.\(n)f", arguments: [self])
    }

    public var afterSeconds: DispatchTime {
        DispatchTime.now() + self
    }

    public func format(_ block: (NumberFormatter) -> Void) -> String {
        let f = NumberFormatter()
        f.numberStyle = .decimal
        block(f)
        return f.string(from: NSNumber(value: self)) ?? ""
    }

}
