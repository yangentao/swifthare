//
//  results.swift
//  HareSwift
//
//  Created by Entao on 2024/12/30.
//
import Foundation

public class BoolMessage: CustomStringConvertible {
    public let success: Bool
    public let message: String?
    public let error: Any?

    public init(value: Bool, message: String? = nil, error: Any? = nil) {
        self.success = value
        self.message = message
        self.error = error
    }
    public init(success message: String?, error: Any? = nil) {
        self.success = true
        self.message = message
        self.error = error
    }
    public init(failed message: String? = nil, error: Any? = nil) {
        self.success = false
        self.message = message
        self.error = error
    }
    public static let OK = BoolMessage(success: "OK")

    public var description: String {
        return "BoolResult(success:\(success), message:\(message ?? "nil"), error:\(error ?? "nil") )"
    }
}
public class BoolData<V>: CustomStringConvertible {
    public let success: Bool
    public let message: String?
    public let error: Any?
    public let data: V?

    public init(value: Bool, data: V?, message: String? = nil, error: Any? = nil) {
        self.success = value
        self.message = message
        self.error = error
        self.data = data
    }
    public init(success data: V, message: String? = nil, error: Any? = nil) {
        self.success = true
        self.message = message
        self.error = error
        self.data = data
    }
    public init(failed message: String?, error: Any? = nil) {
        self.success = false
        self.message = message
        self.error = error
        self.data = nil
    }
    public var description: String {
        return "BoolDataResult(success:\(success), message:\(message ?? "nil"), error:\(error ?? "nil"), data: \(String(describing: data))"
    }
}

public class CodeMessage: CustomStringConvertible {
    public let code: Int
    public let message: String?
    public let error: Any?
    public let SUCCESS_CODE: Int

    public var success: Bool {
        return code == SUCCESS_CODE
    }

    public init(code: Int, message: String? = nil, error: Any? = nil, SUCCESS_CODE: Int = 0) {
        self.code = code
        self.message = message
        self.error = error
        self.SUCCESS_CODE = SUCCESS_CODE
    }
    public init(success message: String?, error: Any? = nil, SUCCESS_CODE: Int = 0) {
        self.code = SUCCESS_CODE
        self.message = message
        self.error = error
        self.SUCCESS_CODE = SUCCESS_CODE
    }
    public init(failed code: Int, message: String? = nil, error: Any? = nil, SUCCESS_CODE: Int = 0) {
        self.code = code
        self.message = message
        self.error = error
        self.SUCCESS_CODE = SUCCESS_CODE
    }
    public static let OK = CodeMessage(success: "OK")
    public var description: String {
        return "CodeResult(code:\(code), message:\(message ?? "nil"), error:\(error ?? "nil") )"
    }
}
public class CodeData<V>: CustomStringConvertible {
    public let code: Int
    public let message: String?
    public let error: Any?
    public let data: V?
    public let SUCCESS_CODE: Int

    public init(code: Int, data: V?, message: String? = nil, error: Any? = nil, SUCCESS_CODE: Int = 0) {
        self.code = code
        self.data = data
        self.message = message
        self.error = error
        self.SUCCESS_CODE = SUCCESS_CODE
    }
    public init(success data: V?, message: String? = nil, error: Any? = nil, SUCCESS_CODE: Int = 0) {
        self.code = SUCCESS_CODE
        self.data = data
        self.message = message
        self.error = error
        self.SUCCESS_CODE = SUCCESS_CODE
    }
    public init(failed code: Int, message: String? = nil, error: Any? = nil, SUCCESS_CODE: Int = 0) {
        self.code = code
        self.data = nil
        self.message = message
        self.error = error
        self.SUCCESS_CODE = SUCCESS_CODE
    }
    public var success: Bool {
        code == SUCCESS_CODE
    }

    public var description: String {
        return "CodeDataResult(code:\(code), message:\(message ?? "nil"), error:\(error ?? "nil"), data: \(String(describing: data) )"
    }

    public static func fromErrno() -> CodeData<Int> {
        CodeData<Int>(failed: errno.intValue, message: String(cString: strerror(errno)))
    }
}
public class DataResult<V>: CustomStringConvertible {
    public let data: V?
    public let message: String?
    public let error: Any?

    public var success: Bool {
        data != nil
    }

    public init(data: V?, message: String? = nil, error: Any? = nil) {
        self.data = data
        self.message = message
        self.error = error
    }
    public init(failed message: String? = nil, error: Any? = nil) {
        self.data = nil
        self.message = message
        self.error = error
    }
    public var description: String {
        return "DataResult(data: \(String(describing: data)), message:\(message ?? "nil"), error:\(error ?? "nil")"
    }
}
public class ListResult<V>: CustomStringConvertible {
    public let items: [V]?
    public let message: String?
    public let error: Any?

    public var success: Bool {
        items != nil
    }

    public init(items: [V]?, message: String? = nil, error: Any? = nil) {
        self.items = items
        self.message = message
        self.error = error
    }
    public var description: String {
        return "ListResult(items: \(String(describing: items)), message:\(message ?? "nil"), error:\(error ?? "nil")"
    }
}
