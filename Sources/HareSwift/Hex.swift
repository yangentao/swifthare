//
// Created by entaoyang@163.com on 2022-11-18.
//

import Foundation

public class Hex {
    private static var AR = "0123456789ABCDEF"

    public static func encode(byte b: UInt8) -> String {
        var ret = ""
        ret.append(AR.at(Int(0x0f & (b >> 4))))
        ret.append(AR.at(Int(0x0f & b)))
        return ret
    }

    public static func encode(chars bytes: [Int8]) -> String {
        var ret = ""
        ret.reserveCapacity(bytes.count * 2)
        for b in bytes {
            ret.append(AR.at(Int(0x0f & (b >> 4))))
            ret.append(AR.at(Int(0x0f & b)))
        }
        return ret
    }

    public static func encode(bytes: [UInt8]) -> String {
        var ret = ""
        ret.reserveCapacity(bytes.count * 2)
        for b in bytes {
            ret.append(AR.at(Int(0x0f & (b >> 4))))
            ret.append(AR.at(Int(0x0f & b)))
        }
        return ret
    }

    public static func encode(text s: String) -> String {
        return encode(bytes: s.dataUtf8.bytes)
    }

    public static func decodeTo(text s: String) throws -> String {
        let ar = try decodeTo(bytes: s)
        return String(bytes: ar, encoding: .utf8)!
    }

    public static func decodeTo(bytes s: String) throws -> [UInt8] {
        if s.count % 2 != 0 {
            throw HareError("Invalid string :\(s)")
        }
        var arr = [UInt8]()
        var hi: Int = 0
        var lo: Int = 0
        var idx: Int = 0
        for ch in s {
            if idx % 2 == 0 {
                if let v = ch.hexDigitValue {
                    hi = v
                } else {
                    throw HareError("Invalid char '\(ch)' in string :\(s)")
                }
            } else {
                if let v = ch.hexDigitValue {
                    lo = v
                } else {
                    throw HareError("Invalid char '\(ch)' in string :\(s)")
                }
                arr.append(UInt8(((hi & 0xFF) << 4) | (lo & 0xFF)))
            }
            idx += 1
        }
        return arr
    }
}
