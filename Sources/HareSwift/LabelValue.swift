//
// Created by entaoyang@163.com on 2022-11-19.
// Copyright (c) 2022 entao.dev. All rights reserved.
//

import Foundation

public struct StringValue<T: Any>: CustomStringConvertible {
    public typealias ValueType = T
    public var key: String
    public var value: T

    public init(key: String, value: T) {
        self.key = key
        self.value = value
    }

    public var description: String {
        "StringValue{key:\(key), value:\(value)}"
    }
}

extension StringValue: Codable where ValueType: Codable {

}



public class Pair<A: Any, B: Any>: ToString {
    public let first: A
    public let second: B
    public init(_ first: A, _ second: B) {
        self.first = first
        self.second = second
    }
    public func toString() -> String {
        return "Pair(first: \(first), second: \(second))"
    }
}

public class LabelValue<T: Any>: ToString {
    public let label: String
    public let value: T
    public init(_ label: String, _ value: T) {
        self.label = label
        self.value = value
    }
    public func toString() -> String {
        return "LabelValue(label:\(label), value: \(value))"
    }
}
