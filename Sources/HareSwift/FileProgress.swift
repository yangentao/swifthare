//
//  FileProgress.swift
//  HareSwift
//
//  Created by Entao on 2024/12/30.
//

import Foundation

public class FileProgress: CustomStringConvertible {
    public let progress: Progress
    private let onCancel: VoidBlock?
    public var progressCallback: ProgressCallback? = nil
    public init(url: URL, total: Int = 100, kind: Progress.FileOperationKind = .downloading, onCancel: VoidBlock? = nil) {
        self.onCancel = onCancel
        let userInfo: [ProgressUserInfoKey: Any] = [
            .fileOperationKindKey: kind,
            .fileURLKey: url,
        ]
        progress = Progress(parent: nil, userInfo: userInfo)
        progress.kind = .file
        progress.totalUnitCount = Int64(total)
        progress.isPausable = false
        progress.isCancellable = true
        progress.cancellationHandler = { [weak self] in
            self?.cancel()
            self?.onCancel?()
        }
        #if os(macOS)
            progress.publish()
        #endif
    }
    public var description: String {
        return "FileProgress(total: \(progress.totalUnitCount), current: \(current), canceled: \(isCanceled), finished: \(isFinished), indeterminate: \(progress.isIndeterminate) )"
    }
    /// 持续更新current属性, current = result.current
    public func continueUpdate(_ onQuery: () -> (alive: Bool, current: Int)) async {
        while self.isAlive {
            await sleep(mill: 100)
            let q = onQuery()
            self.current = q.current
            if !q.alive {
                break
            }
        }
        if !self.isAlive {
            self.unpublish()
        }
    }
    /// 持续更新current属性, current += result.value
    public func continueIncrease(_ onQuery: () -> (alive: Bool, value: Int)) async {
        while self.isAlive {
            await sleep(mill: 100)
            let q = onQuery()
            self.current += q.value
            if !q.alive {
                break
            }
        }
        if !self.isAlive {
            self.unpublish()
        }
    }

    public func unpublish() {
        #if os(macOS)
            self.progress.unpublish()
        #endif
    }

    public var isAlive: Bool {
        return !(progress.isCancelled || progress.isFinished)
    }
    public var isCanceled: Bool {
        progress.isCancelled
    }
    public var isFinished: Bool {
        return progress.isFinished
    }
    public func cancel() {
        if !progress.isCancelled {
            progress.cancel()
        }
        unpublish()
    }

    public func finish(success: Bool = true) {
        if success {
            self.current = self.progress.totalUnitCount.intValue
        } else if !progress.isCancelled {
            progress.cancel()
        }
        unpublish()
    }
    public func increase(value: Int) {
        self.current += value
    }
    public var current: Int {
        get {
            return Int(progress.completedUnitCount)
        }
        set(newValue) {
            progress.completedUnitCount = Int64(newValue)
            if progress.isFinished {
                unpublish()
            }
            progressCallback?(progress.totalUnitCount.intValue, progress.completedUnitCount.intValue)
        }
    }
}
