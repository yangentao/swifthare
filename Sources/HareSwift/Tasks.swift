//
// Created by entaoyang@163.com on 2017/10/10.
// Copyright (c) 2017 yet.net. All rights reserved.
//

import Foundation

@discardableResult
public func backTask(_ block: @escaping () -> Void) -> BlockOperation {
    let op = BlockOperation(block: block)
    TaskX.multiQueue.addOperation(op)
    return op
}
@discardableResult
public func foreTask(_ block: @escaping () -> Void) -> BlockOperation {
    let op = BlockOperation(block: block)
    OperationQueue.main.addOperation(op)
    return op
}
public func foreDelay(_ seconds: TimeInterval, _ block: @escaping () -> Void) {
    OperationQueue.main.schedule(after: OperationQueue.main.now.advanced(by: .seconds(seconds)), block)
}
public var isMainThread: Bool {
    Thread.current.isMainThread
}

public class DelayTrigger {
    private let delay: TimeInterval
    private let action: VoidBlock
    private var counter: UInt = 0
    private var actionCounter: UInt = 0
    private let maxDelay: TimeInterval
    private var preInvokeTime: TimeInterval = 0

    public init(delay: TimeInterval = 0.5, maxDelay: TimeInterval = 0, action: @escaping VoidBlock) {
        self.delay = delay
        self.maxDelay = maxDelay
        self.action = action
    }

    public func cancel() {
        actionCounter = 0
    }

    public func trigger() {
        counter += 1
        if actionCounter > 0 {
            return
        }
        actionCounter = counter
        Tasks.foreDelay(seconds: delay, self.invokeAction)
    }

    private func invokeAction() {
        if actionCounter != counter {
            actionCounter = counter
            let tm = Date().timeIntervalSince1970
            if maxDelay > 0 && tm > preInvokeTime + maxDelay {
                invokeAction()
            } else {
                Tasks.foreDelay(seconds: delay, self.invokeAction)
            }
        } else if actionCounter > 0 {
            actionCounter = 0
            preInvokeTime = Date().timeIntervalSince1970
            action()
        }
    }
}

//  for i in 0..<10 {
//      TaskX.back {
//          printX(i)
//      }
//
//  }
//  TaskX.schedule(after: 2) {
//      printX("After 2 seconds")
//  }
//  TaskX.barrier{
//      printX("Barrier.")
//  }
//  TaskX.back {
//      printX(9999)
//  }
public class TaskX {
    private static func queryMaxCPU() -> Int {
        var ncpu: UInt32 = 0
        var len: size_t = 4
        sysctlbyname("hw.ncpu".cString(using: .utf8), &ncpu, &len, nil, 0)
        return max(1, Int(ncpu))
    }

    public static let cpuCount: Int = queryMaxCPU()

    public static let multiQueue: OperationQueue = OperationQueue().apply {
        $0.maxConcurrentOperationCount = OperationQueue.defaultMaxConcurrentOperationCount
        $0.qualityOfService = .utility
        $0.name = "MultiOperationQueue"
    }
    public static let singleQueue: OperationQueue = {
        OperationQueue().apply {
            $0.maxConcurrentOperationCount = 1
            $0.qualityOfService = .utility
            $0.name = "SingleOperationQueue"
        }
    }()

}

extension TaskX {
    public static func schedule(after seconds: Double, _ block: @escaping () -> Void) {
        multiQueue.schedule(
            after: multiQueue.now.advanced(by: .seconds(seconds)),
            tolerance: multiQueue.minimumTolerance,
            options: nil, block
        )
    }

    public static func barrier(_ block: @escaping () -> Void) {
        multiQueue.addBarrierBlock(block)
    }

    @discardableResult
    public static func back(_ block: @escaping () -> Void) -> BlockOperation {
        let op = BlockOperation(block: block)
        multiQueue.addOperation(op)
        return op
    }
    @discardableResult
    public static func fore(_ block: @escaping () -> Void) -> BlockOperation {
        let op = BlockOperation(block: block)
        OperationQueue.main.addOperation(op)
        return op
    }
}

public class TaskMerge: NSObject {
    public let queue: OperationQueue = OperationQueue()

    public override init() {
        super.init()
        queue.maxConcurrentOperationCount = 1
        queue.qualityOfService = .utility
    }

    public convenience init(name: String) {
        self.init()
        queue.name = name
    }

    public func schedule(after seconds: Double, _ block: @escaping () -> Void) {
        queue.cancelAllOperations()
        queue.schedule(after: seconds) {
            OperationQueue.main.addOperation(block)
        }
    }
}

extension OperationQueue {
    public func schedule(after seconds: Double, _ block: @escaping () -> Void) {
        self.schedule(
            after: self.now.advanced(by: .seconds(seconds)),
            tolerance: .seconds(0.5),
            options: nil, block)
    }
}

extension Double {
    public var afterNow: DispatchTime {
        DispatchTime.now() + self
    }
}

extension Int {
    public var afterNow: DispatchTime {
        DispatchTime.now() + Double(self)
    }
}

private var onceSet: Set<String> = Set<String>()

public class ScheduleItem {
    public let name: String
    fileprivate var block: VoidBlock?
    fileprivate var time: Double = Date().timeIntervalSince1970

    public init(name: String, block: @escaping VoidBlock) {
        self.name = name
        self.block = block
    }

    public var canceled: Bool {
        self.block == nil
    }

    public func cancel() {
        block = nil
    }
}

public class Tasks {
    private static var tasks = [ScheduleItem]()
}

extension Tasks {
    public static func mergeFore(_ name: String, _ block: @escaping VoidBlock) {
        self.mergeFore(name, 0.3, block)
    }

    public static func mergeFore(_ name: String, _ second: Double, _ block: @escaping VoidBlock) {

        let item = ScheduleItem(name: name, block: block)
        self.tasks.append(item)
        DispatchQueue.main.asyncAfter(deadline: second.afterSeconds) {
            if let b = item.block {
                for t in self.tasks {
                    if t.name == name {
                        t.cancel()
                    }
                }
                self.tasks.removeAll {
                    $0.block == nil
                }
                b()
            }
        }
    }

    public static func fore(_ block: @escaping VoidBlock) {
        DispatchQueue.main.async(execute: block)
    }

    public static func foreDelay(seconds: Double, _ block: @escaping VoidBlock) {
        let a = DispatchTime.now() + seconds
        DispatchQueue.main.asyncAfter(deadline: a, execute: block)
    }

    public static func back(_ block: @escaping VoidBlock) {
        DispatchQueue.global().async(execute: block)
    }

    public static func backDelay(seconds: Double, _ block: @escaping VoidBlock) {
        let a = DispatchTime.now() + seconds
        DispatchQueue.global().asyncAfter(deadline: a, execute: block)
    }

    public static func foreSchedule(_ second: Double, _ block: @escaping VoidBlock) -> ScheduleItem {
        let item = ScheduleItem(name: "", block: block)
        DispatchQueue.main.asyncAfter(deadline: second.afterSeconds) {
            item.block?()
            item.block = nil
        }
        return item
    }

    public static func runOnce(_ key: String, _ block: () -> Void) {
        if !onceSet.contains(key) {
            onceSet.insert(key)
            block()
        }
    }
}

public class TaskQueue {
    public let queue: DispatchQueue

    public init(_ label: String) {
        self.queue = DispatchQueue(label: label, qos: .default)
    }

    public func fore(_ block: @escaping VoidBlock) {
        DispatchQueue.main.async(execute: block)
    }

    public func foreDelay(seconds: Double, _ block: @escaping VoidBlock) {
        let a = DispatchTime.now() + seconds
        DispatchQueue.main.asyncAfter(deadline: a, execute: block)
    }

    public func sync(_ block: @escaping VoidBlock) {
        self.queue.sync(execute: block)
    }

    public func backDelay(seconds: Double, _ block: @escaping VoidBlock) {
        let a = DispatchTime.now() + seconds
        self.queue.asyncAfter(deadline: a, execute: block)
    }

    public func back(_ block: @escaping VoidBlock) {
        self.queue.async(execute: block)
    }
}

public class Sync {
    private weak var obj: AnyObject? = nil
    private var state: Int = 0

    public init(_ obj: AnyObject) {
        self.obj = obj
        state = 0
    }

    @discardableResult
    public func enter() -> Sync {
        if let ob = self.obj {
            objc_sync_enter(ob)
            state = 1
        }
        return self
    }

    public func exit() {
        if let ob = self.obj {
            objc_sync_exit(ob)
            state = 2
        }

    }

    deinit {
        if self.state == 1 {
            self.exit()
        }
    }
}

public func sync(_ obj: Any, _ block: () -> Void) {
    objc_sync_enter(obj)
    defer {
        objc_sync_exit(obj)
    }
    block()
}

public func syncRet<T>(_ obj: Any, _ block: () -> T) -> T {
    objc_sync_enter(obj)
    defer {
        objc_sync_exit(obj)
    }
    return block()
}

public func syncR<T>(_ obj: Any, _ block: () -> T) -> T {
    objc_sync_enter(obj)
    defer {
        objc_sync_exit(obj)
    }
    return block()
}

//extension DispatchQueue {

//	@available(iOS 13.0, *)
//	func scheduleAfter(_ seconds: Double, interval: Double, block: @escaping () -> Void) -> Cancellable {
//		let b = DispatchQueue.SchedulerTimeType.Stride(floatLiteral: interval)
//		return DispatchQueue.main.schedule(after: DispatchQueue.SchedulerTimeType(seconds.afterNow), interval: b, block)
//	}
//}
