//
// Created by entaoyang@163.com on 2017/10/25.
// Copyright (c) 2017 yet.net. All rights reserved.
//

import Foundation

public class File {
    public let path: String

    public init(path: String) {
        self.path = path
    }

    public convenience init(parent: String, filename: String) {
        self.init(path: parent.appendPath(filename))
    }

    public static var mainBundle: File {
        File(path: Bundle.main.bundlePath)
    }

    public var lastPath: String {
        path.lastPath
    }
    public var parentPath: String {
        path.parentPath
    }
    public var parentFile: File {
        File(path: self.parentPath)
    }

    public var size: Int {
        return path.fileSize
    }

    public var exist: Bool {
        path.existAny
    }

    public var isFile: Bool {
        path.existFile
    }
    public var isDir: Bool {
        path.existDirectory
    }

    public func readText(_ encoding: String.Encoding = .utf8) throws -> String? {
        return try String(contentsOfFile: path, encoding: encoding)
    }

    public func write(text: String) throws {
        try text.write(toFile: path, atomically: true, encoding: String.Encoding.utf8)
    }

    public func readData() throws -> Data? {
        try Data(contentsOf: path.fileURL, options: Data.ReadingOptions.uncached)
    }

    public func write(data: Data) throws {
        try data.write(to: path.fileURL)
    }

    public func remove() throws {
        try FileManager.default.removeItem(atPath: path)
    }

    public func list(deep: Bool = false) throws -> [String] {
        try path.list(deep: deep)
    }

    public func mkdir() throws {
        try path.mkdirs()
    }
    public func createFile() throws -> Bool {
        return try path.fileCreate()
    }
}
