//
// Created by entaoyang@163.com on 2022/9/13.
//

import Foundation

extension UnsafePointer<CChar> {
    public func string(_ enc: String.Encoding = .utf8) -> String? {
        return String(cString: self, encoding: enc)
    }
}
extension String {
    public var ccharPointer: UnsafePointer<CChar>? {
        return self.withCString { $0 }
    }
    public func cstring(_ enc: String.Encoding = .utf8) -> [CChar] {
        return self.cString(using: enc) ?? []
    }

}

extension String {

    public var toLatin: String {
        if self.isEmpty {
            return self
        }
        let s: CFMutableString = NSMutableString(string: self) as CFMutableString
        CFStringTransform(s, nil, kCFStringTransformToLatin, false)
        CFStringTransform(s, nil, kCFStringTransformStripCombiningMarks, false)
        return ((s as NSString) as String).lowercased()
    }
}

public typealias Regex = NSRegularExpression

public func MakeRegex(_ pattern: String, options: NSRegularExpression.Options = []) -> NSRegularExpression {
    return try! NSRegularExpression(pattern: pattern, options: options)
}
public func regexMake(_ pattern: String, options: NSRegularExpression.Options = []) -> NSRegularExpression {
    return try! NSRegularExpression(pattern: pattern, options: options)
}

private let regStringArg = try! Regex(pattern: "\\{.+\\}", options: [])

extension String {
    public func replaced(of: String, with text: String, options: String.CompareOptions = []) -> String {
        return self.replacingOccurrences(of: of, with: text, options: options)
    }

    public var quoted: String {
        return "\"\(self)\""
    }
    public var quotedSingle: String {
        return "'\(self)'"
    }
    public func formated(_ arg: String) -> String {
        return self.replaced(regex: regStringArg, by: arg)
    }

    public func local(arg: String) -> String {
        self.local.formated(arg)
    }
}

public let argReg = regexMake("\\{[\\w\\.\\s]+\\}")

extension String {
    public func matchedList(regex: NSRegularExpression) -> [String] {
        self.regex(regex).matchedStrings
    }

    public func match(regex: NSRegularExpression) -> Bool {
        matches(regex: regex).notEmpty
    }

    public func match(_ reg: String, ignoreCase: Bool = false) -> Bool {
        if ignoreCase {
            return matches(regex: try! NSRegularExpression(pattern: reg, options: [.caseInsensitive])).notEmpty
        } else {
            return matches(regex: try! NSRegularExpression(pattern: reg, options: [])).notEmpty
        }
    }

    public func matches(regex: NSRegularExpression) -> [NSTextCheckingResult] {
        regex.matches(in: self, range: NSMakeRange(0, self.count))
    }

    public func replaced(regex: NSRegularExpression, by content: String) -> String {
        regex.stringByReplacingMatches(in: self, range: NSMakeRange(0, self.count), withTemplate: content)
    }

    public func replaced(namedArgs: [String: String], regex: Regex = argReg, miss: ((String) -> String)? = nil) -> String {
        let ls: [NSRange] = regex.matches(in: self, options: [], range: NSMakeRange(0, self.count)).map {
            $0.range
        }
        var buf: String = ""
        buf.reserveCapacity(self.count)
        var preRange: NSRange = .init(location: 0, length: 0)
        for r in ls {
            buf.append(self.substring(range: preRange.between(r)))
            let s = self.substring(range: r)
            let key = s.trim(char: "{", "}").trimed
            let value = namedArgs[key] ?? miss?(key) ?? ""
            buf.append(value)
            preRange = r
        }
        buf.append(self.substring(range: preRange.between(NSRange(location: self.count, length: 0))))
        return buf
    }
}

extension NSRange {
    public func between(_ r: NSRange) -> NSRange {
        if self.end < r.location {
            return NSRange(location: self.end, length: (r.location - self.end).ge(0))
        }
        if r.end < self.location {
            return NSRange(location: r.end, length: (self.location - r.end).ge(0))
        }
        return NSRange(location: self.end, length: 0)
    }
    public var end: Int {
        self.location + self.length
    }
}

extension String {

    @discardableResult
    public func onNotEmpty<R: Any>(_ block: (String) -> R) -> R? {
        if self.notEmpty {
            return block(self)
        }
        return nil
    }
    public func onEmpty(replace: String) -> String {
        if self.isEmpty {
            return replace
        }
        return self
    }
    public func onEQ(value: String, replace: String) -> String {
        if self == value {
            return replace
        }
        return self
    }

    public func onNE(value: String, replace: String) -> String {
        if self != value {
            return replace
        }
        return self
    }
}

public func buildString(_ items: Any?..., sep: String = "", nullText: String = "nil") -> String {
    var buf: String = ""
    for a in items {
        if let b = a ?? nil {
            print(b, terminator: sep, to: &buf)
        } else {
            print(nullText, terminator: sep, to: &buf)
        }
    }
    return buf
}

public protocol ToString: CustomStringConvertible {
    func toString() -> String
}

extension ToString {
    public var description: String {
        self.toString()
    }
}

extension Substring {

    public func toString() -> String {
        String(self)
    }

    public var stringValue: String {
        String(self)
    }

}

extension String {
    public var toInt: Int? {
        Int(self)
    }
    public var toDouble: Double? {
        Double(self)
    }
    public var toBool: Bool? {
        Bool(self)
    }
    public var toLong: Long? {
        Long(self)
    }

    public var notEmpty: Bool {
        !self.isEmpty
    }
    public var urlEncoded: String {
        self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    public var urlDecoded: String {
        self.removingPercentEncoding ?? ""
    }
    public var base64Encoded: String {
        self.data(using: .utf8)!.base64EncodedString()
    }
    public var dataUtf8: Data {
        self.data(using: String.Encoding.utf8, allowLossyConversion: false)!
    }
    public var dataUnicode: Data {
        self.data(using: String.Encoding.unicode, allowLossyConversion: false)!
    }

    public var local: String {
        return NSLocalizedString(self, comment: "")
    }
}

extension String {
    public func split(char: Character, limit: Int) -> [String] {
        self.split(separator: char, maxSplits: limit).map {
            $0.toString()
        }
    }

    public func split(char cs: Character..., maxSplits: Int = Int.max) -> [String] {
        self.split(maxSplits: maxSplits) {
            cs.contains($0)
        }.map {
            String($0)
        }
    }

    public func replaced(map: [Character: String]) -> String {
        var buf: String = ""
        buf.reserveCapacity(self.count + self.count / 2 + 16)
        for ch in self {
            if let s = map[ch] {
                buf.append(s)
            } else {
                buf.append(ch)
            }
        }
        return buf
    }

    public func replaced(chars: [Character], to str: String) -> String {
        var buf: String = ""
        buf.reserveCapacity(self.count + self.count / 2 + 16)
        for ch in self {
            if chars.contains(ch) {
                buf.append(str)
            } else {
                buf.append(ch)
            }
        }
        return buf
    }

    public func endWith(_ s: String) -> Bool {
        self.hasSuffix(s)
    }

    public func startWith(_ s: String) -> Bool {
        self.hasPrefix(s)
    }

    public func char(at n: Int) -> Character? {
        if n >= 0 && n < self.count {
            return self[self.idx(n)]
        }
        return nil
    }

    public func at(_ n: Int) -> Character {
        self[self.idx(n)]
    }

    //没有返回-1
    public func lastIndexOf(_ c: Character) -> Int {
        if let a = self.lastIndex(of: c) {
            return self.distance(from: self.startIndex, to: a)
        }
        return -1

    }

    public func indexOf(_ c: Character) -> Int? {
        if let n = self.firstIndex(of: c) {
            return self.distance(from: self.startIndex, to: n)
        }
        return nil
    }
}

//trims

extension String {

    public var trimed: String {
        self.trimmingCharacters(in: .whitespacesAndNewlines)
    }

    public func trim(in cs: String) -> String {
        self.trimmingCharacters(in: CharacterSet(charactersIn: cs))
    }

    public func trim(char cs: Character...) -> String {
        trim(chars: cs)
    }

    public func trim(chars cs: [Character]) -> String {
        var s = String()
        s.append(contentsOf: cs)
        return self.trimmingCharacters(in: CharacterSet(charactersIn: s))
    }

    public func trimEnd(char cs: Character...) -> String {
        trimEnd(chars: cs)
    }

    public func trimEnd(chars cs: [Character]) -> String {
        var end: String.Index = self.endIndex
        for ch in self.reversed() {
            if cs.contains(ch) {
                end = self.index(before: end)
            } else {
                break
            }
        }
        return String(self[..<end])
    }

    public func trimStart(char cs: Character...) -> String {
        trimStart(chars: cs)
    }

    public func trimStart(chars cs: [Character]) -> String {
        var from: String.Index = self.startIndex
        for ch in self {
            if cs.contains(ch) {
                from = self.index(after: from)
            } else {
                break
            }
        }
        return String(self[from...])
    }

}

extension String {
    public init(capcity: Int) {
        self.init()
        self.reserveCapacity(capcity)
    }

    public init(chars: [Character]) {
        self.init(capcity: chars.count + 2)
        self.append(contentsOf: chars)
    }
}

//substring
extension String {

    public func head(_ size: Int) -> String {
        if size >= self.count {
            return self
        }
        return self.substring(from: 0, length: size)
    }

    public func tail(_ size: Int) -> String {
        var from = self.count - size
        if from < 0 {
            from = 0
        }
        return self.substring(from: from)
    }
}

extension String {
    public func idx(_ n: Int) -> String.Index {
        self.index(startIndex, offsetBy: n)
    }

    public func substring(range: NSRange) -> String {
        substring(from: range.location, length: range.length)
    }

    public func substring(tail size: Int, skip: Int = 0) -> String {
        let from: Int = max(0, self.count - size - skip)
        return substring(from: from, to: max(from, self.count - skip))
    }

    public func substring(head size: Int) -> String {
        substring(from: 0, length: min(size, self.count))
    }

    public func substring(from: Int, length: Int) -> String {
        self[idx(max(0, from))..<idx(from + length)].toString()
    }

    //[from, to)

    public func substring(from: Int, to: Int) -> String {
        self[idx(from)..<idx(to)].toString()
    }

    public func substring(from: Int) -> String {
        self[idx(from)...].toString()
    }

    public subscript(value: PartialRangeUpTo<Int>) -> String {
        get {
            self[..<self.idx(value.upperBound)].toString()
        }
    }

    public subscript(value: PartialRangeThrough<Int>) -> String {
        get {
            self[...self.idx(value.upperBound)].toString()
        }
    }

    public subscript(value: PartialRangeFrom<Int>) -> String {
        get {
            self[self.idx(value.lowerBound)...].toString()
        }
    }
    public subscript(value: ClosedRange<Int>) -> String {
        get {
            self[self.idx(value.lowerBound)...self.idx(value.upperBound)].toString()
        }
    }
    public subscript(value: Range<Int>) -> String {
        get {
            self[self.idx(value.lowerBound)..<self.idx(value.upperBound)].toString()
        }
    }

    public func substringAfter(char delimiter: Character, onMiss: String? = nil) -> String {
        if let n = self.firstIndex(of: delimiter) {
            return String(self[self.index(after: n)...])
        } else {
            return onMiss ?? self
        }

    }

    public func substringAfterLast(char delimiter: Character, onMiss: String? = nil) -> String {
        if let n = self.lastIndex(of: delimiter) {
            return String(self[self.index(after: n)...])
        } else {
            return onMiss ?? self
        }

    }

    public func substringBefore(char delimiter: Character, onMiss: String? = nil) -> String {
        if let n = self.firstIndex(of: delimiter) {
            return String(self[self.startIndex..<n])
        } else {
            return onMiss ?? self
        }

    }

    public func substringBeforeLast(char delimiter: Character, onMiss: String? = nil) -> String {
        if let n = self.lastIndex(of: delimiter) {
            return String(self[self.startIndex..<n])
        } else {
            return onMiss ?? self
        }
    }
}

public class StringRegex {
    public let text: String
    public let regex: Regex

    public init(text: String, regex: Regex) {
        self.text = text
        self.regex = regex
    }

    public var firstMatchedString: String? {
        if let r = firstRange {
            return text.substring(range: r)
        }
        return nil
    }
    public var matchedStrings: [String] {
        ranges.map {
            text.substring(range: $0)
        }
    }

    public var firstRange: NSRange? {
        firstResult?.range
    }
    public var firstResult: NSTextCheckingResult? {
        regex.firstMatch(in: text, range: NSMakeRange(0, text.count))
    }

    public var ranges: [NSRange] {
        results.map {
            $0.range
        }
    }

    public var results: [NSTextCheckingResult] {
        regex.matches(in: text, range: NSMakeRange(0, text.count))
    }

    public func replaceFirst(by content: String) -> String? {
        if let r = self.firstRange {
            var buf = String(capcity: text.count + content.count)
            buf.append(contentsOf: text[0..<r.location])
            buf.append(content)
            buf.append(contentsOf: text[r.end...])
            return buf
        } else {
            return nil
        }
    }

    public func replaceAll(by content: String) -> String {
        regex.stringByReplacingMatches(in: text, range: NSMakeRange(0, text.count), withTemplate: content)
    }
    public func replaceAll(_ block: (String) -> String) -> String {
        let ranges: [NSRange] = self.ranges
        if ranges.isEmpty { return self.text }
        var buf = ""
        var preRange: NSRange? = nil
        for range in ranges {
            if let pre = preRange {
                if pre.location + pre.length < range.location {
                    buf.append(self.text.substring(from: pre.location + pre.length, to: range.location))
                }
            } else {
                if range.location > 0 {
                    buf.append(self.text.substring(from: 0, to: range.location))
                }
            }
            let rep = block(self.text.substring(range: range))
            buf.append(rep)
            preRange = range
        }
        if let r = preRange {
            if r.location + r.length < self.text.count {
                buf.append(self.text.substring(from: r.location + r.length))
            }
        }
        return buf
    }
}

extension String {
    public func regex(pattern: String) -> StringRegex {
        StringRegex(text: self, regex: try! Regex(pattern: pattern))
    }

    public func regex(_ regex: Regex) -> StringRegex {
        StringRegex(text: self, regex: regex)
    }
}

func testStr() {
    let path = "/Users/entao/"
    print("path: " + path)
    print("trimStart/: " + path.trimStart(char: "/", ":"))
    print("trimEnd/: " + path.trimEnd(char: "/", ":"))

    let file = "/Users/entao/dog.png"
    print("file: " + file)
    print("lastPathSeg: " + file.lastPath)
    print("after/: " + file.substringAfter(char: "/"))
    print("afterLast/: " + file.substringAfterLast(char: "/"))
    print("before/: " + file.substringBefore(char: "/"))
    print("beforeLast.: " + file.substringBeforeLast(char: "."))
}

private let lowerCaseMap: [Character: Character] = [
    "A": "a", "B": "b",
    "C": "c", "D": "d",
    "E": "e", "F": "f",
    "G": "g", "H": "h",
    "I": "i", "J": "j",
    "K": "k", "L": "l",
    "M": "m", "N": "n",
    "O": "o", "P": "p",
    "Q": "q", "R": "r",
    "S": "s", "T": "t",
    "U": "u", "V": "v",
    "W": "w", "X": "x",
    "Y": "y", "Z": "z",
]

private let upperCaseMap: [Character: Character] = [
    "a": "A", "b": "B",
    "c": "C", "d": "D",
    "e": "E", "f": "F",
    "g": "G", "h": "H",
    "i": "I", "j": "J",
    "k": "K", "l": "L",
    "m": "M", "n": "N",
    "o": "O", "p": "P",
    "q": "Q", "r": "R",
    "s": "S", "t": "T",
    "u": "U", "v": "V",
    "w": "W", "x": "X",
    "y": "Y", "z": "Z",
]

extension Character {
    public var isNumber: Bool {
        (self >= "0" && self <= "9")
    }
    public var isAlpha: Bool {
        (self >= "a" && self <= "z") || (self >= "A" && self <= "Z")
    }
    public var lowerCase: Character {
        lowerCaseMap[self] ?? self
    }
    public var upperCase: Character {
        upperCaseMap[self] ?? self
    }
}
