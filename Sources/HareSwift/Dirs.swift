//
// Created by entaoyang@163.com on 2022-11-29.
//

import Foundation

public class Dirs {
    public static let tempDir: String = NSTemporaryDirectory()
    public static let homeDir: String = NSHomeDirectory()
    public static let cacheDir: String = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0]
    public static let docDir: String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]

    public static func doc(name: String) -> File {
        File(path: docDir.appendPath(name))
    }

    public static func temp(name: String) -> File {
        File(path: tempDir.appendPath(name))
    }
    public static func cache(name: String) -> File {
        File(path: Dirs.cacheDir.appendPath(name))
    }
}

public func DocPath(name: String) -> String {
    NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0].appendPath(name)
}

public func CachePath(name: String) -> String {
    NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0].appendPath(name)
}

public func FileURL(_ file: String) -> URL {
    URL(fileURLWithPath: file, isDirectory: false)
}

public func DirURL(_ dir: String) -> URL {
    URL(fileURLWithPath: dir, isDirectory: true)
}
