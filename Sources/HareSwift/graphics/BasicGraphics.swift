//
// Created by entaoyang@163.com on 2022-11-26.
//

import Foundation

#if os(iOS)
    import UIKit
#else
    import AppKit
#endif

#if os(macOS)
    extension NSEdgeInsets {
        public static let zero: NSEdgeInsets = .init(top: 0, left: 0, bottom: 0, right: 0)
    }

    extension CGRect {
        public func inset(by insets: NSEdgeInsets) -> CGRect {
            CGRect(x: self.origin.x + insets.left, y: self.origin.y + insets.top, width: self.size.width - insets.hor, height: self.size.height - insets.ver)
        }
    }

    extension NSView {
        public func setNeedsLayout() {
            self.needsLayout = true
        }
    }
#endif

extension String {
    public func sizeBy(font: XFont = .systemFont(ofSize: 15)) -> CGSize {
        let a: [NSAttributedString.Key: Any] = [.font: font]
        let sz = NSString(string: self).size(withAttributes: a)
        return CGSize(width: ceil(sz.width), height: ceil(sz.height))
    }
}

extension XFont {
    public func heightOf(_ ch: String = "W") -> CGFloat {
        ch.sizeBy(font: self).height
    }
}
