//
//  AnchorBuilder.swift
//  HareSwift
//
//  Created by Entao on 2025/1/3.
//
import Foundation

#if os(iOS)
    import UIKit
#else
    import AppKit
#endif

extension XView {
    @discardableResult
    public func anchorBuilder(@AnchorBuilder _ content: (AnchorContext) -> AnchorItem) -> Self {
        if let olds: [NSLayoutConstraint] = self.getAttr(key: "_anchorConstraints"), olds.notEmpty {
            NSLayoutConstraint.deactivate(olds)
        }
        let c = AnchorContext(view: self)
        let items: [AnchorItem] = content(c).flat()
        let cs: [NSLayoutConstraint] = items.compactMap({ $0.constraint })
        self.setAttr(key: "_anchorConstraints", value: cs)
        NSLayoutConstraint.activate(cs)
        return self
    }
}

public typealias AnchorAttr = NSLayoutConstraint.Attribute
public struct AnchorItem: CustomStringConvertible {
    public let constraint: NSLayoutConstraint?
    public let items: [AnchorItem]

    public init(_ constraint: NSLayoutConstraint) {
        self.constraint = constraint
        self.items = []
    }
    public init(items: [AnchorItem]) {
        self.constraint = nil
        self.items = items
    }
    public func priority(_ p: XLayoutPriority) -> Self {
        self.constraint?.priority = p
        return self
    }

    public func flat() -> [AnchorItem] {
        if items.isEmpty {
            return [self]
        } else {
            return self.items.flatMap({ $0.flat() })
        }
    }
    public var description: String {
        buildString("AnchorItem: ", constraint, items, sep: ", ")
    }
}

@resultBuilder
public struct AnchorBuilder {
    public static func buildArray(_ components: [AnchorItem]) -> AnchorItem {
        return AnchorItem(items: components)
    }
    public static func buildBlock(_ components: AnchorItem...) -> AnchorItem {
        return AnchorItem(items: components)
    }
    public static func buildEither(first: AnchorItem) -> AnchorItem {
        return first
    }
    public static func buildEither(second: AnchorItem) -> AnchorItem {
        return second
    }
}

public enum AnchorOperator {
    case eq, ge, le
}
public enum AnchorRelate {
    case mine
    case parent
    case view(_ v: XView)
    case previous
    case preOrParent
    case guide(_ g: XLayoutGuide)
}

public class AnchorContext {
    public let view: XView
    public let parent: XView
    public let preView: XView?

    public init(view: XView) {
        self.view = view
        self.parent = view.superview!
        self.preView = view.prevSibling
        self.view.translatesAutoresizingMaskIntoConstraints = false
    }
    public func allowStretch() -> AnchorItem {
        view.setContentHuggingPriority(.defaultLow, for: .horizontal)
        view.setContentHuggingPriority(.defaultLow, for: .vertical)
        return .init(items: [])
    }
    public func allowStretch(_ axis: XLayoutAxis) -> AnchorItem {
        view.setContentHuggingPriority(.defaultLow, for: axis)
        return .init(items: [])
    }
    public func keepWidth() -> AnchorItem {
        view.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        return .init(items: [])
    }
    public func keepHeight() -> AnchorItem {
        view.setContentCompressionResistancePriority(.defaultHigh, for: .vertical)
        return .init(items: [])
    }
    public func keepSize() -> AnchorItem {
        view.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        view.setContentCompressionResistancePriority(.defaultHigh, for: .vertical)
        return .init(items: [])
    }

    /// margins
    public func edges(_ inset: EdgeInsets = .zero, op: AnchorOperator = .eq) -> AnchorItem {
        let a = left(to: .parent, attr: .left, constant: inset.left, op: op)
        let b = right(to: .parent, attr: .right, constant: -inset.right, op: op)
        let c = top(to: .parent, attr: .top, constant: inset.top, op: op)
        let d = bottom(to: .parent, attr: .bottom, constant: -inset.bottom, op: op)
        return AnchorItem(items: [a, b, c, d])
    }
    /// margins
    public func edgeX(_ ml: CGFloat, mr: CGFloat = 0, op: AnchorOperator = .eq) -> AnchorItem {
        let a = left(to: .parent, attr: .left, constant: ml, op: op)
        let b = right(to: .parent, attr: .right, constant: -mr, op: op)
        return AnchorItem(items: [a, b])
    }
    /// margins
    public func edgeY(_ mt: CGFloat, mb: CGFloat = 0, op: AnchorOperator = .eq) -> AnchorItem {
        let c = top(to: .parent, attr: .top, constant: mt, op: op)
        let d = bottom(to: .parent, attr: .bottom, constant: -mb, op: op)
        return AnchorItem(items: [c, d])
    }

    public func safeBottom(_ constant: CGFloat) -> AnchorItem {
        bottom(to: .guide(parent.safeAreaLayoutGuide), attr: .bottom, constant: constant)
    }
    public func safeTop(_ constant: CGFloat) -> AnchorItem {
        top(to: .guide(parent.safeAreaLayoutGuide), attr: .top, constant: constant)
    }
    public func safeRight(_ constant: CGFloat) -> AnchorItem {
        right(to: .guide(parent.safeAreaLayoutGuide), attr: .right, constant: constant)
    }
    public func safeLeft(_ constant: CGFloat) -> AnchorItem {
        left(to: .guide(parent.safeAreaLayoutGuide), attr: .left, constant: constant)
    }

    public func aboveCenter(to relate: AnchorRelate = .parent, constant: CGFloat = 0) -> AnchorItem {
        bottom(to: relate, attr: .centerY, constant: constant)
    }
    public func belowCenter(to relate: AnchorRelate = .parent, constant: CGFloat = 0) -> AnchorItem {
        top(to: relate, attr: .centerY, constant: constant)
    }

    public func toLeft(of relate: AnchorRelate, constant: CGFloat = 0) -> AnchorItem {
        right(to: relate, attr: .left, constant: constant)
    }
    public func toRight(of relate: AnchorRelate, constant: CGFloat = 0) -> AnchorItem {
        left(to: relate, attr: .right, constant: constant)
    }
    public func below(to relate: AnchorRelate, constant: CGFloat = 0) -> AnchorItem {
        top(to: relate, attr: .bottom, constant: constant)
    }
    public func above(to relate: AnchorRelate, constant: CGFloat = 0) -> AnchorItem {
        bottom(to: relate, attr: .top, constant: constant)
    }

    public func size(_ w: CGFloat, _ h: CGFloat? = nil) -> AnchorItem {
        AnchorItem.init(items: [width(w), height(h ?? w)])
    }
    public func center(_ baisX: CGFloat = 0, _ baisY: CGFloat = 0) -> AnchorItem {
        AnchorItem.init(items: [centerX(baisX), centerY(baisY)])
    }

    public func lastBaseline(_ constant: CGFloat, op: AnchorOperator = .eq) -> AnchorItem {
        lastBaseline(to: .parent, attr: .lastBaseline, constant: constant, op: op)
    }
    public func lastBaseline(to relate: AnchorRelate, attr: AnchorAttr, constant: CGFloat = 0, op: AnchorOperator = .eq) -> AnchorItem {
        make(anchor: view.lastBaselineAnchor, other: anchorY(relate, attr), constant: constant, op: op)
    }
    public func firstBaseline(_ constant: CGFloat, op: AnchorOperator = .eq) -> AnchorItem {
        firstBaseline(to: .parent, attr: .firstBaseline, constant: constant, op: op)
    }
    public func firstBaseline(to relate: AnchorRelate, attr: AnchorAttr, constant: CGFloat = 0, op: AnchorOperator = .eq) -> AnchorItem {
        make(anchor: view.firstBaselineAnchor, other: anchorY(relate, attr), constant: constant, op: op)
    }
    public func bottom(_ constant: CGFloat, op: AnchorOperator = .eq) -> AnchorItem {
        bottom(to: .parent, attr: .bottom, constant: constant, op: op)
    }
    public func bottom(to relate: AnchorRelate, attr: AnchorAttr, constant: CGFloat = 0, op: AnchorOperator = .eq) -> AnchorItem {
        make(anchor: view.bottomAnchor, other: anchorY(relate, attr), constant: constant, op: op)
    }

    public func top(_ constant: CGFloat, op: AnchorOperator = .eq) -> AnchorItem {
        top(to: .parent, attr: .top, constant: constant, op: op)
    }
    public func top(to relate: AnchorRelate, attr: AnchorAttr, constant: CGFloat = 0, op: AnchorOperator = .eq) -> AnchorItem {
        make(anchor: view.topAnchor, other: anchorY(relate, attr), constant: constant, op: op)
    }

    public func centerY(_ constant: CGFloat, op: AnchorOperator = .eq) -> AnchorItem {
        centerY(to: .parent, attr: .centerY, constant: constant, op: op)
    }
    public func centerY(to relate: AnchorRelate, attr: AnchorAttr, constant: CGFloat = 0, op: AnchorOperator = .eq) -> AnchorItem {
        make(anchor: view.centerYAnchor, other: anchorY(relate, attr), constant: constant, op: op)
    }
    public func centerX(_ constant: CGFloat, op: AnchorOperator = .eq) -> AnchorItem {
        centerX(to: .parent, attr: .centerX, constant: constant, op: op)
    }
    public func centerX(to relate: AnchorRelate, attr: AnchorAttr, constant: CGFloat = 0, op: AnchorOperator = .eq) -> AnchorItem {
        make(anchor: view.centerXAnchor, other: anchorX(relate, attr), constant: constant, op: op)
    }
    public func trailing(_ constant: CGFloat, op: AnchorOperator = .eq) -> AnchorItem {
        trailing(to: .parent, attr: .trailing, constant: constant, op: op)
    }
    public func trailing(to relate: AnchorRelate, attr: AnchorAttr, constant: CGFloat = 0, op: AnchorOperator = .eq) -> AnchorItem {
        make(anchor: view.trailingAnchor, other: anchorX(relate, attr), constant: constant, op: op)
    }

    public func leading(_ constant: CGFloat, op: AnchorOperator = .eq) -> AnchorItem {
        leading(to: .parent, attr: .leading, constant: constant, op: op)
    }
    public func leading(to relate: AnchorRelate, attr: AnchorAttr, constant: CGFloat = 0, op: AnchorOperator = .eq) -> AnchorItem {
        make(anchor: view.leadingAnchor, other: anchorX(relate, attr), constant: constant, op: op)
    }

    public func right(_ constant: CGFloat, op: AnchorOperator = .eq) -> AnchorItem {
        right(to: .parent, attr: .right, constant: constant, op: op)
    }
    public func right(to relate: AnchorRelate, attr: AnchorAttr, constant: CGFloat = 0, op: AnchorOperator = .eq) -> AnchorItem {
        make(anchor: view.rightAnchor, other: anchorX(relate, attr), constant: constant, op: op)
    }
    public func left(_ constant: CGFloat, op: AnchorOperator = .eq) -> AnchorItem {
        left(to: .parent, attr: .left, constant: constant, op: op)
    }
    public func left(to relate: AnchorRelate, attr: AnchorAttr, constant: CGFloat = 0, op: AnchorOperator = .eq) -> AnchorItem {
        make(anchor: view.leftAnchor, other: anchorX(relate, attr), constant: constant, op: op)
    }

    public func width(_ value: CGFloat, op: AnchorOperator = .eq) -> AnchorItem {
        makeDim(anchor: view.widthAnchor, constant: value, op: op)
    }
    public func height(_ value: CGFloat, op: AnchorOperator = .eq) -> AnchorItem {
        makeDim(anchor: view.heightAnchor, constant: value, op: op)
    }
    public func width(to relate: AnchorRelate, attr: AnchorAttr = .width, multi: CGFloat = 1, constant: CGFloat = 0, op: AnchorOperator = .eq) -> AnchorItem {
        makeDim(anchor: view.widthAnchor, other: anchorDim(relate, attr), multi: multi, constant: constant, op: op)
    }
    public func height(to relate: AnchorRelate, attr: AnchorAttr = .height, multi: CGFloat = 1, constant: CGFloat = 0, op: AnchorOperator = .eq) -> AnchorItem {
        makeDim(anchor: view.heightAnchor, other: anchorDim(relate, attr), multi: multi, constant: constant, op: op)
    }
    public func make<T>(anchor: NSLayoutAnchor<T>, other: NSLayoutAnchor<T>, constant: CGFloat, op: AnchorOperator) -> AnchorItem {
        switch op {
        case .eq:
            return .init(anchor.constraint(equalTo: other, constant: constant))
        case .ge:
            return .init(anchor.constraint(greaterThanOrEqualTo: other, constant: constant))
        case .le:
            return .init(anchor.constraint(lessThanOrEqualTo: other, constant: constant))
        }
    }
    public func makeDim(anchor: NSLayoutDimension, other: NSLayoutDimension, multi: CGFloat, constant: CGFloat, op: AnchorOperator) -> AnchorItem {
        switch op {
        case .eq:
            return .init(anchor.constraint(equalTo: other, multiplier: multi, constant: constant))
        case .ge:
            return .init(anchor.constraint(greaterThanOrEqualTo: other, multiplier: multi, constant: constant))
        case .le:
            return .init(anchor.constraint(lessThanOrEqualTo: other, multiplier: multi, constant: constant))
        }
    }
    public func makeDim(anchor: NSLayoutDimension, constant: CGFloat, op: AnchorOperator) -> AnchorItem {
        switch op {
        case .eq:
            return .init(anchor.constraint(equalToConstant: constant))
        case .ge:
            return .init(anchor.constraint(greaterThanOrEqualToConstant: constant))
        case .le:
            return .init(anchor.constraint(lessThanOrEqualToConstant: constant))
        }
    }
    private func viewOrGuide(_ r: AnchorRelate) -> Any {
        switch r {
        case .parent:
            return parent
        case .previous:
            return preView!
        case .preOrParent:
            return preView ?? parent
        case .view(let v):
            return v
        case .mine:
            return view
        case .guide(let g):
            return g
        }
    }
    private func anchorDim(_ relate: AnchorRelate, _ attr: AnchorAttr) -> NSLayoutDimension {
        let vg = viewOrGuide(relate)
        if let v = vg as? XView {
            switch attr {
            case .width:
                return v.widthAnchor
            case .height:
                return v.heightAnchor
            default:
                fatal(msg: "error side: \(attr)")
            }
        } else if let g = vg as? XLayoutGuide {
            switch attr {
            case .width:
                return g.widthAnchor
            case .height:
                return g.heightAnchor
            default:
                fatal(msg: "error side: \(attr)")
            }
        } else {
            fatal(msg: "error side: \(attr)")
        }
    }
    private func anchorX(_ relate: AnchorRelate, _ attr: AnchorAttr) -> NSLayoutXAxisAnchor {
        let vg = viewOrGuide(relate)
        if let v = vg as? XView {
            switch attr {
            case .leading:
                return v.leadingAnchor
            case .trailing:
                return v.trailingAnchor
            case .left:
                return v.leftAnchor
            case .right:
                return v.rightAnchor
            case .centerX:
                return v.centerXAnchor
            default:
                fatal(msg: "error side: \(attr)")
            }
        } else if let g = vg as? XLayoutGuide {
            switch attr {
            case .leading:
                return g.leadingAnchor
            case .trailing:
                return g.trailingAnchor
            case .left:
                return g.leftAnchor
            case .right:
                return g.rightAnchor
            case .centerX:
                return g.centerXAnchor
            default:
                fatal(msg: "error side: \(attr)")
            }
        } else {
            fatal(msg: "error side: \(attr)")
        }
    }
    private func anchorY(_ relate: AnchorRelate, _ attr: AnchorAttr) -> NSLayoutYAxisAnchor {
        let vg = viewOrGuide(relate)
        if let v = vg as? XView {
            switch attr {
            case .top:
                return v.topAnchor
            case .bottom:
                return v.bottomAnchor
            case .firstBaseline:
                return v.firstBaselineAnchor
            case .lastBaseline:
                return v.lastBaselineAnchor
            case .centerY:
                return v.centerYAnchor
            default:
                fatal(msg: "error side: \(attr)")
            }
        } else if let g = vg as? XLayoutGuide {
            switch attr {
            case .top:
                return g.topAnchor
            case .bottom:
                return g.bottomAnchor
            case .centerY:
                return g.centerYAnchor
            default:
                fatal(msg: "error side: \(attr)")
            }
        } else {
            fatal(msg: "error side: \(attr)")
        }
    }
}
