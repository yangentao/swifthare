//
// Created by entaoyang@163.com on 2022-11-26.
//

import Foundation

#if os(iOS)
import UIKit
#else
import AppKit
#endif

public struct LayoutValue {
    public let fixed: CGFloat
    public let weight: CGFloat

    public init(fixed: CGFloat) {
        self.fixed = fixed.ge(0)
        self.weight = 0
    }

    public init(weight: CGFloat) {
        self.fixed = 0
        self.weight = weight.ge(0)
    }
}

public enum LayoutGravity: String {
    case left, right, center, top, bottom
}

public class StackParams {
    public var width: LayoutValue? = nil
    public var height: LayoutValue? = nil
    public var insets: EdgeInsets? = nil
    public var gravity: LayoutGravity? = nil
}

fileprivate extension AttrKey {
    static let stackCellConfig: AttrKey = .init("_stackcellconfig")
}

public extension XView {
    var stackParams: StackParams? {
        get {
            self[.stackCellConfig]
        }
        set {
            self[.stackCellConfig] = newValue
            if let sv = self.superview as? StackView {
                sv.needsRebuild()
            }
        }
    }

    @discardableResult
    func stackParams(_ block: (StackParams) -> Void) -> Self {
        let p: StackParams = self.stackParams ?? StackParams()
        block(p)
        self.stackParams = p
        return self
    }

}

open class BaseStackView: XView {
    public let childParams: StackParams = StackParams()

    @GreatEQ(wrappedValue: 1, minValue: 1)
    fileprivate var basePercent: CGFloat {
        didSet {
            needsRebuild()
        }
    }
    @GreatEQ(wrappedValue: 0, minValue: 0)
    public var space: CGFloat {
        didSet {
            updateSafeGuide()
            needsRebuild()
        }
    }

    public var paddings: EdgeInsets = .zero {
        didSet {
            updateSafeGuide()
        }
    }
    private var needBuildConstraints = false
    fileprivate var itemContraints: [NSLayoutConstraint] = []
    fileprivate let contentGuide: XLayoutGuide = XLayoutGuide()
    private var lc: NSLayoutConstraint? = nil
    private var rc: NSLayoutConstraint? = nil
    private var tc: NSLayoutConstraint? = nil
    private var bc: NSLayoutConstraint? = nil

    fileprivate let freeSpaceGuide: XLayoutGuide = XLayoutGuide()
    fileprivate var freeWidth: NSLayoutConstraint? = nil
    fileprivate var freeHeight: NSLayoutConstraint? = nil

    #if os(macOS)
    public var backgroundColor: NSColor? = nil {
        didSet {
            needsDisplay = true
        }
    }
    public var borderColor: NSColor? = nil {
        didSet {
            needsDisplay = true
        }
    }
    public var borderWidth: CGFloat = 1 {
        didSet {
            needsDisplay = true
        }
    }
    public var cornerRadius: CGFloat = 0 {
        didSet {
            needsDisplay = true
        }
    }

    public func styleBox() {
        self.backgroundColor = .windowBackgroundColor
        self.borderColor = .placeholderTextColor
        self.borderWidth = 1
        self.cornerRadius = 8
    }

    open override func draw(_ dirtyRect: NSRect) {
        let rect = self.bounds

        if let bc = backgroundColor {
            let path = NSBezierPath(roundedRect: rect, xRadius: cornerRadius, yRadius: cornerRadius)
            bc.setFill()
            path.fill()
        }
        if let sc = borderColor, borderWidth > 0 {
            let x = borderWidth / 2
            let path = NSBezierPath(roundedRect: rect.inset(by: .all(x )), xRadius: cornerRadius - x , yRadius: cornerRadius  - x )
            path.lineWidth = borderWidth
            sc.set()
            path.stroke()
        }

        super.draw(dirtyRect)
    }
    #endif

    public override init(frame: CGRect) {
        super.init(frame: frame)
        childParams.width = .init(weight: 1)
        childParams.height = .init(weight: 1)
        childParams.insets = .zero
        childParams.gravity = .center

        self.addLayoutGuide(contentGuide)
        lc = contentGuide.leftAnchor.constraint(equalTo: self.leftAnchor, constant: paddings.left).active()
        rc = contentGuide.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -paddings.right).active()
        tc = contentGuide.topAnchor.constraint(equalTo: self.topAnchor, constant: paddings.top).active()
        bc = contentGuide.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -paddings.bottom).active()

        self.addLayoutGuide(freeSpaceGuide)
        freeSpaceGuide.leftAnchor.constraint(equalTo: contentGuide.leftAnchor).active()
        freeSpaceGuide.topAnchor.constraint(equalTo: contentGuide.topAnchor).active()

    }

    public required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    fileprivate func gravity(index: Int) -> LayoutGravity {
        self.subviews[index].stackParams?.gravity ?? self.childParams.gravity ?? .center
    }

    fileprivate func insets(index: Int) -> EdgeInsets {
        self.subviews[index].stackParams?.insets ?? self.childParams.insets ?? .zero
    }

    fileprivate func height(index: Int) -> LayoutValue {
        self.subviews[index].stackParams?.height ?? self.childParams.height ?? .init(weight: 1)
    }

    fileprivate func width(index: Int) -> LayoutValue {
        self.subviews[index].stackParams?.width ?? self.childParams.width ?? .init(weight: 1)
    }

    open func needsRebuild() {
        self.needBuildConstraints = true
        setNeedsLayout()
    }

    fileprivate var cellCount: Int {
        self.subviews.count
    }

    private func updateSafeGuide() {
        lc?.constant = paddings.left
        rc?.constant = -paddings.right
        tc?.constant = paddings.top
        bc?.constant = -paddings.bottom
        setNeedsLayout()
    }

    fileprivate func clearItemContraints() {
        for c in itemContraints {
            c.isActive = false
        }
        itemContraints.removeAll()
    }

    fileprivate func buildConstraints() {

    }

    #if os(iOS)
    open override func layoutSubviews() {
        if needBuildConstraints && self.frame != .zero {
            needBuildConstraints = false
            self.buildConstraints()
        }
        super.layoutSubviews()
    }
    #else
    open override func layout() {
        if needBuildConstraints && self.frame != .zero {
            needBuildConstraints = false
            self.buildConstraints()
        }
        super.layout()
    }
    #endif

    open override func didAddSubview(_ subview: XView) {
        subview.translatesAutoresizingMaskIntoConstraints = false
        needsRebuild()
        super.didAddSubview(subview)
    }

    open override func willRemoveSubview(_ subview: XView) {
        needsRebuild()
        super.willRemoveSubview(subview)
    }

}

//竖直排列
open class StackView: BaseStackView {
    public var gravityY: LayoutGravity = .top {
        didSet {
            needsRebuild()
        }
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)
        childParams.gravity = .left
        freeSpaceGuide.widthAnchor.constraint(equalTo: contentGuide.widthAnchor).active()
    }

    public required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    open var fixedHeightSum: CGFloat {
        let rows: Int = self.cellCount
        var total: CGFloat = paddings.ver + space * (cellCount - 1).ge(0)
        for i in 0..<rows {
            total += height(index: i).fixed + insets(index: i).ver
        }
        return total
    }

    fileprivate override func buildConstraints() {
        clearItemContraints()

        let heightFixedSum: CGFloat = fixedHeightSum
        freeHeight?.active(false)
        freeHeight = freeSpaceGuide.heightAnchor.constraint(equalTo: self.heightAnchor, constant: -heightFixedSum).active()

        let viewList = self.subviews
        for i in viewList.indices {
            buildConstraint(index: i, view: viewList[i])
        }
    }

    private func buildConstraint(index: Int, view: XView) {
        let viewList = self.subviews

        var heightWeightSum: CGFloat = 0
        for i in 0..<cellCount {
            heightWeightSum += height(index: i).weight
        }

        let cellInsets = insets(index: index)

        view.anchors { a in

            let widthValue = self.width(index: index)
            if widthValue.weight > 0 {
                //TODO 先减， 再分
                itemContraints += a.width.equal(anchor: contentGuide.widthAnchor, constant: -cellInsets.hor, multi: widthValue.weight.le(basePercent) / basePercent)
            } else {
                itemContraints += a.width.equal(constant: widthValue.fixed)
            }
            let heightValue = self.height(index: index)
            if heightValue.weight > 0 {
                itemContraints += a.height.equal(anchor: freeSpaceGuide.heightAnchor, constant: 0, multi: heightValue.weight / heightWeightSum)
            } else {
                itemContraints += a.height.equal(constant: heightValue.fixed)
            }
            let g: LayoutGravity = gravity(index: index)
            switch g {
            case .right:
                itemContraints += a.right.equal(anchor: contentGuide.rightAnchor, constant: -cellInsets.right)
            case .center:
                itemContraints += a.centerX.equal(anchor: contentGuide.centerXAnchor, constant: 0)
            default:
                itemContraints += a.left.equal(anchor: contentGuide.leftAnchor, constant: cellInsets.left)
            }

            if index == 0 {
                if heightWeightSum == 0 {
                    switch gravityY {
                    case .center:
                        itemContraints += a.top.equal(anchor: freeSpaceGuide.centerYAnchor, constant: cellInsets.top)
                    case .bottom:
                        itemContraints += a.top.equal(anchor: freeSpaceGuide.bottomAnchor, constant: cellInsets.top)
                    default:
                        itemContraints += a.top.equal(anchor: contentGuide.topAnchor, constant: cellInsets.top)
                    }
                } else {
                    itemContraints += a.top.equal(anchor: contentGuide.topAnchor, constant: cellInsets.top)
                }
            } else {
                itemContraints += a.top.equal(anchor: viewList[index - 1].bottomAnchor, constant: space + cellInsets.top + insets(index: index - 1).bottom)
            }

        }

    }

}

//水平
open class StackHorView: BaseStackView {
    public var gravityX: LayoutGravity = .left {
        didSet {
            needsRebuild()
        }
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)
        childParams.gravity = .top
        freeSpaceGuide.heightAnchor.constraint(equalTo: contentGuide.heightAnchor).active()

    }

    public required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    open var fixedWidthSum: CGFloat {
        let count: Int = self.cellCount
        var total: CGFloat = paddings.hor + space * (count - 1).ge(0)
        for i in 0..<count {
            total += width(index: i).fixed + insets(index: i).hor
        }
        return total
    }

    fileprivate override func buildConstraints() {
        clearItemContraints()
        let widthFixedSum: CGFloat = fixedWidthSum
        freeWidth?.active(false)
        freeWidth = freeSpaceGuide.widthAnchor.constraint(equalTo: self.widthAnchor, constant: -widthFixedSum).active()

        let viewList = self.subviews
        for i in viewList.indices {
            buildConstraint(index: i, view: viewList[i])
        }
    }

    private func buildConstraint(index: Int, view: XView) {
        let viewList = self.subviews

        var widthWeightSum: CGFloat = 0
        for i in 0..<cellCount {
            widthWeightSum += width(index: i).weight
        }

        let cellInsets = insets(index: index)
        let heightValue = height(index: index)
        let widthValue = width(index: index)
        let g: LayoutGravity = gravity(index: index)

        view.anchors { a in
            if heightValue.weight > 0 {
                //TODO 先减， 再分
                itemContraints += a.height.equal(anchor: contentGuide.heightAnchor, constant: -cellInsets.ver, multi: heightValue.weight.le(basePercent) / basePercent)
            } else {
                itemContraints += a.height.equal(constant: heightValue.fixed)
            }

            if widthValue.weight > 0 {
                itemContraints += a.width.equal(anchor: freeSpaceGuide.widthAnchor, constant: 0, multi: widthValue.weight / widthWeightSum)
            } else {
                itemContraints += a.width.equal(constant: widthValue.fixed)
            }

            switch g {
            case .bottom:
                itemContraints += a.bottom.equal(anchor: contentGuide.bottomAnchor, constant: -cellInsets.bottom)
            case .center:
                itemContraints += a.centerY.equal(anchor: contentGuide.centerYAnchor, constant: 0)
            default:
                itemContraints += a.top.equal(anchor: contentGuide.topAnchor, constant: cellInsets.top)
            }
            if index == 0 {
                if widthWeightSum == 0 {
                    switch gravityX {
                    case .center:
                        itemContraints += a.left.equal(anchor: freeSpaceGuide.centerXAnchor, constant: cellInsets.left)
                    case .right:
                        itemContraints += a.left.equal(anchor: freeSpaceGuide.rightAnchor, constant: cellInsets.left)
                    default:
                        itemContraints += a.left.equal(anchor: contentGuide.leftAnchor, constant: cellInsets.left)
                    }
                } else {
                    itemContraints += a.left.equal(anchor: contentGuide.leftAnchor, constant: cellInsets.left)
                }
            } else {
                itemContraints += a.left.equal(anchor: viewList[index - 1].rightAnchor, constant: space + cellInsets.left + insets(index: index - 1).right)
            }

        }

    }

}
