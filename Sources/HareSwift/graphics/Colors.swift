//
// Created by entaoyang@163.com on 2022-11-26.
//

import Foundation
import CoreGraphics

#if os(iOS)
import UIKit
#else
import AppKit
#endif

public extension String {
    var colorValue: XColor? {
        XColor.parseColor(self)
    }
}

public extension XColor {

    static func sRGB(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat = 1.0) -> XColor {
        #if os(iOS)
        return XColor(red: red, green: green, blue: blue, alpha: alpha)
        #else
        return XColor(srgbRed: red, green: green, blue: blue, alpha: alpha)
        #endif
    }

    static func rgb(_ r: Int, _ g: Int, _ b: Int) -> XColor {
        return sRGB(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: 1.0)
    }

    static func rgba(_ r: Int, _ g: Int, _ b: Int, _ a: Int) -> XColor {
        return sRGB(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }

    static func white(_ f: CGFloat) -> XColor {
        return XColor(white: f, alpha: 1.0)
    }

    static func parseColor(_ textColor: String) -> XColor? {
        let text = textColor.trimed.trim(char: "#")
        if text.isEmpty {
            return nil
        }
        if let n = Int(text, radix: 16) {
            if text.count == 8 {
                return n.argbColor
            } else if text.count == 6 {
                return n.rgbColor
            }
        }
        return nil
    }

    var colorString: String {
        return String(format: "%08lX", self.colorInt)
    }

    var colorInt: Int {
        let rgba = rgbaValues255
        return (rgba.3 << 24) & 0xFF000000 | (rgba.0 << 16) & 0xFF0000 | (rgba.1 << 8) & 0xFF00 | rgba.2 & 0xFF
    }

    var rgbaValues255: (Int, Int, Int, Int) {
        let rgba = rgbaValues
        return (Int(rgba.0 * 255), Int(rgba.1 * 255), Int(rgba.2 * 255), Int(rgba.3 * 255))
    }
    var rgbaValues: (CGFloat, CGFloat, CGFloat, CGFloat) {
        let c = self.sRGBValue

        var a: CGFloat = 0
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        c.getRed(&r, green: &g, blue: &b, alpha: &a)
        return (r, g, b, a)
    }

    var sRGBValue: XColor {
        #if os(iOS)
        let cg = self.cgColor.converted(to: CGColorSpace(name: CGColorSpace.sRGB)!, intent: .defaultIntent, options: nil)!
        return UIColor(cgColor: cg)
        #else
        return self.usingColorSpace(.sRGB)!
        #endif
    }

    func isColorSpace(_ cs: CFString) -> Bool {
        #if os(iOS)
        self.cgColor.colorSpace?.name == cs
        #else
        self.colorSpace.cgColorSpace?.name == cs
        #endif
    }
}

public extension Int {
    var argbColor: XColor {
        let a = (self & 0xff000000) >> 24
        let r = (self & 0x00ff0000) >> 16
        let g = (self & 0x0000ff00) >> 8
        let b = (self & 0x000000ff)
        return XColor.sRGB(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
    var rgbColor: XColor {
        let r = (self & 0x00ff0000) >> 16
        let g = (self & 0x0000ff00) >> 8
        let b = (self & 0x000000ff)

        return XColor.sRGB(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: 1.0)
    }
}
