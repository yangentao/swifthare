//
// Created by entaoyang@163.com on 2022/9/28.
//

import Foundation
#if os(iOS)
import UIKit
#else
import AppKit
#endif

public extension XView {
    @discardableResult
    func anchors(_ block: (AnchorLayout) -> Void) -> Self {
        self.translatesAutoresizingMaskIntoConstraints = false
        let al = AnchorLayout(self)
        block(al)
        NSLayoutConstraint.activate(al.list)
        return self
    }
}

public extension AnchorLayout {
    @discardableResult
    func safeX(lc: CGFloat = 0, rc: CGFloat = 0) -> Self {
        return self.safeLeft(lc).safeRight(rc)
    }

    @discardableResult
    func safeY(tc: CGFloat = 0, bc: CGFloat = 0) -> Self {
        return self.safeTop(tc).safeBottom(bc)
    }

    @discardableResult
    func safeLeft(_ c: CGFloat = 0) -> Self {
        self.left.equal(anchor: parent.safeAreaLayoutGuide.leftAnchor, constant: c)
        return self
    }

    @discardableResult
    func safeRight(_ c: CGFloat = 0) -> Self {
        self.right.equal(anchor: parent.safeAreaLayoutGuide.rightAnchor, constant: c)
        return self
    }

    @discardableResult
    func safeTop(_ c: CGFloat = 0) -> Self {
        self.top.equal(anchor: parent.safeAreaLayoutGuide.topAnchor, constant: c)
        return self
    }

    @discardableResult
    func safeBottom(_ c: CGFloat = 0) -> Self {
        self.bottom.equal(anchor: parent.safeAreaLayoutGuide.bottomAnchor, constant: c)
        return self
    }
}

public extension AnchorLayout {

    func ratioW(multi: CGFloat, constant: CGFloat = 0) {
        self.width.equal(anchor: self.view.heightAnchor, constant: constant, multi: multi)
    }

    func ratioH(multi: CGFloat, constant: CGFloat = 0) {
        self.height.equal(anchor: self.view.widthAnchor, constant: constant, multi: multi)
    }

    func heightFlex(font: XFont) {
        heightFlex(minValue: "W".sizeBy(font: font).height)
    }

    func heightFlex(minValue: CGFloat) {
        self.height(minValue).priority(.belowHigh)
        self.keepContent(.vertical)
    }

    @discardableResult
    func height(font: XFont) -> NSLayoutConstraint {
        return self.height.equal(constant: "W".sizeBy(font: font).height)
    }

    @discardableResult
    func size(_ size: CGSize) -> Self {
        self.size(size.width, size.height)
        return self
    }

    @discardableResult
    func size(_ size: CGFloat) -> Self {
        self.size(size, size)
        return self
    }

    @discardableResult
    func size(_ width: CGFloat, _ height: CGFloat) -> Self {
        self.width.equal(constant: width)
        self.height.equal(constant: height)
        return self
    }

    @discardableResult
    func toRightPre(_ c: CGFloat = 0) -> Self {
        if let pre = prevSibling {
            self.left.equal(anchor: pre.rightAnchor, constant: c)
        } else {
            self.left.equal(anchor: parent.leftAnchor, constant: c)
        }
        return self
    }

    @discardableResult
    func toLeftPre(_ c: CGFloat = 0) -> Self {
        if let pre = prevSibling {
            self.right.equal(anchor: pre.leftAnchor, constant: c)
        } else {
            self.right.equal(anchor: parent.rightAnchor, constant: c)
        }
        return self
    }

    @discardableResult
    func abovePre(_ c: CGFloat = 0) -> Self {
        if let pre = prevSibling {
            above(pre, c)
        } else {
            self.bottom.equal(anchor: parent.bottomAnchor, constant: c)
        }
        return self
    }

    @discardableResult
    func above(_ view: XView, _ c: CGFloat = 0) -> Self {
        self.bottom.equal(anchor: view.topAnchor, constant: c)
        return self
    }

    @discardableResult
    func belowPre(_ c: CGFloat = 0) -> Self {
        if let pre = prevSibling {
            below(pre, c)
        } else {
            self.top.equal(anchor: parent.topAnchor, constant: c)
        }
        return self
    }

    @discardableResult
    func below(_ view: XView, _ c: CGFloat = 0) -> Self {
        self.top.equal(anchor: view.bottomAnchor, constant: c)
        return self
    }

    @discardableResult
    func center(_ x: CGFloat = 0, _ y: CGFloat = 0) -> Self {
        self.centerX.equal(anchor: parent.centerXAnchor, constant: x)
        self.centerY.equal(anchor: parent.centerYAnchor, constant: y)
        return self
    }

    @discardableResult
    func centerY(_ y: CGFloat = 0) -> Self {
        self.centerY.equal(anchor: parent.centerYAnchor, constant: y)
        return self
    }

    @discardableResult
    func centerY(_ view: XView, _ y: CGFloat = 0) -> Self {
        self.centerY.equal(anchor: view.centerYAnchor, constant: y)
        return self
    }

    @discardableResult
    func centerX(_ x: CGFloat = 0) -> Self {
        self.centerX.equal(anchor: parent.centerXAnchor, constant: x)
        return self
    }

    @discardableResult
    func centerX(_ view: XView, _ x: CGFloat = 0) -> Self {
        self.centerX.equal(anchor: view.centerXAnchor, constant: x)
        return self
    }

    @discardableResult
    func height(_ c: CGFloat) -> NSLayoutConstraint {
        return self.height.equal(constant: c)
    }

    @discardableResult
    func height(_ view: XView, _ c: CGFloat = 0, multi: CGFloat = 1) -> NSLayoutConstraint {
        return self.height.equal(anchor: view.heightAnchor, constant: c, multi: multi)
    }

    @discardableResult
    func width(_ c: CGFloat) -> NSLayoutConstraint {
        return self.width.equal(constant: c)
    }

    @discardableResult
    func width(_ view: XView, _ c: CGFloat = 0, multi: CGFloat = 1) -> NSLayoutConstraint {
        return self.width.equal(anchor: view.widthAnchor, constant: c, multi: multi)
    }

    @discardableResult
    func bottom(_ c: CGFloat) -> NSLayoutConstraint {
        return self.bottom.equal(anchor: parent.bottomAnchor, constant: c)
    }

    @discardableResult
    func bottom(_ view: XView, _ c: CGFloat = 0) -> NSLayoutConstraint {
        return self.bottom.equal(anchor: view.bottomAnchor, constant: c)
    }

    @discardableResult
    func top(_ c: CGFloat) -> NSLayoutConstraint {
        return self.top.equal(anchor: parent.topAnchor, constant: c)
    }

    @discardableResult
    func top(_ view: XView, _ c: CGFloat = 0) -> NSLayoutConstraint {
        return self.top.equal(anchor: view.topAnchor, constant: c)
    }

    @discardableResult
    func right(_ c: CGFloat) -> NSLayoutConstraint {
        return self.right.equal(anchor: parent.rightAnchor, constant: c)
    }

    @discardableResult
    func right(_ view: XView, _ c: CGFloat = 0) -> NSLayoutConstraint {
        return self.right.equal(view: view, constant: c)
    }

    @discardableResult
    func left(_ c: CGFloat) -> Self {
        self.left.equal(anchor: parent.leftAnchor, constant: c)
        return self
    }

    @discardableResult
    func left(_ view: XView, _ c: CGFloat = 0) -> Self {
        self.left.equal(view: view, constant: c)
        return self
    }

    @discardableResult
    func edgeX(inset: CGFloat) -> Self {
        self.left.equal(anchor: parent.leftAnchor, constant: inset)
        self.right.equal(anchor: parent.rightAnchor, constant: -inset)
        return self
    }

    @discardableResult
    func edgeX(view: XView, inset: CGFloat = 0) -> Self {
        self.left.equal(anchor: view.leftAnchor, constant: inset)
        self.right.equal(anchor: view.rightAnchor, constant: -inset)
        return self
    }

    @discardableResult
    func edgeY(inset: CGFloat) -> Self {
        self.top.equal(anchor: parent.topAnchor, constant: inset)
        self.bottom.equal(anchor: parent.bottomAnchor, constant: -inset)
        return self
    }

    @discardableResult
    func edgeY(view: XView, inset: CGFloat = 0) -> Self {
        self.top.equal(anchor: view.topAnchor, constant: inset)
        self.bottom.equal(anchor: view.bottomAnchor, constant: -inset)
        return self
    }

    @discardableResult
    func edges(inset: CGFloat) -> Self {
        edgeX(inset: inset)
        edgeY(inset: inset)
        return self
    }

    var preOrSuper: XView {
        self.prevSibling ?? self.parent
    }
    var firstOrSuper: XView {
        self.firstSibling ?? self.parent
    }
    var firstSibling: XView? {
        if let v = self.view.superview?.subviews[0], v !== self.view {
            return v
        }
        return nil
    }

    var prevSibling: XView? {
        if let ls = self.view.superview?.subviews, let idx = ls.firstIndex(of: self.view), idx > 0 {
            return ls[idx - 1]
        }
        return nil
    }
}

public class AnchorLayout {
    public let view: XView
    fileprivate var list: [NSLayoutConstraint] = []

    public init(_ view: XView) {
        self.view = view
        view.translatesAutoresizingMaskIntoConstraints = false
    }

    public var parent: XView {
        if let v = view.superview {
            return v
        }
        fatalError("Super view is NULL. should add this view  to super first.")
    }

    @discardableResult
    public func stretchContent(_ axis: XLayoutAxis) -> Self {
        view.setContentHuggingPriority(XLayoutPriority(rawValue: XLayoutPriority.defaultLow.rawValue - 1), for: axis)
        return self
    }

    //resist smaller than intrinsic content size

    @discardableResult
    public func keepContent(_ axis: XLayoutAxis) -> Self {
        view.setContentCompressionResistancePriority(XLayoutPriority(rawValue: XLayoutPriority.defaultHigh.rawValue + 1), for: axis)
        return self
    }

    public var left: AnchorXSide {
        AnchorXSide(self, self.view.leftAnchor)
    }
    public var right: AnchorXSide {
        AnchorXSide(self, self.view.rightAnchor)
    }
    public var centerX: AnchorXSide {
        AnchorXSide(self, self.view.centerXAnchor)
    }
    public var leading: AnchorXSide {
        AnchorXSide(self, self.view.leadingAnchor)
    }
    public var trailing: AnchorXSide {
        AnchorXSide(self, self.view.trailingAnchor)
    }

    public var top: AnchorYSide {
        AnchorYSide(self, self.view.topAnchor)
    }
    public var bottom: AnchorYSide {
        AnchorYSide(self, self.view.bottomAnchor)
    }
    public var centerY: AnchorYSide {
        AnchorYSide(self, self.view.centerYAnchor)
    }
    public var firstBaseline: AnchorYSide {
        AnchorYSide(self, self.view.firstBaselineAnchor)
    }
    public var lastBaseline: AnchorYSide {
        AnchorYSide(self, self.view.lastBaselineAnchor)
    }
    public var width: AnchorDimSide {
        AnchorDimSide(self, self.view.widthAnchor)
    }
    public var height: AnchorDimSide {
        AnchorDimSide(self, self.view.heightAnchor)
    }
}

public class AnchorXSide {
    fileprivate let anchor: NSLayoutXAxisAnchor
    fileprivate let layout: AnchorLayout

    fileprivate init(_ layout: AnchorLayout, _ anchor: NSLayoutXAxisAnchor) {
        self.layout = layout
        self.anchor = anchor
    }

    @discardableResult
    public func equal(view: XView, constant: CGFloat = 0) -> NSLayoutConstraint {
        if anchor === layout.view.leftAnchor {
            return equal(anchor: view.leftAnchor, constant: constant)
        } else if anchor === layout.view.rightAnchor {
            return equal(anchor: view.rightAnchor, constant: constant)
        } else if anchor === layout.view.centerXAnchor {
            return equal(anchor: view.centerXAnchor, constant: constant)
        } else if anchor === layout.view.leadingAnchor {
            return equal(anchor: view.leadingAnchor, constant: constant)
        } else if anchor === layout.view.trailingAnchor {
            return equal(anchor: view.trailingAnchor, constant: constant)
        } else {
            fatalError("error anchor in x side ")
        }

    }

    @discardableResult
    public func equal(anchor: NSLayoutXAxisAnchor, constant: CGFloat = 0) -> NSLayoutConstraint {
        let c = self.anchor.constraint(equalTo: anchor, constant: constant)
        layout.list.append(c)
        return c
    }
}

public class AnchorYSide {
    fileprivate let anchor: NSLayoutYAxisAnchor
    fileprivate let layout: AnchorLayout

    fileprivate init(_ layout: AnchorLayout, _ anchor: NSLayoutYAxisAnchor) {
        self.layout = layout
        self.anchor = anchor
    }

    @discardableResult
    public func equal(view: XView, constant: CGFloat = 0) -> NSLayoutConstraint {
        if anchor === layout.view.topAnchor {
            return equal(anchor: view.topAnchor, constant: constant)
        } else if anchor === layout.view.bottomAnchor {
            return equal(anchor: view.bottomAnchor, constant: constant)
        } else if anchor === layout.view.centerYAnchor {
            return equal(anchor: view.centerYAnchor, constant: constant)
        } else if anchor === layout.view.firstBaselineAnchor {
            return equal(anchor: view.firstBaselineAnchor, constant: constant)
        } else if anchor === layout.view.lastBaselineAnchor {
            return equal(anchor: view.lastBaselineAnchor, constant: constant)
        } else {
            fatalError("error anchor in y side ")
        }
    }

    @discardableResult
    public func equal(anchor: NSLayoutYAxisAnchor, constant: CGFloat = 0) -> NSLayoutConstraint {
        let c = self.anchor.constraint(equalTo: anchor, constant: constant)
        layout.list.append(c)
        return c
    }
}

public class AnchorDimSide {
    fileprivate let anchor: NSLayoutDimension
    fileprivate let layout: AnchorLayout

    fileprivate init(_ layout: AnchorLayout, _ anchor: NSLayoutDimension) {
        self.layout = layout
        self.anchor = anchor
    }

    @discardableResult
    public func equal(constant: CGFloat) -> NSLayoutConstraint {
        let c = self.anchor.constraint(equalToConstant: constant)
        layout.list.append(c)
        return c
    }

    @discardableResult
    public func equal(view: XView, constant: CGFloat = 0, multi: CGFloat = 1) -> NSLayoutConstraint {
        if anchor === layout.view.widthAnchor {
            return equal(anchor: view.widthAnchor, constant: constant, multi: multi)
        } else if anchor === layout.view.heightAnchor {
            return equal(anchor: view.heightAnchor, constant: constant, multi: multi)
        } else {
            fatalError("error side in width/height ")
        }
    }

    @discardableResult
    public func equal(anchor: NSLayoutDimension, constant: CGFloat = 0, multi: CGFloat = 1) -> NSLayoutConstraint {
        let c = self.anchor.constraint(equalTo: anchor, multiplier: multi, constant: constant)
        layout.list.append(c)
        return c
    }

    @discardableResult
    public func ge(constant: CGFloat) -> NSLayoutConstraint {
        let c = self.anchor.constraint(greaterThanOrEqualToConstant: constant)
        layout.list.append(c)
        return c
    }

    @discardableResult
    public func ge(view: XView, constant: CGFloat = 0, multi: CGFloat = 1) -> NSLayoutConstraint {
        if anchor === layout.view.widthAnchor {
            return ge(anchor: view.widthAnchor, constant: constant, multi: multi)
        } else if anchor === layout.view.heightAnchor {
            return ge(anchor: view.heightAnchor, constant: constant, multi: multi)
        } else {
            fatalError("error side in width/height ")
        }
    }

    @discardableResult
    public func ge(anchor: NSLayoutDimension, constant: CGFloat = 0, multi: CGFloat = 1) -> NSLayoutConstraint {
        let c = self.anchor.constraint(greaterThanOrEqualTo: anchor, multiplier: multi, constant: constant)
        layout.list.append(c)
        return c
    }

    @discardableResult
    public func le(constant: CGFloat) -> NSLayoutConstraint {
        let c = self.anchor.constraint(lessThanOrEqualToConstant: constant)
        layout.list.append(c)
        return c
    }

    @discardableResult
    public func le(view: XView, constant: CGFloat = 0, multi: CGFloat = 1) -> NSLayoutConstraint {
        if anchor === layout.view.widthAnchor {
            return le(anchor: view.widthAnchor, constant: constant, multi: multi)
        } else if anchor === layout.view.heightAnchor {
            return le(anchor: view.heightAnchor, constant: constant, multi: multi)
        } else {
            fatalError("error side in width/height ")
        }
    }

    @discardableResult
    public func le(anchor: NSLayoutDimension, constant: CGFloat = 0, multi: CGFloat = 1) -> NSLayoutConstraint {
        let c = self.anchor.constraint(lessThanOrEqualTo: anchor, multiplier: multi, constant: constant)
        layout.list.append(c)
        return c
    }
}



public extension XLayoutPriority {
    static let belowRequired = XLayoutPriority(XLayoutPriority.required.rawValue - 1)

    static let aboveHigh = XLayoutPriority(XLayoutPriority.defaultHigh.rawValue + 1)
    static let belowHigh = XLayoutPriority(XLayoutPriority.defaultHigh.rawValue - 1)

    static let aboveLow = XLayoutPriority(XLayoutPriority.defaultLow.rawValue + 1)
    static let belowLow = XLayoutPriority(XLayoutPriority.defaultLow.rawValue - 1)

    #if os(iOS)
    static let aboveFitting = XLayoutPriority(XLayoutPriority.fittingSizeLevel.rawValue + 1)
    static let belowFitting = XLayoutPriority(XLayoutPriority.fittingSizeLevel.rawValue - 1)
    #else
    static let aboveFitting = XLayoutPriority(XLayoutPriority.fittingSizeCompression.rawValue + 1)
    static let belowFitting = XLayoutPriority(XLayoutPriority.fittingSizeCompression.rawValue - 1)
    #endif

    static func value(_ v: Float) -> XLayoutPriority {
        XLayoutPriority(rawValue: v)
    }
}

