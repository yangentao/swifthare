//
//  position.swift
//  HareSwift
//
//  Created by Entao on 2025/1/27.
//
import Foundation

#if os(iOS)
    import UIKit
#else
    import AppKit
#endif
public enum PosValue {
    case start(_ offset: CGFloat = 0)
    case center(_ offset: CGFloat = 0)
    case end(_ offset: CGFloat = 0)
}

public enum PosDim {
    case wrap
    case fill(_ added: CGFloat = 0)
    case fixed(_ value: CGFloat)
    case percent(percent: CGFloat, added: CGFloat)
}

extension XView {

    @discardableResult
    public func position(x: PosValue, y: PosValue, w: PosDim, h: PosDim) -> Self {
        guard let parent = superview else {
            errorX("super view is nil")
        }
        self.translatesAutoresizingMaskIntoConstraints = false
        self.setContentHuggingPriority(.defaultLow, for: .horizontal)
        self.setContentHuggingPriority(.defaultLow, for: .vertical)

        var cList: [NSLayoutConstraint] = []
        switch x {
        case .start(let offset):
            cList += self.leftAnchor.constraint(equalTo: parent.leftAnchor, constant: offset)
        case .end(let offset):
            cList += self.rightAnchor.constraint(equalTo: parent.rightAnchor, constant: -offset)
        case .center(let offset):
            cList += self.centerXAnchor.constraint(equalTo: parent.centerXAnchor, constant: offset)
        }
        switch y {
        case .start(let offset):
            cList += self.topAnchor.constraint(equalTo: parent.topAnchor, constant: offset)
        case .end(let offset):
            cList += self.bottomAnchor.constraint(equalTo: parent.bottomAnchor, constant: -offset)
        case .center(let offset):
            cList += self.centerYAnchor.constraint(equalTo: parent.centerYAnchor, constant: offset)
        }
        switch w {
        case .wrap:
            self.keepContent(.horizontal)
        case .fixed(let value):
            cList += self.widthAnchor.constraint(equalToConstant: value)
        case .percent(let percent, let added):
            cList += self.widthAnchor.constraint(equalTo: parent.widthAnchor, multiplier: percent, constant: added)
        case .fill(let added):
            cList += self.widthAnchor.constraint(equalTo: parent.widthAnchor, multiplier: 1, constant: added)
        }

        switch h {
        case .wrap:
            self.keepContent(.vertical)
        case .fixed(let value):
            cList += self.heightAnchor.constraint(equalToConstant: value)
        case .percent(let percent, let added):
            cList += self.heightAnchor.constraint(equalTo: parent.heightAnchor, multiplier: percent, constant: added)
        case .fill(let added):
            cList += self.heightAnchor.constraint(equalTo: parent.heightAnchor, multiplier: 1, constant: added)
        }
        if let pd: [NSLayoutConstraint] = self.getAttr(key: "pos_data") {
            NSLayoutConstraint.deactivate(pd)
        }
        NSLayoutConstraint.activate(cList)
        self.setAttr(key: "pos_data", value: cList)
        return self
    }
}
