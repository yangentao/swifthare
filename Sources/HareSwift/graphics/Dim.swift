//
// Created by entaoyang@163.com on 2022-11-25.
//

import Foundation
import CoreGraphics

#if os(iOS)
import UIKit
#else
import AppKit
#endif

public typealias XSize = CGSize
public typealias XRect = CGRect

public extension EdgeInsets {
    var hor: CGFloat {
        left + right
    }
    var ver: CGFloat {
        top + bottom
    }

    static func xy(_ x: CGFloat, y: CGFloat) -> EdgeInsets {
        EdgeInsets(top: y, left: x, bottom: y, right: x)
    }

    static func x(left: CGFloat, right: CGFloat) -> EdgeInsets {
        EdgeInsets(top: 0, left: left, bottom: 0, right: right)
    }

    static func y(top: CGFloat, bottom: CGFloat) -> EdgeInsets {
        EdgeInsets(top: top, left: 0, bottom: bottom, right: 0)
    }

    static func all(_ value: CGFloat) -> EdgeInsets {
        EdgeInsets(top: value, left: value, bottom: value, right: value)
    }
}

public extension CGSize {
    var maxValue: Double {
        max(self.width, self.height)
    }

    func added(_ w: CGFloat, _ h: CGFloat) -> CGSize {
        return CGSize(width: self.width + w, height: self.height + h)
    }

    static func sized(_ w: CGFloat) -> CGSize {
        return sized(w, w)
    }

    static func sized(_ w: CGFloat, _ h: CGFloat) -> CGSize {
        return CGSize(width: w, height: h)
    }

    static func both(_ value: CGFloat) -> CGSize {
        CGSize(width: value, height: value)
    }

    static func all(_ value: CGFloat) -> CGSize {
        CGSize(width: value, height: value)
    }

    static func w(_ value: CGFloat) -> CGSize {
        CGSize(width: value, height: 0)
    }

    static func h(_ value: CGFloat) -> CGSize {
        CGSize(width: 0, height: value)
    }
}

public extension CGRect {
    var center: CGPoint {
        return CGPoint(x: (self.minX + self.maxX) / 2, y: (self.minY + self.maxY) / 2)
    }

    //	static var zero: CGRect {
    //		return CGRect(x: 0, y: 0, width: 0, height: 0)
    //	}
    static var one: CGRect {
        return CGRect(x: 0, y: 0, width: 1, height: 1)
    }

    static func sized(_ sz: CGSize) -> CGRect {
        return sized(sz.width, sz.height)
    }

    static func sized(_ w: CGFloat) -> CGRect {
        return sized(w, w)
    }

    static func sized(_ w: CGFloat, _ h: CGFloat) -> CGRect {
        return CGRect(x: 0, y: 0, width: w, height: h)
    }

    static func make(_ x: CGFloat, _ y: CGFloat, _ w: CGFloat, _ h: CGFloat) -> CGRect {
        return CGRect(x: x, y: y, width: w, height: h)
    }

    func center(in rect: CGRect) -> CGRect {
        let wDh: Double = self.width / self.height

        let size: CGSize
        if wDh * rect.height > rect.width {
            size = .init(width: rect.width, height: rect.width / wDh)
        } else {
            size = .init(width: rect.height * wDh, height: rect.height)
        }
        let org: CGPoint = .init(x: rect.origin.x + (rect.width - size.width) / 2, y: rect.origin.y + (rect.height - size.height) / 2)
        return CGRect(origin: org, size: size)
    }
}