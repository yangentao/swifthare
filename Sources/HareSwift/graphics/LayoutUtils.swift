//
// Created by entaoyang@163.com on 2022/11/8.
//

import Foundation

#if os(iOS)
    import UIKit
#else
    import AppKit
#endif

extension NSLayoutConstraint {
    @discardableResult
    public func active(_ b: Bool = true) -> Self {
        self.isActive = b
        return self
    }
    @discardableResult
    public func priority(_ p: XLayoutPriority) -> Self {
        self.priority = p
        return self
    }
    @discardableResult
    public func notRequired() -> NSLayoutConstraint {
        self.priority = .belowRequired
        return self
    }

    @discardableResult
    public func ident(_ value: String) -> NSLayoutConstraint {
        self.identifier = value
        return self
    }
}
extension XView {
    public func updateConstant(attr: NSLayoutConstraint.Attribute, value: CGFloat) {
        if attr == .width || attr == .height {
            if let c = findSizeConstraint(attr) {
                c.constant = value
                self.setNeedsLayout()
            }
        } else {
            if let c = findEdgeConstraint(attr) {
                c.constant = value
                superview?.setNeedsLayout()
            }
        }
    }

    //only width/height support
    private func findSizeConstraint(_ attr: NSLayoutConstraint.Attribute) -> NSLayoutConstraint? {
        self.constraints.first { n in
            n.firstItem === self && n.secondItem == nil && n.firstAttribute == attr
        }
    }

    private func findEdgeConstraint(_ attr: NSLayoutConstraint.Attribute) -> NSLayoutConstraint? {
        if let p = superview {
            return p.constraints.first { n in
                n.firstItem === self && n.secondItem === p && n.firstAttribute == attr
            }
        }
        return nil
    }

}
