//
// Created by entaoyang@163.com on 2022-11-26.
//

import Foundation

#if os(iOS)
import UIKit
#else
import AppKit
#endif

open class ColumnView: XView {
    @GreatEQ(wrappedValue: 3, minValue: 1)
    public var columnCount: Int {
        didSet {
            needsRebuild()
        }
    }

    @GreatEQ(wrappedValue: 0, minValue: 0)
    public var spaceX: CGFloat {
        didSet {
            updateSafeGuide()
            needsRebuild()
        }
    }

    @GreatEQ(wrappedValue: 0, minValue: 0)
    public var spaceY: CGFloat {
        didSet {
            updateSafeGuide()
            needsRebuild()
        }
    }

    public var paddings: EdgeInsets = .zero {
        didSet {
            updateSafeGuide()
        }
    }

    public var columnWidth: LayoutValue = .init(weight: 1) {
        didSet {
            needsRebuild()
        }
    }

    public var rowHeight: LayoutValue = .init(fixed: 48) {
        didSet {
            needsRebuild()
        }
    }
    public var gravityX: LayoutGravity = .left {
        didSet {
            needsRebuild()
        }
    }
    public var gravityY: LayoutGravity = .top {
        didSet {
            needsRebuild()
        }
    }

    private let contentGuide: XLayoutGuide = XLayoutGuide()
    private var lc: NSLayoutConstraint? = nil
    private var rc: NSLayoutConstraint? = nil
    private var tc: NSLayoutConstraint? = nil
    private var bc: NSLayoutConstraint? = nil

    private var itemContraints: [NSLayoutConstraint] = []

    private var needBuildConstraints = false

    private var colWidthMap: [Int: LayoutValue] = [:]
    private var rowHeightMap: [Int: LayoutValue] = [:]

    private let freeSpaceGuide: XLayoutGuide = XLayoutGuide()
    private var freeWidth: NSLayoutConstraint? = nil
    private var freeHeight: NSLayoutConstraint? = nil

    #if os(macOS)
    public var backgroundColor: NSColor? = nil {
        didSet {
            needsDisplay = true
        }
    }
    public var borderColor: NSColor? = nil {
        didSet {
            needsDisplay = true
        }
    }
    public var borderWidth: CGFloat = 0 {
        didSet {
            needsDisplay = true
        }
    }
    public var cornerRadius: CGFloat = 0 {
        didSet {
            needsDisplay = true
        }
    }

    public func styleBox() {
        self.backgroundColor = .windowBackgroundColor
        self.borderColor = .placeholderTextColor
        self.borderWidth = 1
        self.cornerRadius = 8
    }

    open override func draw(_ dirtyRect: NSRect) {
        let rect = self.bounds

        if let bc = backgroundColor {
            let path = NSBezierPath(roundedRect: rect, xRadius: cornerRadius, yRadius: cornerRadius)
            bc.setFill()
            path.fill()
        }
        if let sc = borderColor, borderWidth > 0 {
            let x = borderWidth / 2
            let path = NSBezierPath(roundedRect: rect.inset(by: .all(x)), xRadius: cornerRadius - x, yRadius: cornerRadius - x)
            path.lineWidth = borderWidth
            sc.set()
            path.stroke()
        }

        super.draw(dirtyRect)
    }
    #endif

    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.addLayoutGuide(contentGuide)
        lc = contentGuide.leftAnchor.constraint(equalTo: self.leftAnchor, constant: paddings.left).active()
        rc = contentGuide.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -paddings.right + spaceX).active()
        tc = contentGuide.topAnchor.constraint(equalTo: self.topAnchor, constant: paddings.top).active()
        bc = contentGuide.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -paddings.bottom).active()

        self.addLayoutGuide(freeSpaceGuide)
        let _ = freeSpaceGuide.leftAnchor.constraint(equalTo: contentGuide.leftAnchor, constant: 0).active()
        let _ = freeSpaceGuide.topAnchor.constraint(equalTo: contentGuide.topAnchor, constant: 0).active()

    }

    public required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    var rowCount: Int {
        (self.subviews.count + columnCount - 1) / columnCount
    }

    public func setWidth(column: Int, value: LayoutValue) {
        colWidthMap[column] = value
        needsRebuild()
    }

    public func setHeight(row: Int, value: LayoutValue) {
        rowHeightMap[row] = value
        needsRebuild()
    }

    private var rowHeightArray: [LayoutValue] {
        var ls: [LayoutValue] = []
        let rc = rowCount
        for i in 0..<rc {
            ls.append(rowHeightMap[i] ?? rowHeight)
        }
        return ls
    }

    private var columnWidthArray: [LayoutValue] {
        var ls: [LayoutValue] = []
        for i in 0..<columnCount {
            ls.append(colWidthMap[i] ?? columnWidth)
        }
        return ls
    }

    public func needsRebuild() {
        self.needBuildConstraints = true
        setNeedsLayout()
    }

    private func updateSafeGuide() {
        lc?.constant = paddings.left
        rc?.constant = -paddings.right + spaceX
        tc?.constant = paddings.top
        bc?.constant = -paddings.bottom
        setNeedsLayout()
    }

    public var fixedHeightSum: CGFloat {
        let rowHeightList: [LayoutValue] = rowHeightArray
        let fixedSum = rowHeightList.sumBy {
            $0.fixed
        }
        let rows = self.rowCount
        return paddings.top + paddings.bottom + spaceY * (rows - 1).ge(0) + fixedSum
    }
    public var fixedWidthSum: CGFloat {
        let columnList: [LayoutValue] = columnWidthArray
        let fixedSum = columnList.sumBy {
            $0.fixed
        }
        return paddings.left + paddings.right + spaceX * (columnCount - 1).ge(0) + fixedSum
    }

    private func buildConstraints() {
        for c in itemContraints {
            c.isActive = false
        }
        itemContraints.removeAll()

        let columnWidthList: [LayoutValue] = columnWidthArray
        var widthFixedSum: CGFloat = paddings.left + paddings.right + spaceX * (columnCount - 1).ge(0)
        widthFixedSum += columnWidthList.sumBy {
            $0.fixed
        }
        freeWidth?.active(false)
        freeWidth = freeSpaceGuide.widthAnchor.constraint(equalTo: self.widthAnchor, constant: -widthFixedSum).active()

        let rowHeightList: [LayoutValue] = rowHeightArray
        var heightFixedSum: CGFloat = paddings.top + paddings.bottom + spaceY * (rowCount - 1).ge(0)
        heightFixedSum += rowHeightList.sumBy {
            $0.fixed
        }
        freeHeight?.active(false)
        freeHeight = freeSpaceGuide.heightAnchor.constraint(equalTo: self.heightAnchor, constant: -heightFixedSum).active()

        let viewList = self.subviews
        for i in viewList.indices {
            buildConstraint(index: i, view: viewList[i], columnLayoutValues: columnWidthList, rowLayoutValues: rowHeightList)
        }
    }

    private func buildConstraint(index: Int, view: XView, columnLayoutValues: [LayoutValue], rowLayoutValues: [LayoutValue]) {
        let viewList = self.subviews
        let row: Int = index / columnCount
        let col: Int = index % columnCount

        let colWeightSum: CGFloat = columnLayoutValues.sumBy {
            $0.weight
        }
        let rowWeightSum: CGFloat = rowLayoutValues.sumBy {
            $0.weight
        }

        view.anchors { a in

            let columnValue = columnLayoutValues[col]
            if columnValue.weight > 0 {
                itemContraints += a.width.equal(anchor: freeSpaceGuide.widthAnchor, multi: columnValue.weight / colWeightSum)
            } else {
                itemContraints += a.width.equal(constant: columnValue.fixed)
            }
            let rowValue = rowLayoutValues[row]
            if rowValue.weight > 0 {
                itemContraints += a.height.equal(anchor: freeSpaceGuide.heightAnchor, multi: rowValue.weight / rowWeightSum)
            } else {
                itemContraints += a.height.equal(constant: rowValue.fixed)
            }
            if col == 0 {
                if colWeightSum == 0 {
                    switch gravityX {
                    case .center:
                        itemContraints += a.left.equal(anchor: freeSpaceGuide.centerXAnchor, constant: 0)
                    case .right:
                        itemContraints += a.left.equal(anchor: freeSpaceGuide.rightAnchor, constant: 0)
                    default:
                        itemContraints += a.left.equal(anchor: freeSpaceGuide.leftAnchor, constant: 0)
                    }
                } else {
                    itemContraints += a.left.equal(anchor: contentGuide.leftAnchor, constant: 0)
                }
            } else {
                itemContraints += a.left.equal(anchor: viewList[row * columnCount + col - 1].rightAnchor, constant: spaceX)
            }
            if row == 0 {
                if rowWeightSum == 0 {
                    switch gravityY {
                    case .center:
                        itemContraints += a.top.equal(anchor: freeSpaceGuide.centerYAnchor, constant: 0)
                    case .bottom:
                        itemContraints += a.top.equal(anchor: freeSpaceGuide.bottomAnchor, constant: 0)
                    default:
                        itemContraints += a.top.equal(anchor: contentGuide.topAnchor, constant: 0)
                    }
                } else {
                    itemContraints += a.top.equal(anchor: contentGuide.topAnchor, constant: 0)
                }
            } else {
                itemContraints += a.top.equal(anchor: viewList[(row - 1) * columnCount + col].bottomAnchor, constant: spaceY)
            }

        }

    }

    #if os(iOS)
    open override func layoutSubviews() {
        if needBuildConstraints && self.frame != .zero {
            needBuildConstraints = false
            self.buildConstraints()
        }
        super.layoutSubviews()
    }
    #else
    open override func layout() {
        if needBuildConstraints && self.frame != .zero {
            needBuildConstraints = false
            self.buildConstraints()
        }
        super.layout()
    }
    #endif

    open override func didAddSubview(_ subview: XView) {
        subview.translatesAutoresizingMaskIntoConstraints = false
        needsRebuild()
        super.didAddSubview(subview)
    }

    open override func willRemoveSubview(_ subview: XView) {
        needsRebuild()
        super.willRemoveSubview(subview)
    }
}
