//
// Created by entaoyang@163.com on 2022-11-26.
//

import Foundation

#if os(iOS)
    import UIKit
#else
    import AppKit
#endif

#if os(iOS)
    public typealias EdgeInsets = UIEdgeInsets
    public typealias XLayoutPriority = UILayoutPriority
    public typealias XLayoutAxis = NSLayoutConstraint.Axis
    public typealias XLayoutGuide = UILayoutGuide
    public typealias XView = UIView
    public typealias XFont = UIFont
    public typealias XColor = UIColor
#else
    public typealias EdgeInsets = NSEdgeInsets
    public typealias XLayoutPriority = NSLayoutConstraint.Priority
    public typealias XLayoutAxis = NSLayoutConstraint.Orientation
    public typealias XLayoutGuide = NSLayoutGuide
    public typealias XView = NSView
    public typealias XFont = NSFont
    public typealias XColor = NSColor
#endif

extension XView {
    @discardableResult
    public func stretchContent(_ axis: XLayoutAxis) -> Self {
        self.setContentHuggingPriority(.belowLow, for: axis)
        return self
    }
    @discardableResult
    public func keepContent(_ axis: XLayoutAxis) -> Self {
        self.setContentCompressionResistancePriority(.aboveHigh, for: axis)
        return self
    }
}
extension XView {
    @discardableResult
    public func addView<T: XView>(_ view: T) -> T {
        return self.add(view: view)
    }
    @discardableResult
    public func add<T: XView>(view: T) -> T {
        #if os(iOS)
            if let sv = self as? UIStackView {
                sv.addArrangedSubview(view)
            } else {
                self.addSubview(view)
            }
        #else
            if let sv = self as? NSStackView {
                sv.addArrangedSubview(view)
            } else {
                self.addSubview(view)
            }
        #endif
        return view
    }

    public func add<T: XView>(type: T.Type) -> T {
        add(view: T.init(frame: .zero))
    }

    public func cleanChildren() {
        let arr = self.subviews
        for v in arr {
            v.removeFromSuperview()
        }
    }

    #if os(iOS)
        public var selfViewController: UIViewController? {
            if let r = self.next as? UIViewController {
                return r
            }
            if let rr = self.next as? UIView {
                return rr.selfViewController
            }
            return nil
        }
    #else
        public var selfViewController: NSViewController? {
            if let r = self.nextResponder as? NSViewController {
                return r
            }
            if let rr = self.nextResponder as? XView {
                return rr.selfViewController
            }
            return nil

        }
    #endif

    public func eachChild(_ block: (XView) -> Void) {
        for v in self.subviews {
            block(v)
            v.eachChild(block)
        }
    }

    public var firstSibling: XView? {
        if let ls = self.superview?.subviews, ls.count >= 2 {
            if ls[0] !== self {
                return ls[0]
            }
            return ls[1]
        }
        return nil
    }
    public var prevSibling: XView? {
        if let ls = self.superview?.subviews, let idx = ls.firstIndex(of: self), idx > 0 {
            return ls[idx - 1]
        }
        return nil
    }

    public var nextSibling: XView? {
        if let ls = self.superview?.subviews, let idx = ls.firstIndex(of: self), idx < ls.count - 1 {
            return ls[idx + 1]
        }
        return nil
    }

    public func firstView(_ block: (XView) -> Bool) -> XView? {
        for v in self.subviews {
            if block(v) {
                return v
            }
            if let vv = v.firstView(block) {
                return vv
            }
        }
        return nil
    }

    public func firstView<T: XView>(type: T.Type) -> T? {
        firstView {
            $0 is T
        } as? T
    }

    public func firstView<T: XView>() -> T? {
        firstView {
            $0 is T
        } as? T
    }

    public func siblings<T>(_: T.Type) -> [T] {
        self.superview!.subviews.filter {
            $0 != self
        }
        .compactMap {
            $0 as? T
        }
    }

}
