//
// Created by entaoyang@163.com on 2022/9/13.
//

import Foundation

public class Rand {
    let from: Int
    let delta: Int

    public init(from: Int, to: Int) {
        self.from = from
        ensure(cond: from < to, msg: "invalid range [\(from),\(to))")
        delta = to - from
    }

    public var next: Int {
        from + Int(arc4random()) % delta
    }
}

public func random(from: Int, to: Int) -> Int {
    ensure(cond: from < to, msg: "invalid range [\(from),\(to))")
    return from + Int(arc4random()) % (to - from)
}

public class TickTime {
    let start: TimeInterval

    fileprivate init(start: TimeInterval = Date().timeIntervalSince1970) {
        self.start = start
    }

    public func end(msg: String = "") {
        let delta = Date().timeIntervalSince1970 - start
        logd("Tick: ", msg, ": ", delta.keepDot(6), "S")
    }
}

public func tickStart() -> TickTime {
    TickTime(start: Date().timeIntervalSince1970)
}

public func tick(_ msg: String = "seconds", _ block: VoidBlock) {
    let t = tickStart()
    block()
    t.end(msg: msg)
}

public func tickR<R>(_ msg: String = "tick", _ block: () -> R) -> R {
    let t = tickStart()
    let r = block()
    t.end(msg: msg)
    return r
}

public func errorX(_ msg: String, file: StaticString = #file, line: UInt = #line) -> Never {
    fatalError(msg, file: file, line: line)
}
public func fatal(msg: String, file: StaticString = #file, line: UInt = #line) -> Never {
    fatalError(msg, file: file, line: line)
}

public func fatal(on cond: Bool, msg: String, file: StaticString = #file, line: UInt = #line) {
    if cond {
        fatalError(msg, file: file, line: line)
    }
}

public func ensure(cond: Bool, msg: String, file: StaticString = #file, line: UInt = #line) {
    if !cond {
        fatalError(msg, file: file, line: line)
    }
}

public extension time_t {

    var toDate: Date {
        Date(timeIntervalSince1970: TimeInterval(self))
    }
}

public func max<T>(_ x: T, _ y: T, _ z: T) -> T where T: Comparable {
    max(max(x, y), z)
}

public func max<T>(_ a: T, _ b: T, _ c: T, _ d: T) -> T where T: Comparable {
    max(max(a, b), max(c, d))
}

