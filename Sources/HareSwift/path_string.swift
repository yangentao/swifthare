import Foundation

extension String {

    public var fileURL: URL {
        URL(fileURLWithPath: self)
    }
    public func mkdirs() throws {
        if !self.existDirectory {
            try FileManager.default.createDirectory(atPath: self, withIntermediateDirectories: true)
        }
    }
    public func list(deep: Bool = false) throws -> [String] {
        if deep {
            return try FileManager.default.subpathsOfDirectory(atPath: self)
        } else {
            return try FileManager.default.contentsOfDirectory(atPath: self)
        }
    }

    public var fileSize: Int {
        if let map = try? fileAttrs(), let n: NSNumber = map[FileAttributeKey.size] as? NSNumber {
            return Int(truncating: n)
        }
        return 0
    }

    public func fileAttrs() throws -> [FileAttributeKey: Any] {
        return try FileManager.default.attributesOfItem(atPath: self)
    }

    @discardableResult
    public func fileCreate(data: Data? = nil) throws -> Bool {
        try self.parentPath.mkdirs()
        return FileManager.default.createFile(atPath: self, contents: data)
    }
    public func dirCreate() throws {
        try fileManager.createDirectory(atPath: self, withIntermediateDirectories: true)
    }

    public var existAny: Bool {
        self.exists.exists
    }
    public var existFile: Bool {
        self.exists.isFile
    }
    public var existDirectory: Bool {
        self.exists.isDir
    }
    @available(*, deprecated, renamed: "exists", message: "rename to exists")
    public var fileExists: ExistsResult {
        return exists
    }
    @available(*, deprecated, renamed: "exists", message: "rename to exists")
    public var existResult: ExistsResult {
        return exists
    }
    public var exists: ExistsResult {
        var b: ObjCBool = false
        let e = FileManager.default.fileExists(atPath: self, isDirectory: &b)
        return ExistsResult(exists: e, isDirectory: b.boolValue)
    }
    public var fileReadable: Bool {
        FileManager.default.isReadableFile(atPath: self)
    }
    public var fileWritable: Bool {
        FileManager.default.isWritableFile(atPath: self)
    }

    public var fileContent: Data? {
        return FileManager.default.contents(atPath: self)
    }
    public var fileContentText: String? {
        return FileManager.default.contents(atPath: self)?.string()
    }

    public func fileRemove() throws {
        try FileManager.default.removeItem(atPath: self)
    }
    public func copyItem(to path: String) throws {
        try FileManager.default.copyItem(atPath: self, toPath: path)
    }

}

extension String {
    public func startWith(path: String) -> Bool {
        if self == path {
            return true
        }
        if path.endWith("/") {
            return self.startWith(path)
        }
        return self.startWith(path + "/")
    }
    public func equalPath(_ p: String) -> Bool {
        self.trim(char: "/") == p.trim(char: "/")
    }
    public func subpath(parent: String) -> String? {
        let p: String
        if parent.hasSuffix("/") {
            p = parent
        } else {
            p = parent + "/"
        }
        if self.hasPrefix(p) {
            return self.substring(from: p.count)
        }
        return nil
    }

    public func join(path: String) -> String {
        joinPath(self, path)
    }

    public var filename: String {
        return self.trimEnd(char: "/").substringAfterLast(char: "/", onMiss: nil)
    }

    public var extName: String {
        return filename.substringAfterLast(char: ".", onMiss: "")
    }
    public var baseName: String {
        return filename.substringBeforeLast(char: ".", onMiss: nil)
    }
    @available(*, deprecated, renamed: "baseName", message: "use baseName instead")
    public var shortName: String {
        return baseName
    }

    public func appendPath(_ file: String) -> String {
        return self.join(path: file)
    }

    public var lastPath: String {
        return self.trimEnd(char: "/").substringAfterLast(char: "/")
    }

    //  /home/wang/  -> /home -> /
    // "/" -> ""
    public var parentPath: String {
        if self == "/" {
            return ""
        }
        if self.last == "/" {
            return self.trimEnd(char: "/").parentPath
        }
        return self.substringBeforeLast(char: "/", onMiss: "")
    }
}

//path
public func joinPath(_ a: String, _ b: String) -> String {
    if a.isEmpty { return b }
    if b.isEmpty { return a }
    if a.endWith("/") {
        if b.startWith("/") {
            return a + b.substring(from: 1)
        } else {
            return a + b
        }
    } else {
        if b.startWith("/") {
            return a + b
        } else {
            return a + "/" + b
        }
    }
}
