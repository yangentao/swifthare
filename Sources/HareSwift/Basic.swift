//
// Created by entaoyang@163.com on 2022/9/13.
//

import Foundation

public let NSEC: Int = 1_000_000_000

public typealias VoidBlock = () -> Void
public typealias StringBlock = (String) -> Void
public typealias BoolBlock = (Bool) -> Void
public typealias IntBlock = (Int) -> Void

public typealias BlockBool = () -> Bool
public typealias BlockString = () -> String
public typealias BlockInt = () -> String

public typealias ProgressCallback = (Int, Int) -> Void

public class StdErrorTextOutputStream: TextOutputStream {
    public func write(_ string: String) {
        fputs(string, stderr)
    }
}

public var StdError = StdErrorTextOutputStream()

public func itemsToString(_ items: [Any?]) -> String {
    var buf = ""
    for a in items {
        if let b = a ?? nil {
            print(b, terminator: " ", to: &buf)
        } else {
            print("nil", terminator: " ", to: &buf)
        }
    }
    return buf
}
public var isDebug: Bool {
    #if DEBUG
        return true
    #else
        return false
    #endif
}

public func sleep(mill: UInt64) async {
    try? await Task.sleep(nanoseconds: mill * 1_000_000)
}

extension NSObjectProtocol {

    @discardableResult
    public func apply(_ block: (Self) -> Void) -> Self {
        block(self)
        return self
    }
    @available(*, deprecated, renamed: "trans()")
    public func also<R: Any>(_ block: (Self) -> R) -> R {
        block(self)
    }
    public func trans<R: Any>(_ block: (Self) -> R) -> R {
        block(self)
    }
}

extension Encodable {

    @discardableResult
    public func apply(_ block: (Self) -> Void) -> Self {
        block(self)
        return self
    }
    @available(*, deprecated, renamed: "trans()")
    public func also<R: Any>(_ block: (Self) -> R) -> R {
        block(self)
    }
    public func trans<R: Any>(_ block: (Self) -> R) -> R {
        block(self)
    }
}

public func println(_ items: Any?..., sep: String = " ") {
    for a in items {
        if let v = a ?? nil {
            print(v, separator: sep, terminator: " ")
        } else {
            print("nil", separator: sep, terminator: " ")
        }
    }
    print("")
}

public class TextFileStream: TextOutputStream {
    public let path: String
    public let fh: FileHandle?

    public init(path: String) {
        self.path = path
        fh = try? FileHandle.openForWrite(path: path, append: true)
    }

    public func write(_ string: String) {
        if let d = string.data(using: .utf8) {
            fh?.write(d)

        }
    }

    public func close() {
        try? fh?.close()
    }
}

public class AppExitClean {
    private var mapCallback: [String: VoidBlock] = [:]

    private init() {
    }

    public func add(_ key: String, _ callback: @escaping VoidBlock) {
        mapCallback[key] = callback
    }

    public func remove(_ key: String) {
        mapCallback.removeValue(forKey: key)
    }

    public func clean() {
        let keySet = mapCallback.keySet
        for key in keySet {
            mapCallback[key]?()
        }
        mapCallback.removeAll()
    }

    public static let shared: AppExitClean = AppExitClean()
}

//init只能是类的实例,

public class WeakRef<T: AnyObject>: Equatable {

    public weak var value: T?

    public init(_ value: T) {
        self.value = value
    }

    public var isNull: Bool {
        return value == nil
    }
    public var notNull: Bool {
        return value != nil
    }

    public static func == (lhs: WeakRef, rhs: WeakRef) -> Bool {
        return lhs === rhs || lhs.value === rhs.value
    }

}

private var _idSeed: Int = 0

public func IDGen() -> Int {
    _idSeed += 1
    return _idSeed
}

public class JSON {

    public static func decode<T: Decodable>(_ json: String?) -> T? {
        return decode(T.self, json: json)
    }

    public static func decode<T>(_ type: T.Type, json: String?) -> T? where T: Decodable {
        return decode(type, json: json) {
            $0.dateDecodingStrategy = .millisecondsSince1970
        }
    }

    public static func decode<T>(_ type: T.Type, json: String?, _ block: (JSONDecoder) -> Void) -> T? where T: Decodable {
        if let s = json, s.notEmpty {
            let d = JSONDecoder()
            block(d)
            return try? d.decode(type, from: s.dataUtf8)
        }
        return nil
    }

    public static func encode<T>(_ value: T, pretty: Bool = false) -> String? where T: Encodable {
        return encode(value) {
            $0.dateEncodingStrategy = .millisecondsSince1970
            if pretty {
                $0.outputFormatting = .prettyPrinted
            }
        }
    }

    public static func encode<T>(_ value: T, _ block: (JSONEncoder) -> Void) -> String? where T: Encodable {
        let e = JSONEncoder()
        block(e)
        return (try? e.encode(value))?.string()
    }
}

extension Data {

    public var bytes: [UInt8] {
        [UInt8](self)
    }

    public mutating func append(_ s: String, encode: String.Encoding = .utf8) {
        if let d = s.data(using: encode) {
            self.append(d)
        }
    }

    public mutating func appendNewLine() {
        self.append("\r\n".dataUtf8)
    }

    public mutating func appendNewLine(text: String) {
        self.append((text + "\r\n").dataUtf8)
    }

    public func string(_ enc: String.Encoding = .utf8) -> String? {
        String(data: self, encoding: enc)
    }

    public var base64: String {
        self.base64EncodedString(options: Base64EncodingOptions.endLineWithCarriageReturn)

    }
}

public class HareError: Error, ToString {
    public let code: Int
    public let message: String
    public let file: String
    public let line: Int
    public init(_ message: String, code: Int = -1, file: String = #file, line: Int = #line) {
        self.code = code
        self.message = message
        self.file = file
        self.line = line
    }
    public func toString() -> String {
        return "HareError(code: \(code), message:\(message), file: \(file), \(line))"
    }

    public var localizedDescription: String {
        return "\(code):\(message)"
    }

}
open class MsgError: Error {
    public let messge: String

    public init(msg: String) {
        self.messge = msg
    }

}

open class CodeError: MsgError {
    public let code: Int

    public init(code: Int, msg: String) {
        self.code = code
        super.init(msg: msg)
    }
}

extension Bundle {
    public func textContent(filename: String, type: String) -> String? {
        self.path(forResource: filename, ofType: type)?.fileContentText
    }
}
