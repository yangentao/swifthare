//
// Created by entaoyang@163.com on 2022-11-29.
//

import Foundation

public class FileArray<V: Codable>: NSObject {
    public let fileValue: FileValue<[V]>

    public init(path: String, miss: () -> [V] = {
        []
    }) {
        fileValue = .init(path: path, miss: miss)
        super.init()
    }

    public convenience init(file: String, cache: Bool = false, miss: () -> [V] = {
        []
    }) {
        self.init(path: cache ? CachePath(name: file) : DocPath(name: file), miss: miss)
    }

    public func trigger() {
        fileValue.trigger()
    }

    public func save() {
        fileValue.save()
    }

    public var values: Array<V> {
        get {
            fileValue.value
        }
        set {
            fileValue.value = newValue
            fileValue.trigger()
        }
    }
    public var count: Int {
        fileValue.value.count
    }
    public var isEmpty: Bool {
        count == 0
    }

    public var indices: Range<Int> {
        fileValue.value.indices
    }

    public func contains(value: V) -> Bool where V: Equatable {
        fileValue.value.contains(value)
    }

    public func remove(index: Int) -> V {
        let v = fileValue.value.remove(at: index)
        fileValue.trigger()
        return v
    }

    public func remove(value: V) where V: Equatable {
        fileValue.value.removeElement(value)
        fileValue.trigger()
    }

    public func append(all values: any Sequence<V>) {
        fileValue.value.append(contentsOf: values)
        fileValue.trigger()
    }

    public func append(value: V) {
        fileValue.value.append(value)
        fileValue.trigger()
    }

    public func add(value: V, index: Int) {
        if fileValue.value.indices.contains(index) {
            fileValue.value.insert(value, at: index)
        } else {
            fileValue.value.append(value)
        }
        fileValue.trigger()
    }

    public func set(value: V, index: Int) -> Bool {
        if fileValue.value.indices.contains(index) {
            fileValue.value[index] = value
            fileValue.trigger()
            return true
        }
        return false
    }

    public subscript(_ index: Int) -> V {
        get {
            fileValue.value[index]
        }
        set {
            fileValue.value[index] = newValue
            fileValue.trigger()
        }
    }

    public func load<K: Comparable>(key: K, _ keyBlock: (V) -> K) -> V? {
        for i in self.indices {
            let v = self[i]
            if keyBlock(v) == key {
                return v
            }
        }
        return nil
    }

    public func save<K: Comparable>(value: V, by keyBlock: (V) -> K) {
        for i in self.indices {
            if keyBlock(self[i]) == keyBlock(value) {
                self[i] = value
                return
            }
        }
        self.append(value: value)
    }

    public func clear() {
        fileValue.value.removeAll(keepingCapacity: true)
        fileValue.save()
    }
}

public class FileTable<K: Codable & Hashable, V: Codable>: NSObject {
    public let fileValue: FileValue<[K: V]>

    public init(path: String, miss: () -> [K: V] = {
        [:]
    }) {
        fileValue = .init(path: path, miss: miss)
        super.init()
    }

    public convenience init(file: String, cache: Bool = false, miss: () -> [K: V] = {
        [:]
    }) {
        self.init(path: cache ? CachePath(name: file) : DocPath(name: file), miss: miss)
    }

    public func trigger() {
        fileValue.trigger()
    }

    public func save() {
        fileValue.save()
    }

    public var map: [K: V] {
        get {
            fileValue.value
        }
        set {
            fileValue.value = newValue
        }
    }

    public var keys: Set<K> {
        fileValue.value.keySet
    }
    public var values: [V] {
        fileValue.value.valueArray
    }
    public var count: Int {
        fileValue.value.count
    }

    public func remove(key: K) -> V? {
        let v = fileValue.value.removeValue(forKey: key)
        fileValue.trigger()
        return v
    }

    public func contains(key: K) -> Bool {
        fileValue.value[key] != nil
    }

    public func getOrPut(key: K, _ block: (K) -> V) -> V {
        if let v = fileValue.value[key] {
            return v
        }
        let vv = block(key)
        fileValue.value[key] = vv
        fileValue.trigger()
        return vv
    }

    public subscript(_ key: K) -> V? {
        get {
            fileValue.value[key]
        }
        set {
            if let v = newValue {
                fileValue.value[key] = v
            } else {
                fileValue.value.removeValue(forKey: key)
            }
            fileValue.trigger()
        }
    }

    public func clear() {
        fileValue.value.removeAll(keepingCapacity: true)
        fileValue.save()
    }
}

public class FileMap<V: Codable>: NSObject {
    public let fileValue: FileValue<[String: V]>

    public init(path: String, miss: () -> [String: V] = {
        [:]
    }) {
        fileValue = .init(path: path, miss: miss)
        super.init()
    }

    public convenience init(file: String, cache: Bool = false, miss: () -> [String: V] = {
        [:]
    }) {
        self.init(path: cache ? CachePath(name: file) : DocPath(name: file), miss: miss)
    }

    public func trigger() {
        fileValue.trigger()
    }

    public func save() {
        fileValue.save()
    }

    public var map: [String: V] {
        get {
            fileValue.value
        }
        set {
            fileValue.value = newValue
        }
    }
    public var keys: Set<String> {
        fileValue.value.keySet
    }
    public var values: Array<V> {
        fileValue.value.valueArray
    }
    public var count: Int {
        fileValue.value.count
    }
    public var isEmpty: Bool {
        count == 0
    }

    @discardableResult
    public func remove(key: String) -> V? {
        let v = fileValue.value.removeValue(forKey: key)
        fileValue.trigger()
        return v
    }

    public func contains(key: String) -> Bool {
        fileValue.value[key] != nil
    }

    public func getOrPut(key: String, _ block: (String) -> V) -> V {
        if let v = fileValue.value[key] {
            return v
        }
        let vv = block(key)
        fileValue.value[key] = vv
        fileValue.trigger()
        return vv
    }

    public subscript(_ key: String) -> V? {
        get {
            fileValue.value[key]
        }
        set {
            if let v = newValue {
                fileValue.value[key] = v
            } else {
                fileValue.value.removeValue(forKey: key)
            }
            fileValue.trigger()
        }
    }

    public func clear() {
        fileValue.value.removeAll(keepingCapacity: true)
        fileValue.save()
    }
}

@propertyWrapper
public struct PropFileValue<T: Codable> {
    private let fileValue: FileValue<T>

    public init(file: String, cache: Bool = false, _ block: () -> T) {
        self.fileValue = .init(path: cache ? CachePath(name: file) : DocPath(name: file), miss: block)
    }

    public func trigger() {
        fileValue.trigger()
    }


    public var projectedValue: Self {
        return self
    }
    public var wrappedValue: T {
        get {
            fileValue.value
        }
        set {
            fileValue.value = newValue
        }
    }
}

public class FileValue<T: Codable> {
    private let path: String
    public var value: T {
        didSet {
            saver.trigger()
        }
    }
    private lazy var saver: DelayTrigger = .init(delay: 0.5) { [weak self] in
        self?.save()
    }

    public init(path: String, miss: () -> T) {
        self.path = path
        self.value = loadFrom(path: path) ?? miss()
    }

    public func trigger() {
        saver.trigger()
    }

    public func save() {
        saveTo(path: path, data: value)
    }
}

public func saveTo<T: Codable>(path: String, data: T) {
    let s = Json.encode(value: data).jsonText
    try? s.write(toFile: path, atomically: true, encoding: .utf8)
}

public func loadFrom<T: Codable>(path: String) -> T? {
    guard let s = try? String(contentsOfFile: path) else {
        return nil
    }
    return try? Json.decode(text: s)
}
