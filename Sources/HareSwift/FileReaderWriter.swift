import Foundation

public let SIZE_8K: Int = 8192

public class ByteBuffer {
    public let capacity: Int
    public let buffer: UnsafeMutablePointer<UInt8>

    public init(capacity: Int = SIZE_8K) {
        self.capacity = capacity
        buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: capacity)
    }
    deinit {
        buffer.deallocate()
    }
    public var bufferCChar: UnsafeMutablePointer<CChar> {
        buffer.withMemoryRebound(to: CChar.self, capacity: capacity, { $0 })
    }
    public func toArray(size: Int) -> [UInt8] {
        var ls: [UInt8] = .init(repeating: 0, count: size)
        for i in 0..<size {
            ls[i] = self.buffer[i]
        }
        return ls
    }
}
public class CCharBuffer {
    public let capacity: Int
    public let buffer: UnsafeMutablePointer<Int8>

    public init(capacity: Int = SIZE_8K) {
        self.capacity = capacity
        buffer = UnsafeMutablePointer<Int8>.allocate(capacity: capacity + 2)
    }
    deinit {
        buffer.deallocate()
    }
    public var bufferBytes: UnsafeMutablePointer<UInt8> {
        buffer.withMemoryRebound(to: UInt8.self, capacity: capacity, { $0 })
    }
    public func toArray(size: Int) -> [CChar] {
        var ls: [CChar] = .init(repeating: 0, count: size)
        for i in 0..<size {
            ls[i] = self.buffer[i]
        }
        return ls
    }
}

public class FileWriter {
    public let fd: Int32
    public init(_ path: String, flag: Int32 = O_WRONLY | O_CREAT | O_TRUNC, mode: mode_t = 0o644) throws {
        fd = Darwin.open(path, flag, mode)
        if fd == -1 {
            throw HareError(String(cString: strerror(errno)), code: errno.intValue)
        }
    }
    public func write(buf: UnsafeRawPointer, size: Int) -> Int {
        return Darwin.write(fd, buf, size)
    }
    public func write(text: String, encoding: String.Encoding = .utf8) -> Int {
        let s = text.cstring(encoding)
        return write(buf: s, size: s.count)
    }
    public func write(data: Data) -> Int {
        return write(buf: data.bytes, size: data.count)
    }
    deinit {
        if fd != -1 {
            Darwin.close(fd)
        }
    }
}
public class FileReader {
    public let fd: Int32
    public init(_ path: String, flag: Int32 = O_RDONLY) throws {
        fd = Darwin.open(path, flag, 0)
        if fd == -1 {
            throw HareError(String(cString: strerror(errno)), code: errno.intValue)
        }
    }
    deinit {
        if fd != -1 {
            Darwin.close(fd)
        }
    }
    public var statValue: Darwin.stat {
        var st: Darwin.stat = Darwin.stat()
        Darwin.fstat(self.fd, &st)
        return st
    }
    public func read(buf: UnsafeMutableRawPointer, size: Int) -> Int {
        return Darwin.read(fd, buf, size)
    }
    public func read(bytes: ByteBuffer) -> Int {
        return self.read(buf: bytes.buffer, size: bytes.capacity)
    }
    public func read(chars: CCharBuffer) -> Int {
        return self.read(buf: chars.buffer, size: chars.capacity)
    }
    public func readData(size: Int) -> Data? {
        if size <= 0 { return nil }
        let buf = ByteBuffer(capacity: size)
        let n = read(bytes: buf)
        if n < 0 { return nil }
        return Data(bytes: buf.buffer, count: n)
    }
    public func readAll() -> Data? {
        let buf = ByteBuffer(capacity: SIZE_8K)
        var data: Data = Data(capacity: SIZE_8K)
        var n: Int = read(bytes: buf)
        while n > 0 {
            data.append(buf.buffer, count: n)
            n = read(bytes: buf)
        }
        if n < 0 {
            return nil
        }
        return data
    }
    public func readAllText(encoding: String.Encoding = .utf8) -> String? {
        if let d = readAll() {
            return String(data: d, encoding: encoding)
        }
        return nil
    }

    public func sendFile(sock: Int32) -> CodeData<Int> {

        var size: Int64 = 0
        let ret = Darwin.sendfile(self.fd, sock, 0, &size, nil, 0)

        if ret == 0 {
            return CodeData<Int>(success: size.intValue)
        } else {
            return CodeData<Int>.fromErrno()
        }

    }

}
