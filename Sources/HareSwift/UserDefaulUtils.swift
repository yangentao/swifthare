//
// Created by entaoyang@163.com on 2022-11-17.
// Copyright (c) 2022 entao.dev. All rights reserved.
//

import Foundation

public extension UserDefaults {
    func get<T: Any & Codable>(key: String) -> T? {
        self.value(forKey: key) as? T
    }

    func put<T: Any & Codable>(key: String, value: T) {
        self.set(value, forKey: key)
    }
}

@propertyWrapper
public struct UserDefaultX<T: Any> {
    private let key: String
    private let defaultBlock: () -> T
    private let onChanged: VoidBlock
    let store: UserDefaults

    public init(key: String, store: UserDefaults? = nil, defaultBlock: @escaping () -> T, onChanged: @escaping VoidBlock = {
    }) {
        self.key = key
        self.defaultBlock = defaultBlock
        self.onChanged = onChanged
        self.store = store ?? UserDefaults.standard
    }

    public var wrappedValue: T {
        get {
            return store.object(forKey: key) as? T ?? defaultBlock()
        }
        set {
            store.set(newValue, forKey: key)
            onChanged()
        }
    }
}

@propertyWrapper
public struct UserDefaultOption<T: Any> {
    let key: String
    let store: UserDefaults

    public init(_ key: String, store: UserDefaults? = nil) {
        self.key = key
        self.store = store ?? UserDefaults.standard
    }

    public var wrappedValue: T? {
        get {
            return store.object(forKey: key) as? T
        }
        set {
            if let v = newValue ?? nil {
                store.set(v, forKey: key)
            } else {
                store.removeObject(forKey: key)
            }
        }
    }
}

@propertyWrapper
public struct UserDefault<T: Any> {
    let key: String
    let defaultValue: T
    let store: UserDefaults

    public init(_ key: String, defaultValue: T, store: UserDefaults? = nil) {
        self.key = key
        self.defaultValue = defaultValue
        self.store = store ?? UserDefaults.standard
    }

    public var wrappedValue: T {
        get {
            return store.object(forKey: key) as? T ?? defaultValue
        }
        set {
            store.set(newValue, forKey: key)
        }
    }
}
