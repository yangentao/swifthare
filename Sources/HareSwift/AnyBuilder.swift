//
// Created by yangentao on 2021/1/29.
//

import Foundation

public typealias AnyBuildBlock = () -> AnyGroup

public func buildItems<T: Any>(@AnyBuilder _ block: AnyBuildBlock) -> [T] {
    let c = block()
    return c.listItem()
}

public class AnyGroup {
    private var _items: [Any]

    public init(_ items: [Any]) {
        self._items = items
    }

    public func listItem<T: Any>() -> [T] {
        let list: [T?] = _items.flatMap { item in
            if let g = item as? AnyGroup {
                let ls: [T] = g.listItem()
                return ls
            } else if let v = item as? T {
                return [v]
            } else {
                return []
            }
        }
        return list.compactMap({ $0 })
    }
}

@resultBuilder
public struct AnyBuilder {

    public static func buildBlock(_ cs: Any...) -> AnyGroup {
        AnyGroup(cs)
    }

    public static func buildArray(_ cs: [Any]) -> AnyGroup {
        AnyGroup(cs)
    }

    public static func buildOptional(_ content: Any?) -> Any {
        content ?? AnyGroup([])
    }

    public static func buildEither(first: Any) -> Any {
        first
    }

    public static func buildEither(second: Any) -> Any {
        second
    }
}
