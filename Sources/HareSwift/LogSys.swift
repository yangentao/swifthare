//
//  LogSys.swift
//  HareSwift
//
//  Created by Entao on 2025/1/20.
//

import OSLog

public class SysLog {
    public let logger: Logger
    public init(subsystem: String = "dev.entao", category: String = "main") {
        logger = Logger(subsystem: subsystem, category: category)
    }

    public func d(_ items: Any?..., file: String = #file, line: Int = #line) {
        let f = file.substringAfterLast(char: "/")
        let msg = itemsToString(items)
        logger.debug("\(f)@\(line): \(msg)")
    }

    public func i(_ items: Any?..., file: String = #file, line: Int = #line) {
        let f = file.substringAfterLast(char: "/")
        let msg = itemsToString(items)
        logger.info("\(f)@\(line): \(msg)")
    }
    public func w(_ items: Any?..., file: String = #file, line: Int = #line) {
        let f = file.substringAfterLast(char: "/")
        let msg = itemsToString(items)
        logger.warning("\(f)@\(line): \(msg)")
    }
    public func e(_ items: Any?..., file: String = #file, line: Int = #line) {
        let f = file.substringAfterLast(char: "/")
        let msg = itemsToString(items)
        logger.error("\(f)@\(line): \(msg)")
    }
}
