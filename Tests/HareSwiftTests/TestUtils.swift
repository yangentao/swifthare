//
// Created by entaoyang@163.com on 2022/9/13.
//

import XCTest

@testable import HareSwift

extension String {
    func lastIndexOf(_ c: Character) -> Int {
        if let a = self.lastIndex(of: c) {
            return self.distance(from: self.startIndex, to: a)
        }
        return -1
    }
}

final class StringTests2: XCTestCase {
    func testIndex() throws {
        let path = "abcd"
        let n = path.lastIndexOf("c")
        logd(n)
        XCTAssertEqual(n, 2)
    }
}

final class BuilderTest: XCTestCase {
    func testBuilder() throws {
        
    }
}
