//
// Created by entaoyang@163.com on 2022/9/13.
//

import XCTest
@testable import HareSwift


final class StringTests: XCTestCase {
    func testTrim() throws {
        let path = "/Users/entao/"
        XCTAssertEqual(path.trimEnd(char: "/"), "/Users/entao")
        XCTAssertEqual(path.trimStart(char: "/"), "Users/entao/")
    }

    func testTrimEnd() throws {
        let s = "11233"
        XCTAssertEqual(s.trimEnd(char: "/"), "11233")
        XCTAssertEqual(s.trimEnd(char: "3"), "112")
    }
    func testTrimStart() throws {
        let s = "11233"
        XCTAssertEqual(s.trimStart(char: "/"), "11233")
        XCTAssertEqual(s.trimStart(char: "1"), "233")
    }
    func testAfter() throws {
        let path = "/Users/entao/abc.png"
        XCTAssertEqual(path.lastPath, "abc.png")
        XCTAssertEqual(path.substringAfter(char: "/"), "Users/entao/abc.png")
        XCTAssertEqual(path.substringAfterLast(char: "/"), "abc.png")
        XCTAssertEqual("abc".substringAfter(char: "/"), "abc")
        XCTAssertEqual("abc".substringAfter(char: "/", onMiss: "1"), "1")
        XCTAssertEqual("abc".substringAfterLast(char: "/"), "abc")
        XCTAssertEqual("abc".substringAfterLast(char: "/", onMiss: "2"), "2")
    }

    func testBefore() throws {
        let path = "/Users/entao/abc.png"
        XCTAssertEqual(path.substringBefore(char: "/"), "")
        XCTAssertEqual(path.substringBeforeLast(char: "/"), "/Users/entao")
    }
}
