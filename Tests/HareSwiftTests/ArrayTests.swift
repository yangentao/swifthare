import XCTest
@testable import HareSwift

final class ArrayTests: XCTestCase {
    func testAppend() throws {
        var ls = [1]
        ls += 2
        XCTAssertEqual(ls, [1, 2])

    }

    @available(iOS 15.0, macOS 12.0, *)
    func testSort() throws {
        let  ls = [(2, 2, 1), (2, 2, 2), (2, 2, 3), (2, 2, 4)]

        println(ls.sorted(order: .forward, \.0, order2: .reverse, \.1, order3: .reverse, \.2))

    }

//    func testAppendArray() throws {

//        var ls = [1]
//        ls += [2, 3]
//        XCTAssertEqual(ls, [1, 2, 3])
//    }
}
