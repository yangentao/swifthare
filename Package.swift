// swift-tools-version: 5.9
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "HareSwift",
    platforms: [
        .iOS(.v15),
        .macOS(.v12),
    ],
    products: [
        .library(name: "HareSwift", type: .static, targets: ["HareSwift"])
    ],
    dependencies: [],
    targets: [
        .target(name: "HareSwift", dependencies: []),
        .testTarget(name: "HareSwiftTests", dependencies: ["HareSwift"]),
        .executableTarget(name: "TestMain", dependencies: ["HareSwift"]),
    ],
    swiftLanguageVersions: [.v5]
)
